/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Label.h"
#include <vector>
#include <utility>
using std::vector;
using LegendOfTheSquareHammer::Renders::Label;
namespace LegendOfTheSquareHammer::ProgramData
{
	struct LabelsData
	{
		vector<Label *> Mini;
		vector<Label *> Menu;
		vector<Label *> Items;
		vector<Label *> Stats;
		vector<Label *> Resistance;
		vector<Label *> Damage;
		vector<std::pair<Label *, Label *>> Arrows;
		vector<Label *> Info;
		vector<Label *> Armor;
		vector<Label *> Weapon;
		vector<Label *> Potion;
	};
}