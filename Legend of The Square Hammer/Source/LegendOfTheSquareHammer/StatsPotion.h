/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Potion.h"
namespace LegendOfTheSquareHammer::Renders::Items::SingleUse
{
	class StatsPotion : public Potion
	{
		Stats statisticsChange;
	public:
		StatsPotion() {}
		StatsPotion(const string & newName, const string & newDescription, int newMana, int newHealth, size_t levelRequired, const string & path, int x, int y, const Stats & statistics) : Potion(newName, newDescription, newMana, newHealth, levelRequired, path, x, y), statisticsChange(statistics) {}

		inline Stats StatisticsChange() const { return statisticsChange; }

		virtual LegendOfTheSquareHammer::ItemType ItemType() const { return eStatsPot; }
		virtual ofstream & Write(ofstream & fout) const override { return fout << *this; }
		virtual ifstream & Read(ifstream & fin) override { return fin >> *this; }

		friend ofstream & operator<<(ofstream & fout, const StatsPotion & potion);
		friend ifstream & operator>>(ifstream & fin, StatsPotion & potion);
	};
}