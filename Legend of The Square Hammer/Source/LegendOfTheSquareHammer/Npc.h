/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "PickUp.h"
namespace LegendOfTheSquareHammer::Renders::Objects
{
	extern const size_t FIELD_WIDTH;
	extern const size_t FIELD_HEIGHT;

	class Npc : public IObject
	{
		SDL_Rect position;
		SDL_Rect collision;
		Renders::Texture texture;
		size_t X = 0;
		size_t Y = 0;
		size_t health = 0;
		size_t maxHealth = 0;
		size_t colliderRight = 0;
		size_t colliderDown = 0;
		short currentDirection = NONE;
		size_t spawnTime = 0;
		size_t woundTime = 0;
		size_t moveTime = 0;
		size_t attackTime = 0;
		size_t experience = 0;
		bool active = true;
		Damage resistance;
		Damage damage;
		PickUp * item = nullptr;
	public:
		Npc() {}
		Npc(double scale, size_t absoluteX, size_t absoluteY, int x, int y, size_t playerExperience, size_t newHealth, size_t deathSpawnTime, const SDL_Rect & collider, const Damage & newDamage, const Damage & newResistance, const string & path, PickUp * droppedItem = nullptr);

		inline SDL_Rect Collision() const { return collision; }
		inline SDL_Rect Position() const { return position; }
		inline Renders::Texture & Texture() { return texture; }
		inline size_t AbsoluteX() const { return X; }
		inline size_t AbsoluteY() const { return Y; }
		inline bool IsActive() const { return active; }
		inline size_t Experience() const { return experience; }

		void DeathSpawn();
		void SetPosition(int x, int y);
		void ChangePosition(int x, int y);
		void SetAbsolutePosition(size_t x, size_t y);
		void ChangeAbsolutePosition(int x, int y);
		short CollisionDirection(const IObject * object);
		void Move(Player * player, int fieldWidth = FIELD_WIDTH, int fieldHeight = FIELD_HEIGHT, short collisionDirection = NONE);
		virtual void Render();
		PickUp * Wound(const Damage & damageTaken);

		friend ofstream & operator<<(ofstream & fout, const Npc & npc);
		friend ifstream & operator>>(ifstream & fin, Npc & npc);

		~Npc();
	};
}