/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include <map>
#include <vector>
#include <array>
#include <utility>
#include "IObject.h"
#include "Armor.h"
#include "Weapon.h"
using std::map;
using std::vector;
using std::pair;
using namespace LegendOfTheSquareHammer::Renders::Items;
namespace LegendOfTheSquareHammer::Renders::Objects
{
	typedef map<const string, Item *> ItemMap;

	class Player : public IObject
	{
		string name = "player";
		size_t health = 0;
		size_t maxHealth = 0;
		size_t mana = 0;
		size_t maxMana = 0;
		size_t money = 0;
		size_t experience = 0;
		size_t nextLevelExperience = 0;
		size_t level = 0;
		size_t levelPoints = 0;
		Stats playerStatistics;
		Damage damage;
		Weapon * currentWeapon = nullptr;
		std::array<Armor *, 7> currentArmor;
		ItemMap items;
		SDL_Rect position;
		SDL_Rect collision;
		size_t X = 2000;
		size_t Y = 2000;
		vector<Renders::Texture* > textures;
		size_t textureNumber = 0;
		size_t animationDelay = 4;
		bool reverseAnimation = false;
		SDL_RendererFlip flip = SDL_FLIP_NONE;
	public:
		Player() {}
		Player(const string & name, const Stats & statistics);

		inline SDL_Rect Position() const { return position; }
		inline Renders::Texture & Texture() { return *textures.at(0); }
		inline string Name() const { return name; }
		inline SDL_Rect Collision() const { return collision; }
		inline size_t AbsoluteX() const { return X; }
		inline size_t AbsoluteY() const { return Y; }
		inline Damage Damage() const { return damage; }
		inline void SetTextureFlip(SDL_RendererFlip textureFlip) { flip = textureFlip; }
		inline void TakeMoney(size_t moneyTaken) { money += moneyTaken; }
		inline size_t Health() const { return health; }
		inline size_t Mana() const { return mana; }
		inline size_t MaxHealth() const { return maxHealth; }
		inline size_t MaxMana() const { return maxMana; }
		inline size_t Money() const { return money; }
		inline size_t Experience() const { return experience; }
		inline size_t NextLevelExperience() const { return nextLevelExperience; }
		inline size_t PointsLefToUse() const { return levelPoints; }
		inline Stats Statistics() const { return playerStatistics; }
		inline size_t Level() const { return level; }

		bool AddExperience(size_t earnedExperience);
		void LevelUp(const Stats & additionalStatistics);
		bool Wear(const string & name, short ringNumber = 2);
		bool TakeOff(BodyPart bodyPart, short ringNumber = 2);
		Item * ThrowOut(const string & name);
		void GetItem(Item * item);
		void SetPosition(int x, int y);
		void ChangePosition(int x, int y);
		void SetAbsolutePosition(size_t x, size_t y);
		void ChangeAbsolutePosition(int x, int y);
		virtual void Render();
		bool UseMana(size_t requestedMana);
		void RegenerateMana(size_t regeneratedMana);
		bool SpendMoney(size_t requestedMoney);
		bool Wound(const LegendOfTheSquareHammer::Damage & damageTaken);
		void Heal(size_t healthHealed);
		void ListOfItems(vector<pair<const string, Item *>> & list);
		void ListOfEquipment(pair<Weapon *, std::array<Armor *, 7>> & list);
		short Use(const string & name);

		friend ofstream & operator<<(ofstream & fout, const Player & player);
		friend ifstream & operator>>(ifstream & fin, Player & player);

		~Player() { for_each(textures.begin(), textures.end(), [](Renders::Texture * x) { x->Free(true); delete x; }); }
	};
}