/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "Info.h"
namespace LegendOfTheSquareHammer
{
	void Info::ArmorInfo(Armor * item, const Stats & statistics)
	{
		switch (item->Part())
		{
		case Necklace:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("Necklace", NormalColor);
			break;
		}
		case Ring:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("Ring", NormalColor);
			break;
		}
		case Head:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("Helmet", NormalColor);
			break;
		}
		case Chest:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("Armor", NormalColor);
			break;
		}
		case Leg:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("Greaves", NormalColor);
			break;
		}
		case Hands:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("Gauntlets", NormalColor);
			break;
		}
		}
		for_each(Data::GetLabels().Resistance.begin(), Data::GetLabels().Resistance.end(), [](Label * x) { x->SetPosition(x->Position().x, x->Position().y - 380); });
		Data::GetLabels().Armor.at(0)->Texture().LoadText("Inteligence: " + to_string(item->Statistics().MainStatistics.Inteligence), NormalColor);
		Data::GetLabels().Armor.at(1)->Texture().LoadText("Strength: " + to_string(item->Statistics().MainStatistics.Strenght), NormalColor);
		Data::GetLabels().Armor.at(2)->Texture().LoadText("Dextermity: " + to_string(item->Statistics().MainStatistics.Dextermity), NormalColor);
		Data::GetLabels().Armor.at(3)->Texture().LoadText("Endurance: " + to_string(item->Statistics().Endurance), NormalColor);
		Data::GetLabels().Armor.at(4)->Texture().LoadText("Vitality: " + to_string(item->Statistics().Vitality), NormalColor);
		Data::GetLabels().Armor.at(5)->Texture().LoadText("Armor: " + to_string(item->Statistics().Armor), NormalColor);
		Data::GetLabels().Resistance.at(1)->Texture().LoadText("Fire: " + to_string(item->Statistics().FireResistance), { 255,0,0,255 });
		Data::GetLabels().Resistance.at(2)->Texture().LoadText("Ice: " + to_string(item->Statistics().IceResistance), { 0,197,255,255 });
		Data::GetLabels().Resistance.at(3)->Texture().LoadText("Magic: " + to_string(item->Statistics().MagicResistance), { 87,14,185,255 });
		Data::GetLabels().Resistance.at(4)->Texture().LoadText("Darkness: " + to_string(item->Statistics().DarknessResistance), { 100,100,100,255 });
		if (statistics.MainStatistics.Inteligence >= item->MinimumStatistics().Inteligence)
			Data::GetLabels().Armor.at(8)->Texture().LoadText("Inteligence: " + to_string(item->MinimumStatistics().Inteligence), NormalColor);
		else
			Data::GetLabels().Armor.at(8)->Texture().LoadText("Inteligence: " + to_string(item->MinimumStatistics().Inteligence), { 255,0,0,255 });
		if (statistics.MainStatistics.Strenght >= item->MinimumStatistics().Strenght)
			Data::GetLabels().Armor.at(9)->Texture().LoadText("Strenght: " + to_string(item->MinimumStatistics().Strenght), NormalColor);
		else
			Data::GetLabels().Armor.at(9)->Texture().LoadText("Strenght: " + to_string(item->MinimumStatistics().Strenght), { 255,0,0,255 });
		if (statistics.MainStatistics.Dextermity >= item->MinimumStatistics().Dextermity)
			Data::GetLabels().Armor.at(10)->Texture().LoadText("Dextermity: " + to_string(item->MinimumStatistics().Dextermity), NormalColor);
		else
			Data::GetLabels().Armor.at(10)->Texture().LoadText("Dextermity: " + to_string(item->MinimumStatistics().Dextermity), { 255,0,0,255 });
		bool quit = false, resistance = false;
		SDL_Event e;
		while (!quit)
		{
			Data::GetLabels().Mini.at(14)->Render();
			item->Texture().Render(312, 193, 245, 245);
			for_each(Data::GetLabels().Info.begin(), Data::GetLabels().Info.begin() + 10, [](Label * x) { x->Render(); });
			for_each(Data::GetLabels().Armor.begin(), Data::GetLabels().Armor.end(), [](Label * x) { x->Render(); });
			if (resistance)
			{
				for_each(Data::GetLabels().Resistance.begin(), Data::GetLabels().Resistance.end(), [](Label * x) { x->Render(); });
				Data::GetLabels().Armor.at(6)->Render();
			}
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_MOUSEMOTION:
				{
					int x, y;
					SDL_GetMouseState(&x, &y);
					if (Data::GetLabels().Info.at(7)->IsMouseOn(x, y))
						Data::GetLabels().Info.at(7)->Texture().SetColor(HooverColor);
					else
						Data::GetLabels().Info.at(7)->Texture().SetColor(NormalColor);
					if (Data::GetLabels().Armor.at(6)->IsMouseOn(x, y))
						Data::GetLabels().Armor.at(6)->Texture().SetColor(HooverColor);
					else
						Data::GetLabels().Armor.at(6)->Texture().SetColor(NormalColor);
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						int x, y;
						SDL_GetMouseState(&x, &y);
						if (Data::GetLabels().Info.at(7)->IsMouseOn(x, y))
						{
							quit = true;
							break;
						}
						if (Data::GetLabels().Armor.at(6)->IsMouseOn(x, y))
							resistance = true;
						else
							resistance = false;
					}
					break;
				}
				}
			}
		}
		for_each(Data::GetLabels().Resistance.begin(), Data::GetLabels().Resistance.end(), [](Label * x) { x->SetPosition(x->Position().x, x->Position().y + 380); });
		Data::GetLabels().Resistance.at(1)->Texture().LoadText("Fire: " + to_string(statistics.FireResistance), { 255,0,0,255 });
		Data::GetLabels().Resistance.at(2)->Texture().LoadText("Ice: " + to_string(statistics.IceResistance), { 0,197,255,255 });
		Data::GetLabels().Resistance.at(3)->Texture().LoadText("Magic: " + to_string(statistics.MagicResistance), { 87,14,185,255 });
		Data::GetLabels().Resistance.at(4)->Texture().LoadText("Darkness: " + to_string(statistics.DarknessResistance), { 100,100,100,255 });
	}

	void Info::WeaponInfo(Renders::Items::Weapon * item, const Stats & statistics)
	{
		switch (item->Type())
		{
		case Axe:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("   Axe   ", NormalColor);
			break;
		}
		case Hammer:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("Hammer", NormalColor);
			break;
		}
		case Sword:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("Sword", NormalColor);
			break;
		}
		case Dagger:
		{
			Data::GetLabels().Info.at(9)->Texture().LoadText("Dagger", NormalColor);
			break;
		}
		}
		for_each(Data::GetLabels().Resistance.begin(), Data::GetLabels().Resistance.end(), [](Label * x) { x->SetPosition(x->Position().x, x->Position().y - 255); });
		Data::GetLabels().Weapon.at(0)->Texture().LoadText("Inteligence: " + to_string(item->Statistics().MainStatistics.Inteligence), NormalColor);
		Data::GetLabels().Weapon.at(1)->Texture().LoadText("Strength: " + to_string(item->Statistics().MainStatistics.Strenght), NormalColor);
		Data::GetLabels().Weapon.at(2)->Texture().LoadText("Dextermity: " + to_string(item->Statistics().MainStatistics.Dextermity), NormalColor);
		Data::GetLabels().Weapon.at(3)->Texture().LoadText("Endurance: " + to_string(item->Statistics().Endurance), NormalColor);
		Data::GetLabels().Weapon.at(4)->Texture().LoadText("Vitality: " + to_string(item->Statistics().Vitality), NormalColor);
		Data::GetLabels().Weapon.at(5)->Texture().LoadText("Armor: " + to_string(item->Statistics().Armor), NormalColor);
		Data::GetLabels().Resistance.at(1)->Texture().LoadText("Fire: " + to_string(item->Statistics().FireResistance), { 255,0,0,255 });
		Data::GetLabels().Resistance.at(2)->Texture().LoadText("Ice: " + to_string(item->Statistics().IceResistance), { 0,197,255,255 });
		Data::GetLabels().Resistance.at(3)->Texture().LoadText("Magic: " + to_string(item->Statistics().MagicResistance), { 87,14,185,255 });
		Data::GetLabels().Resistance.at(4)->Texture().LoadText("Darkness: " + to_string(item->Statistics().DarknessResistance), { 100,100,100,255 });
		Data::GetLabels().Weapon.at(14)->Texture().LoadText("Normal: " + to_string(item->Damage().Normal), NormalColor);
		Data::GetLabels().Weapon.at(15)->Texture().LoadText("Fire: " + to_string(item->Damage().Fire), { 255,0,0,255 });
		Data::GetLabels().Weapon.at(16)->Texture().LoadText("Ice: " + to_string(item->Damage().Ice), { 0,197,255,255 });
		Data::GetLabels().Weapon.at(17)->Texture().LoadText("Magic: " + to_string(item->Damage().Magic), { 87,14,185,255 });
		Data::GetLabels().Weapon.at(18)->Texture().LoadText("Darkness: " + to_string(item->Damage().Darkness), { 100,100,100,255 });
		Data::GetLabels().Weapon.at(19)->Texture().LoadText("Critical: " + to_string(item->CriticalChance()), NormalColor);
		Data::GetLabels().Weapon.at(20)->Texture().LoadText("Penetration: " + to_string(item->ArmorPenetration()), NormalColor);
		if (statistics.MainStatistics.Inteligence >= item->MinimumStatistics().Inteligence)
			Data::GetLabels().Weapon.at(8)->Texture().LoadText("Inteligence: " + to_string(item->MinimumStatistics().Inteligence), NormalColor);
		else
			Data::GetLabels().Weapon.at(8)->Texture().LoadText("Inteligence: " + to_string(item->MinimumStatistics().Inteligence), { 255,0,0,255 });
		if (statistics.MainStatistics.Strenght >= item->MinimumStatistics().Strenght)
			Data::GetLabels().Weapon.at(9)->Texture().LoadText("Strenght: " + to_string(item->MinimumStatistics().Strenght), NormalColor);
		else
			Data::GetLabels().Weapon.at(9)->Texture().LoadText("Strenght: " + to_string(item->MinimumStatistics().Strenght), { 255,0,0,255 });
		if (statistics.MainStatistics.Dextermity >= item->MinimumStatistics().Dextermity)
			Data::GetLabels().Weapon.at(10)->Texture().LoadText("Dextermity: " + to_string(item->MinimumStatistics().Dextermity), NormalColor);
		else
			Data::GetLabels().Weapon.at(10)->Texture().LoadText("Dextermity: " + to_string(item->MinimumStatistics().Dextermity), { 255,0,0,255 });
		bool quit = false, resistance = false, damage = false;
		SDL_Event e;
		while (!quit)
		{
			Data::GetLabels().Mini.at(14)->Render();
			item->Texture().Render(312, 193, 245, 245);
			for_each(Data::GetLabels().Info.begin(), Data::GetLabels().Info.begin() + 10, [](Label * x) { x->Render(); });
			for_each(Data::GetLabels().Weapon.begin(), Data::GetLabels().Weapon.begin() + 13, [](Label * x) { x->Render(); });
			if (resistance)
			{
				for_each(Data::GetLabels().Resistance.begin(), Data::GetLabels().Resistance.end(), [](Label * x) { x->Render(); });
				Data::GetLabels().Weapon.at(6)->Render();
			}
			if (damage)
			{
				for_each(Data::GetLabels().Weapon.begin() + 13, Data::GetLabels().Weapon.end(), [](Label * x) { x->Render(); });
				Data::GetLabels().Weapon.at(12)->Render();
			}
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_MOUSEMOTION:
				{
					int x, y;
					SDL_GetMouseState(&x, &y);
					if (Data::GetLabels().Info.at(7)->IsMouseOn(x, y))
						Data::GetLabels().Info.at(7)->Texture().SetColor(HooverColor);
					else
						Data::GetLabels().Info.at(7)->Texture().SetColor(NormalColor);
					if (Data::GetLabels().Weapon.at(6)->IsMouseOn(x, y))
						Data::GetLabels().Weapon.at(6)->Texture().SetColor(HooverColor);
					else
						Data::GetLabels().Weapon.at(6)->Texture().SetColor(NormalColor);
					if (Data::GetLabels().Weapon.at(12)->IsMouseOn(x, y))
						Data::GetLabels().Weapon.at(12)->Texture().SetColor(HooverColor);
					else
						Data::GetLabels().Weapon.at(12)->Texture().SetColor(NormalColor);
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						int x, y;
						SDL_GetMouseState(&x, &y);
						if (Data::GetLabels().Info.at(7)->IsMouseOn(x, y))
						{
							quit = true;
							break;
						}
						if (Data::GetLabels().Weapon.at(6)->IsMouseOn(x, y))
							resistance = true;
						else
							resistance = false;
						if (Data::GetLabels().Weapon.at(12)->IsMouseOn(x, y))
							damage = true;
						else
							damage = false;
					}
					break;
				}
				}
			}
		}
		for_each(Data::GetLabels().Resistance.begin(), Data::GetLabels().Resistance.end(), [](Label * x) { x->SetPosition(x->Position().x, x->Position().y + 255); });
		Data::GetLabels().Resistance.at(1)->Texture().LoadText("Fire: " + to_string(statistics.FireResistance), { 255,0,0,255 });
		Data::GetLabels().Resistance.at(2)->Texture().LoadText("Ice: " + to_string(statistics.IceResistance), { 0,197,255,255 });
		Data::GetLabels().Resistance.at(3)->Texture().LoadText("Magic: " + to_string(statistics.MagicResistance), { 87,14,185,255 });
		Data::GetLabels().Resistance.at(4)->Texture().LoadText("Darkness: " + to_string(statistics.DarknessResistance), { 100,100,100,255 });
	}

	void Info::PotionInfo(Renders::Items::SingleUse::Potion * item)
	{
		if (item->Health() > 0)
			Data::GetLabels().Potion.at(0)->Texture().LoadText("Health restored: " + to_string(item->Health()), NormalColor);
		else if (item->Health() < 0)
			Data::GetLabels().Potion.at(0)->Texture().LoadText("Health taken: " + to_string(-1 * item->Health()), NormalColor);
		else
			Data::GetLabels().Potion.at(0)->Texture().LoadText("No health changed", NormalColor);
		if (item->Mana() > 0)
			Data::GetLabels().Potion.at(1)->Texture().LoadText("Mana restored: " + to_string(item->Mana()), NormalColor);
		else if (item->Mana() < 0)
			Data::GetLabels().Potion.at(1)->Texture().LoadText("Mana required: " + to_string(-1 * item->Mana()), NormalColor);
		else
			Data::GetLabels().Potion.at(1)->Texture().LoadText("No mana required", NormalColor);
		if (dynamic_cast<const Renders::Items::SingleUse::StatsPotion *>(item) != nullptr)
			Data::GetLabels().Info.at(9)->Texture().LoadText("Upgrade Potion", NormalColor);
		else
			Data::GetLabels().Info.at(9)->Texture().LoadText("Potion", NormalColor);
		bool quit = false;
		SDL_Event e;
		while (!quit)
		{
			Data::GetLabels().Mini.at(14)->Render();
			item->Texture().Render(312, 193, 245, 245);
			for_each(Data::GetLabels().Info.begin(), Data::GetLabels().Info.begin() + 10, [](Label * x) { x->Render(); });
			for_each(Data::GetLabels().Potion.begin(), Data::GetLabels().Potion.end(), [](Label * x) { x->Render(); });
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_MOUSEMOTION:
				{
					int x, y;
					SDL_GetMouseState(&x, &y);
					if (Data::GetLabels().Info.at(7)->IsMouseOn(x, y))
						Data::GetLabels().Info.at(7)->Texture().SetColor(HooverColor);
					else
						Data::GetLabels().Info.at(7)->Texture().SetColor(NormalColor);
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						int x, y;
						SDL_GetMouseState(&x, &y);
						if (Data::GetLabels().Info.at(7)->IsMouseOn(x, y))
						{
							quit = true;
							break;
						}
					}
					break;
				}
				}
			}
		}
	}

	void Info::SetStatsLabels(const Stats & currentStatistics, const Damage & currentDamage)
	{
		Data::GetLabels().Stats.at(1)->Texture().LoadText("Inteligence: " + to_string(currentStatistics.MainStatistics.Inteligence), NormalColor);
		Data::GetLabels().Stats.at(2)->Texture().LoadText("Strength: " + to_string(currentStatistics.MainStatistics.Strenght), NormalColor);
		Data::GetLabels().Stats.at(3)->Texture().LoadText("Dextermity: " + to_string(currentStatistics.MainStatistics.Dextermity), NormalColor);
		Data::GetLabels().Stats.at(4)->Texture().LoadText("Endurance: " + to_string(currentStatistics.Endurance), NormalColor);
		Data::GetLabels().Stats.at(5)->Texture().LoadText("Vitality: " + to_string(currentStatistics.Vitality), NormalColor);
		Data::GetLabels().Stats.at(8)->Texture().LoadText("Armor: " + to_string(currentStatistics.Armor), NormalColor);
		Data::GetLabels().Resistance.at(1)->Texture().LoadText("Fire: " + to_string(currentStatistics.FireResistance), { 255,0,0,255 });
		Data::GetLabels().Resistance.at(2)->Texture().LoadText("Ice: " + to_string(currentStatistics.IceResistance), { 0,197,255,255 });
		Data::GetLabels().Resistance.at(3)->Texture().LoadText("Magic: " + to_string(currentStatistics.MagicResistance), { 87,14,185,255 });
		Data::GetLabels().Resistance.at(4)->Texture().LoadText("Darkness: " + to_string(currentStatistics.DarknessResistance), { 100,100,100,255 });
		Data::GetLabels().Damage.at(1)->Texture().LoadText("Normal: " + to_string(currentDamage.Normal), NormalColor);
		Data::GetLabels().Damage.at(2)->Texture().LoadText("Fire: " + to_string(currentDamage.Fire), { 255,0,0,255 });
		Data::GetLabels().Damage.at(3)->Texture().LoadText("Ice: " + to_string(currentDamage.Ice), { 0,197,255,255 });
		Data::GetLabels().Damage.at(4)->Texture().LoadText("Magic: " + to_string(currentDamage.Magic), { 87,14,185,255 });
		Data::GetLabels().Damage.at(5)->Texture().LoadText("Darkness: " + to_string(currentDamage.Darkness), { 100,100,100,255 });
	}

	void Info::ItemInfo(Item * item, const Renders::Objects::Player * player)
	{
		Data::GetLabels().Info.at(0)->Texture().LoadText(item->Name(), HooverColor);
		if (player->Level() < item->MinimumLevel())
			Data::GetLabels().Info.at(8)->Texture().LoadText("Level: " + to_string(item->MinimumLevel()), { 255,0,0,255 });
		else
			Data::GetLabels().Info.at(8)->Texture().LoadText("Level: " + to_string(item->MinimumLevel()), NormalColor);
		string description = item->Description();
		if (description.size() > 125)
		{
			string line = "";
			for (int i = 0, I = description.size(), j = 0; i < 5; ++i)
			{
				for (int J = I / 5 + i * I / 5; j < J && j < I;)
				{
					if (j == J - 1 && description[j] == ' ')
						++j;
					else if (j == J - 1 && j < I - 1 && description[j + 1] != ' ' && description[j] != ' ')
					{
						for (; j < I && description[j] != ' '; ++j)
							line += description[j];
						break;
					}
					else if (j == I - 1)
					{
						line += description[j];
						++j;
						for (; j < J; ++j)
							line += " ";
						break;
					}
					else
					{
						line += description[j];
						++j;
					}
				}
				Data::GetLabels().Info.at(2 + i)->Texture().LoadText(line, NormalColor);
				line = "";
			}
		}
		else if (description.size() > 25)
		{
			string line = "";
			for (int i = 0, I = description.size(), j = 0; i < 5; ++i)
			{
				for (int J = 25 + i * 25; j < J && j < I;)
				{
					if (j == J - 1 && description[j] == ' ')
						++j;
					else if (j == J - 1 && j < I - 1 && description[j + 1] != ' ' && description[j] != ' ')
					{
						for (; j < I && description[j] != ' '; ++j)
							line += description[j];
						break;
					}
					else if (j == I - 1)
					{
						line += description[j];
						++j;
						for (; j < J; ++j)
							line += " ";
					}
					else
					{
						line += description[j];
						++j;
					}
				}
				Data::GetLabels().Info.at(2 + i)->Texture().LoadText(line, NormalColor);
				line = "";
			}
		}
		else
			Data::GetLabels().Info.at(2)->Texture().LoadText(description, NormalColor);
		if (dynamic_cast<const Armor *>(item) != nullptr)
			return ArmorInfo((Armor *)item, player->Statistics());
		else if (dynamic_cast<const Weapon *>(item) != nullptr)
			return WeaponInfo((Weapon *)item, player->Statistics());
		else if (dynamic_cast<const SingleUse::Potion *>(item) != nullptr)
			return PotionInfo((SingleUse::Potion *)item);
		else if (dynamic_cast<const Magic::AttackScroll *>(item) != nullptr)
			Data::GetLabels().Info.at(9)->Texture().LoadText("Offensive Scroll", NormalColor);
		else if (dynamic_cast<const Magic::EffectScroll *>(item) != nullptr)
			Data::GetLabels().Info.at(9)->Texture().LoadText("Enchantment", NormalColor);
		else if (dynamic_cast<const Magic::SpellScroll *>(item) != nullptr)
			Data::GetLabels().Info.at(9)->Texture().LoadText("Spell", NormalColor);
		else
			return;
	}

	void Info::PlayerStatistics(Player * player)
	{
		size_t pointsLeft = 0;
		Stats newStatistics;
		bool resistance = false, damage = false, isStatisticsHoover = false, newLevel = false;
		if (player->PointsLefToUse())
		{
			newLevel = true;
			pointsLeft = player->PointsLefToUse() * 5;
			Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
		}
		SDL_Event e;
		Data::GetLabels().Stats.at(0)->Texture().LoadText("Level: " + to_string(player->Level()), NormalColor);
		Data::GetLabels().Stats.at(9)->Texture().LoadText(player->Name(), HooverColor);
		Data::GetLabels().Stats.at(10)->Texture().LoadText("Health: " + to_string(player->Health()) + "/" + to_string(player->MaxHealth()), { 255,0,0,255 });
		Data::GetLabels().Stats.at(11)->Texture().LoadText("Mana: " + to_string(player->Mana()) + "/" + to_string(player->MaxMana()), { 0,197,255,255 });
		Data::GetLabels().Stats.at(13)->Texture().LoadText("Exp: " + to_string(player->Experience()) + "/" + to_string(player->NextLevelExperience()), NormalColor);
		Data::GetLabels().Stats.at(12)->Texture().LoadText("Money: " + to_string(player->Money()), { 255,215,0,255 });
		SetStatsLabels(player->Statistics(), player->Damage());
		while (true)
		{
			Data::GetLabels().Mini.at(12)->Render();
			for_each(Data::GetLabels().Stats.begin() + 9, Data::GetLabels().Stats.begin() + 13, [](Label * x) { x->Render(); });
			if (!newLevel)
			{
				Data::GetLabels().Stats.at(13)->Render();
				for_each(Data::GetLabels().Stats.begin(), Data::GetLabels().Stats.end(), [](Label * x) { x->Render(); });
				if (damage)
				{
					for_each(Data::GetLabels().Damage.begin(), Data::GetLabels().Damage.end(), [](Label * x) { x->Render(); });
					Data::GetLabels().Stats.at(6)->Render();
				}
				if (resistance)
				{
					for_each(Data::GetLabels().Resistance.begin(), Data::GetLabels().Resistance.end(), [](Label * x) { x->Render(); });
					Data::GetLabels().Stats.at(7)->Render();
				}
			}
			else
			{
				for_each(Data::GetLabels().Stats.begin(), Data::GetLabels().Stats.begin() + 6, [](Label * x) { x->Render(); });
				if (pointsLeft)
					for_each(Data::GetLabels().Arrows.begin(), Data::GetLabels().Arrows.end(), [](pair<Label *, Label * > & x) { x.second->Render(); });
				Data::GetLabels().Mini.at(13)->Render();
				if (newStatistics.MainStatistics.Inteligence)
					Data::GetLabels().Arrows.at(0).first->Render();
				if (newStatistics.MainStatistics.Strenght)
					Data::GetLabels().Arrows.at(1).first->Render();
				if (newStatistics.MainStatistics.Dextermity)
					Data::GetLabels().Arrows.at(2).first->Render();
				if (newStatistics.Endurance)
					Data::GetLabels().Arrows.at(3).first->Render();
				if (newStatistics.Vitality)
					Data::GetLabels().Arrows.at(4).first->Render();
			}
			if (isStatisticsHoover)
			{
				Data::GetLabels().Mini.at(10)->Render();
				Data::GetLabels().Mini.at(11)->Render();
			}
			else
			{
				Data::GetLabels().Mini.at(11)->Render();
				Data::GetLabels().Mini.at(10)->Render();
			}
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_KEYDOWN:
				case SDL_KEYUP:
				{
					Events::EventHandler::GetKeyboard().HandleKey(e);
					break;
				}
				case SDL_MOUSEMOTION:
				{
					int x, y;
					SDL_GetMouseState(&x, &y);
					if (Data::GetLabels().Mini.at(11)->IsMouseOn(x, y))
					{
						Data::GetLabels().Mini.at(11)->Texture().SetColor(HooverColor);
						isStatisticsHoover = true;
					}
					else
					{
						Data::GetLabels().Mini.at(11)->Texture().SetColor(NormalColor);
						isStatisticsHoover = false;
					}
					if (!newLevel)
					{
						if (Data::GetLabels().Stats.at(6)->IsMouseOn(x, y))
							Data::GetLabels().Stats.at(6)->Texture().SetColor(HooverColor);
						else
							Data::GetLabels().Stats.at(6)->Texture().SetColor(NormalColor);
						if (Data::GetLabels().Stats.at(7)->IsMouseOn(x, y))
							Data::GetLabels().Stats.at(7)->Texture().SetColor(HooverColor);
						else
							Data::GetLabels().Stats.at(7)->Texture().SetColor(NormalColor);
					}
					else
					{
						if (pointsLeft)
							for_each(Data::GetLabels().Arrows.begin(), Data::GetLabels().Arrows.end(), [&x, &y](pair<Label *, Label * > & z)
						{
							if (z.second->IsMouseOn(x, y))
								z.second->Texture().SetColor(HooverColor);
							else
								z.second->Texture().SetColor(NormalColor);
						});
						else if (Data::GetLabels().Mini.at(13)->IsMouseOn(x, y))
							Data::GetLabels().Mini.at(13)->Texture().SetColor(HooverColor);
						else
							Data::GetLabels().Mini.at(13)->Texture().SetColor(NormalColor);
						if (newStatistics.MainStatistics.Inteligence && Data::GetLabels().Arrows.at(0).first->IsMouseOn(x, y))
							Data::GetLabels().Arrows.at(0).first->Texture().SetColor(HooverColor);
						else
							Data::GetLabels().Arrows.at(0).first->Texture().SetColor(NormalColor);
						if (newStatistics.MainStatistics.Strenght && Data::GetLabels().Arrows.at(1).first->IsMouseOn(x, y))
							Data::GetLabels().Arrows.at(1).first->Texture().SetColor(HooverColor);
						else
							Data::GetLabels().Arrows.at(1).first->Texture().SetColor(NormalColor);
						if (newStatistics.MainStatistics.Dextermity && Data::GetLabels().Arrows.at(2).first->IsMouseOn(x, y))
							Data::GetLabels().Arrows.at(2).first->Texture().SetColor(HooverColor);
						else
							Data::GetLabels().Arrows.at(2).first->Texture().SetColor(NormalColor);
						if (newStatistics.Endurance && Data::GetLabels().Arrows.at(3).first->IsMouseOn(x, y))
							Data::GetLabels().Arrows.at(3).first->Texture().SetColor(HooverColor);
						else
							Data::GetLabels().Arrows.at(3).first->Texture().SetColor(NormalColor);
						if (newStatistics.Vitality && Data::GetLabels().Arrows.at(4).first->IsMouseOn(x, y))
							Data::GetLabels().Arrows.at(4).first->Texture().SetColor(HooverColor);
						else
							Data::GetLabels().Arrows.at(4).first->Texture().SetColor(NormalColor);
					}
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					int x, y;
					SDL_GetMouseState(&x, &y);
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						if (Data::GetLabels().Mini.at(11)->IsMouseOn(x, y))
						{
							Data::GetLabels().Mini.at(11)->Texture().SetColor(NormalColor);
							return PlayerEquipment(player);
						}
						if (!newLevel)
						{
							if (Data::GetLabels().Stats.at(6)->IsMouseOn(x, y))
								damage = true;
							else
								damage = false;
							if (Data::GetLabels().Stats.at(7)->IsMouseOn(x, y))
								resistance = true;
							else
								resistance = false;
						}
						else
						{
							if (pointsLeft)
							{
								if (Data::GetLabels().Arrows.at(0).second->IsMouseOn(x, y))
								{
									--pointsLeft;
									Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
									++newStatistics.MainStatistics.Inteligence;
									Data::GetLabels().Stats.at(1)->Texture().LoadText("Inteligence: " + to_string(player->Statistics().MainStatistics.Inteligence + newStatistics.MainStatistics.Inteligence), NormalColor);
									Data::GetLabels().Stats.at(11)->Texture().LoadText("Mana: " + to_string(player->Mana()) + "/" + to_string((player->Statistics().MainStatistics.Inteligence + newStatistics.MainStatistics.Inteligence) * 7), { 0,197,255,255 });
								}
								if (Data::GetLabels().Arrows.at(1).second->IsMouseOn(x, y))
								{
									--pointsLeft;
									Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
									++newStatistics.MainStatistics.Strenght;
									Data::GetLabels().Stats.at(2)->Texture().LoadText("Strength: " + to_string(player->Statistics().MainStatistics.Strenght + newStatistics.MainStatistics.Strenght), NormalColor);
								}
								if (Data::GetLabels().Arrows.at(2).second->IsMouseOn(x, y))
								{
									--pointsLeft;
									Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
									++newStatistics.MainStatistics.Dextermity;
									Data::GetLabels().Stats.at(3)->Texture().LoadText("Dextermity: " + to_string(player->Statistics().MainStatistics.Dextermity + newStatistics.MainStatistics.Dextermity), NormalColor);
								}
								if (Data::GetLabels().Arrows.at(3).second->IsMouseOn(x, y))
								{
									--pointsLeft;
									Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
									++newStatistics.Endurance;
									Data::GetLabels().Stats.at(4)->Texture().LoadText("Endurance: " + to_string(player->Statistics().Endurance + newStatistics.Endurance), NormalColor);
								}
								if (Data::GetLabels().Arrows.at(4).second->IsMouseOn(x, y))
								{
									--pointsLeft;
									Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
									++newStatistics.Vitality;
									Data::GetLabels().Stats.at(5)->Texture().LoadText("Vitality: " + to_string(player->Statistics().Vitality + newStatistics.Vitality), NormalColor);
									Data::GetLabels().Stats.at(10)->Texture().LoadText("Health: " + to_string(player->Health()) + "/" + to_string((player->Statistics().Vitality + newStatistics.Vitality) * 12), { 255,0,0,255 });
								}
								if (!pointsLeft)
									Data::GetLabels().Mini.at(13)->Texture().LoadText("Accept", NormalColor);
							}
							else if (Data::GetLabels().Mini.at(13)->IsMouseOn(x, y))
							{
								newLevel = false;
								player->LevelUp(newStatistics);
								SetStatsLabels(player->Statistics(), player->Damage());
								Data::GetLabels().Stats.at(10)->Texture().LoadText("Health: " + to_string(player->Health()) + "/" + to_string(player->MaxHealth()), { 255,0,0,255 });
								Data::GetLabels().Stats.at(11)->Texture().LoadText("Mana: " + to_string(player->Mana()) + "/" + to_string(player->MaxMana()), { 0,197,255,255 });
								break;
							}
							if (newStatistics.MainStatistics.Inteligence && Data::GetLabels().Arrows.at(0).first->IsMouseOn(x, y))
							{
								++pointsLeft;
								Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
								--newStatistics.MainStatistics.Inteligence;
								Data::GetLabels().Stats.at(1)->Texture().LoadText("Inteligence: " + to_string(player->Statistics().MainStatistics.Inteligence + newStatistics.MainStatistics.Inteligence), NormalColor);
								Data::GetLabels().Stats.at(11)->Texture().LoadText("Mana: " + to_string(player->Mana()) + "/" + to_string((player->Statistics().MainStatistics.Inteligence + newStatistics.MainStatistics.Inteligence) * 7), { 0,197,255,255 });
							}
							if (newStatistics.MainStatistics.Strenght && Data::GetLabels().Arrows.at(1).first->IsMouseOn(x, y))
							{
								++pointsLeft;
								Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
								--newStatistics.MainStatistics.Strenght;
								Data::GetLabels().Stats.at(2)->Texture().LoadText("Strength: " + to_string(player->Statistics().MainStatistics.Strenght + newStatistics.MainStatistics.Strenght), NormalColor);
							}
							if (newStatistics.MainStatistics.Dextermity && Data::GetLabels().Arrows.at(2).first->IsMouseOn(x, y))
							{
								++pointsLeft;
								Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
								--newStatistics.MainStatistics.Dextermity;
								Data::GetLabels().Stats.at(3)->Texture().LoadText("Dextermity: " + to_string(player->Statistics().MainStatistics.Dextermity + newStatistics.MainStatistics.Dextermity), NormalColor);
							}
							if (newStatistics.Endurance && Data::GetLabels().Arrows.at(3).first->IsMouseOn(x, y))
							{
								++pointsLeft;
								Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
								--newStatistics.Endurance;
								Data::GetLabels().Stats.at(4)->Texture().LoadText("Endurance: " + to_string(player->Statistics().Endurance + newStatistics.Endurance), NormalColor);
							}
							if (newStatistics.Vitality && Data::GetLabels().Arrows.at(4).first->IsMouseOn(x, y))
							{
								++pointsLeft;
								Data::GetLabels().Mini.at(13)->Texture().LoadText("Points left: " + to_string(pointsLeft), NormalColor);
								--newStatistics.Vitality;
								Data::GetLabels().Stats.at(5)->Texture().LoadText("Vitality: " + to_string(player->Statistics().Vitality + newStatistics.Vitality), NormalColor);
								Data::GetLabels().Stats.at(10)->Texture().LoadText("Health: " + to_string(player->Health()) + "/" + to_string((player->Statistics().Vitality + newStatistics.Vitality) * 12), { 255,0,0,255 });
							}
						}
					}
					break;
				}
				default:
					break;
				}
			}
			if (Events::EventHandler::GetKeyboard().GetKeyState(SDLK_ESCAPE))
			{
				Events::EventHandler::GetKeyboard().SetKeyState(SDLK_ESCAPE, false);
				break;
			}
		}
	}

	void Info::PlayerEquipment(Player * player)
	{
		size_t itemCardNumber = 0;
		vector<pair<const string, Item *>> itemList;
		pair<Weapon *, std::array<Armor *, 7>> equipmentList;
		player->ListOfItems(itemList);
		player->ListOfEquipment(equipmentList);
		bool rightClick = false, isRing = false, isStatisticsHoover = false;
		string itemName = "";
		int X = 0, Y = 0, itemNumber = 0;
		SDL_Event e;
		while (true)
		{
			Data::GetLabels().Mini.at(3)->Render();
			if (isStatisticsHoover)
			{
				Data::GetLabels().Mini.at(11)->Render();
				Data::GetLabels().Mini.at(10)->Render();
			}
			else
			{
				Data::GetLabels().Mini.at(10)->Render();
				Data::GetLabels().Mini.at(11)->Render();
			}
			for (size_t i = 0, current = 0 + itemCardNumber * 42, Max = itemList.size(); i < 6 && current < Max; ++i)
				for (size_t j = 0; j < 7 && current < Max; ++j, ++current)
					itemList.at(current).second->Texture().Render(877 + j * 131, 266 + i * 125, 70, 70);
			for_each(Data::GetLabels().Items.begin(), Data::GetLabels().Items.end(), [](Label * x) { x->Render(); });
			if (equipmentList.second.at(0) != nullptr)
				equipmentList.second.at(0)->Texture().Render(292, 516, 70, 70);
			if (equipmentList.second.at(1) != nullptr)
				equipmentList.second.at(1)->Texture().Render(292, 641, 70, 70);
			if (equipmentList.second.at(3) != nullptr)
				equipmentList.second.at(3)->Texture().Render(292, 766, 70, 70);
			if (equipmentList.second.at(6) != nullptr)
				equipmentList.second.at(6)->Texture().Render(292, 891, 70, 70);
			if (equipmentList.first != nullptr)
				equipmentList.first->Texture().Render(506, 516, 70, 70);
			if (equipmentList.second.at(2) != nullptr)
				equipmentList.second.at(2)->Texture().Render(506, 641, 70, 70);
			if (equipmentList.second.at(4) != nullptr)
				equipmentList.second.at(4)->Texture().Render(506, 766, 70, 70);
			if (equipmentList.second.at(5) != nullptr)
				equipmentList.second.at(5)->Texture().Render(506, 891, 70, 70);
			if (rightClick)
			{
				Data::GetLabels().Mini.at(4)->Render();
				Data::GetLabels().Mini.at(5)->Render();
				Data::GetLabels().Mini.at(6)->Render();
				Data::GetLabels().Mini.at(7)->Render();
				if (isRing)
				{
					Data::GetLabels().Mini.at(8)->Render();
					Data::GetLabels().Mini.at(9)->Render();
				}
			}
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_KEYDOWN:
				case SDL_KEYUP:
				{
					Events::EventHandler::GetKeyboard().HandleKey(e);
					break;
				}
				case SDL_MOUSEMOTION:
				{
					int x, y;
					bool isHoover = false;
					SDL_GetMouseState(&x, &y);
					if (Data::GetLabels().Mini.at(10)->IsMouseOn(x, y))
					{
						Data::GetLabels().Mini.at(10)->Texture().SetColor(HooverColor);
						isStatisticsHoover = isHoover = true;
					}
					else
					{
						Data::GetLabels().Mini.at(10)->Texture().SetColor(NormalColor);
						isStatisticsHoover = false;
					}
					if (!isHoover && equipmentList.second.at(0) != nullptr && Data::GetLabels().Items.at(0)->IsMouseOn(x, y))
					{
						Data::GetLabels().Items.at(0)->Texture().SetColor(255, 0, 0);
						isHoover = true;
					}
					else
						Data::GetLabels().Items.at(0)->Texture().SetColor(NormalColor);
					if (!isHoover && equipmentList.second.at(1) != nullptr && Data::GetLabels().Items.at(1)->IsMouseOn(x, y))
					{
						Data::GetLabels().Items.at(1)->Texture().SetColor(255, 0, 0);
						isHoover = true;
					}
					else
						Data::GetLabels().Items.at(1)->Texture().SetColor(NormalColor);
					if (!isHoover && equipmentList.second.at(3) != nullptr && Data::GetLabels().Items.at(2)->IsMouseOn(x, y))
					{
						Data::GetLabels().Items.at(2)->Texture().SetColor(255, 0, 0);
						isHoover = true;
					}
					else
						Data::GetLabels().Items.at(2)->Texture().SetColor(NormalColor);
					if (!isHoover && equipmentList.second.at(6) != nullptr && Data::GetLabels().Items.at(3)->IsMouseOn(x, y))
					{
						Data::GetLabels().Items.at(3)->Texture().SetColor(255, 0, 0);
						isHoover = true;
					}
					else
						Data::GetLabels().Items.at(3)->Texture().SetColor(NormalColor);
					if (!isHoover && equipmentList.first != nullptr && Data::GetLabels().Items.at(4)->IsMouseOn(x, y))
					{
						Data::GetLabels().Items.at(4)->Texture().SetColor(255, 0, 0);
						isHoover = true;
					}
					else
						Data::GetLabels().Items.at(4)->Texture().SetColor(NormalColor);
					if (!isHoover && equipmentList.second.at(2) != nullptr && Data::GetLabels().Items.at(5)->IsMouseOn(x, y))
					{
						Data::GetLabels().Items.at(5)->Texture().SetColor(255, 0, 0);
						isHoover = true;
					}
					else
						Data::GetLabels().Items.at(5)->Texture().SetColor(NormalColor);
					if (!isHoover && equipmentList.second.at(4) != nullptr && Data::GetLabels().Items.at(6)->IsMouseOn(x, y))
					{
						Data::GetLabels().Items.at(6)->Texture().SetColor(255, 0, 0);
						isHoover = true;
					}
					else
						Data::GetLabels().Items.at(6)->Texture().SetColor(NormalColor);
					if (!isHoover && equipmentList.second.at(5) != nullptr && Data::GetLabels().Items.at(7)->IsMouseOn(x, y))
					{
						Data::GetLabels().Items.at(7)->Texture().SetColor(255, 0, 0);
						isHoover = true;
					}
					else
						Data::GetLabels().Items.at(7)->Texture().SetColor(NormalColor);
					if (rightClick)
					{
						if (!isHoover && Data::GetLabels().Mini.at(5)->IsMouseOn(x, y))
						{
							Data::GetLabels().Mini.at(5)->Texture().SetColor(HooverColor);
							isHoover = true;
						}
						else
							Data::GetLabels().Mini.at(5)->Texture().SetColor(NormalColor);
						if (!isHoover && Data::GetLabels().Mini.at(6)->IsMouseOn(x, y))
						{
							Data::GetLabels().Mini.at(6)->Texture().SetColor(HooverColor);
							isHoover = true;
						}
						else
							Data::GetLabels().Mini.at(6)->Texture().SetColor(NormalColor);
						if (isRing)
						{
							if (!isHoover && Data::GetLabels().Mini.at(8)->IsMouseOn(x, y))
							{
								Data::GetLabels().Mini.at(8)->Texture().SetColor(HooverColor);
								isHoover = true;
							}
							else
								Data::GetLabels().Mini.at(8)->Texture().SetColor(NormalColor);
							if (!isHoover && Data::GetLabels().Mini.at(9)->IsMouseOn(x, y))
							{
								Data::GetLabels().Mini.at(9)->Texture().SetColor(HooverColor);
								isHoover = true;
							}
							else
								Data::GetLabels().Mini.at(9)->Texture().SetColor(NormalColor);
						}
						else
						{
							if (!isHoover && Data::GetLabels().Mini.at(7)->IsMouseOn(x, y))
							{
								Data::GetLabels().Mini.at(7)->Texture().SetColor(HooverColor);
								isHoover = true;
							}
							else
								Data::GetLabels().Mini.at(7)->Texture().SetColor(NormalColor);
						}
					}
					for (size_t i = 0, current = 0 + itemCardNumber * 42, Max = itemList.size(); i < 6 && current < Max; ++i)
						for (size_t j = 0; j < 7 && current < Max; ++j, ++current)
						{
							if (!isHoover && x >= 852 + int(j) * 131 && x <= 971 + int(j) * 131 && y >= 248 + int(i) * 125 && y <= 354 + int(i) * 125)
							{
								itemList.at(current).second->Texture().SetColor(HooverColor);
								isHoover = true;
							}
							else
								itemList.at(current).second->Texture().SetColor(NormalColor);
						}
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					SDL_GetMouseState(&X, &Y);
					if (e.button.button == SDL_BUTTON_RIGHT)
					{
						rightClick = false;
						for (size_t i = 0, current = 0 + itemCardNumber * 42, Max = itemList.size(); i < 6 && current < Max && !rightClick; ++i)
							for (size_t j = 0; j < 7 && current < Max; ++j, ++current)
							{
								if (X >= 852 + int(j) * 131 && X <= 971 + int(j) * 131 && Y >= 248 + int(i) * 125 && Y <= 354 + int(i) * 125)
								{
									itemName = itemList.at(current).first;
									itemNumber = current;
									rightClick = true;
									Data::GetLabels().Mini.at(4)->SetPosition(X, Y); //back
									Data::GetLabels().Mini.at(5)->SetPosition(X, Y); //info
									Data::GetLabels().Mini.at(6)->SetPosition(X, Y + 64); // throw
									Data::GetLabels().Mini.at(7)->SetPosition(X, Y + 128); //use/wear/takeoff
									Data::GetLabels().Mini.at(8)->SetPosition(X, Y + 192); //left
									Data::GetLabels().Mini.at(9)->SetPosition(X, Y + 256); //right
									switch (itemList.at(itemNumber).second->ItemType())
									{
									case eArmor:
									{
										if (((Armor*)(itemList.at(itemNumber).second))->Part() == Ring)
											isRing = true;
									}
									case eWeapon:
									{
										Data::GetLabels().Mini.at(7)->Texture().LoadText("Wear", NormalColor);
										break;
									}
									default:
									{
										Data::GetLabels().Mini.at(7)->Texture().LoadText("Use", NormalColor);
										break;
									}
									}
									break;
								}
								else
								{
									itemName = "";
									rightClick = false;
								}
							}
					}
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						if (rightClick)
						{
							if (Data::GetLabels().Mini.at(5)->IsMouseOn(X, Y))
								ItemInfo(itemList.at(itemNumber).second, player);
							else if (Data::GetLabels().Mini.at(6)->IsMouseOn(X, Y))
							{
								player->ThrowOut(itemName);
								player->ListOfItems(itemList);
							}
							else if (!isRing && Data::GetLabels().Mini.at(7)->IsMouseOn(X, Y))
							{
								if (itemList.at(itemNumber).second->ItemType() == eArmor || itemList.at(itemNumber).second->ItemType() == eWeapon)
								{
									player->Wear(itemName);
									player->ListOfItems(itemList);
									player->ListOfEquipment(equipmentList);
								}
								else
								{
									player->Use(itemName);
									player->ListOfItems(itemList);
								}
							}
							else if (isRing && Data::GetLabels().Mini.at(8)->IsMouseOn(X, Y))
							{
								player->Wear(itemName, 0);
								player->ListOfItems(itemList);
								player->ListOfEquipment(equipmentList);
							}
							else if (isRing && Data::GetLabels().Mini.at(9)->IsMouseOn(X, Y))
							{
								player->Wear(itemName, 1);
								player->ListOfItems(itemList);
								player->ListOfEquipment(equipmentList);
							}
							isRing = rightClick = false;
						}
						if (Data::GetLabels().Mini.at(10)->IsMouseOn(X, Y))
						{
							Data::GetLabels().Mini.at(10)->Texture().SetColor(NormalColor);
							return PlayerStatistics(player);
						}
						if (equipmentList.second.at(0) != nullptr && Data::GetLabels().Items.at(0)->IsMouseOn(X, Y))
						{
							player->TakeOff(Necklace);
							player->ListOfEquipment(equipmentList);
							player->ListOfItems(itemList);
						}
						if (equipmentList.second.at(1) != nullptr && Data::GetLabels().Items.at(1)->IsMouseOn(X, Y))
						{
							player->TakeOff(Ring, 0);
							player->ListOfEquipment(equipmentList);
							player->ListOfItems(itemList);
						}
						if (equipmentList.second.at(3) != nullptr && Data::GetLabels().Items.at(2)->IsMouseOn(X, Y))
						{
							player->TakeOff(Head);
							player->ListOfEquipment(equipmentList);
							player->ListOfItems(itemList);
						}
						if (equipmentList.second.at(6) != nullptr && Data::GetLabels().Items.at(3)->IsMouseOn(X, Y))
						{
							player->TakeOff(Hands);
							player->ListOfEquipment(equipmentList);
							player->ListOfItems(itemList);
						}
						if (equipmentList.first != nullptr && Data::GetLabels().Items.at(4)->IsMouseOn(X, Y))
						{
							player->TakeOff(WeaponE);
							player->ListOfEquipment(equipmentList);
							player->ListOfItems(itemList);
						}
						if (equipmentList.second.at(2) != nullptr && Data::GetLabels().Items.at(5)->IsMouseOn(X, Y))
						{
							player->TakeOff(Ring, 1);
							player->ListOfEquipment(equipmentList);
							player->ListOfItems(itemList);
						}
						if (equipmentList.second.at(4) != nullptr && Data::GetLabels().Items.at(6)->IsMouseOn(X, Y))
						{
							player->TakeOff(Chest);
							player->ListOfEquipment(equipmentList);
							player->ListOfItems(itemList);
						}
						if (equipmentList.second.at(5) != nullptr && Data::GetLabels().Items.at(7)->IsMouseOn(X, Y))
						{
							player->TakeOff(Leg);
							player->ListOfEquipment(equipmentList);
							player->ListOfItems(itemList);
						}
					}
					break;
				}
				}
			}
			if (Events::EventHandler::GetKeyboard().GetKeyState(SDLK_ESCAPE))
			{
				Events::EventHandler::GetKeyboard().SetKeyState(SDLK_ESCAPE, false);
				break;
			}
		}
	}
}