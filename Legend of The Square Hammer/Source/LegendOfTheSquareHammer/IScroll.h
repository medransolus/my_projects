/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Item.h"
namespace LegendOfTheSquareHammer::Renders::Items::Magic
{
	__interface IScroll
	{
		short ManaCost() const;
		MainStats MinimumStatistics() const;
	};
}