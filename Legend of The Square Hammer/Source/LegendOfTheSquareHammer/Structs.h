/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include <fstream>
using std::ofstream;
using std::ifstream;
namespace LegendOfTheSquareHammer
{
	enum BodyPart { WeaponE, Necklace, Ring, Head, Chest, Leg, Hands };
	enum WeaponType { Axe, Hammer, Sword, Dagger };
	enum ItemType { eAttackS, eEffectS, eSpellS, ePotion, eStatsPot, eArmor, eWeapon, eItem };
	enum Direction { NONE = 0, N = 1, E = 2, W = 4, S = 8, ENTER = 16 };

	namespace Renders::Objects
	{
		class Building;
		class Save;
	}

	struct CollisionEntrance
	{
		short Direction = 0;
		Renders::Objects::Building * Building = nullptr;
	};

	struct Damage
	{
		int Normal = 0;
		int Fire = 0;
		int Ice = 0;
		int Magic = 0;
		int Darkness = 0;

		friend ofstream & operator<<(ofstream & fout, const Damage & damage);
		friend ifstream & operator>>(ifstream & fin, Damage & damage);
	};

	struct MainStats
	{
		int Inteligence = 0;
		int Strenght = 0;
		int Dextermity = 0;

		inline MainStats operator*(int x) const { return { Inteligence * x, Strenght * x, Dextermity * x }; }
		inline MainStats operator+(const MainStats & mainStatistics) const { return { Inteligence + mainStatistics.Inteligence, Strenght + mainStatistics.Strenght, Dextermity + mainStatistics.Dextermity }; }
		inline MainStats operator-(const MainStats & mainStatistics) const { return *this + mainStatistics * -1; }
		inline bool operator>=(const MainStats & mainStatistics) { return (Inteligence >= mainStatistics.Inteligence && Strenght >= mainStatistics.Strenght && Dextermity >= mainStatistics.Dextermity); }

		MainStats & operator+=(const MainStats & mainStatistics);

		friend ofstream & operator<<(ofstream & fout, const MainStats & mainStatistics);
		friend ifstream & operator>>(ifstream & fin, MainStats & mainStatistics);
	};

	struct Stats
	{
		MainStats MainStatistics;
		int Endurance = 0;
		int Vitality = 0;
		int Armor = 0;
		short FireResistance = 0;
		short IceResistance = 0;
		short MagicResistance = 0;
		short DarknessResistance = 0;

		void LevelUp(const Stats & st);

		inline Stats operator*(short x) const { return { MainStatistics * x, Endurance * x, Vitality * x, Armor * x, FireResistance * x, IceResistance * x, MagicResistance * x, DarknessResistance * x }; }
		inline Stats & operator-=(const Stats & statistics) { return *this += statistics * -1; }
		inline Stats operator+(const Stats & statistics) const { return { MainStatistics + statistics.MainStatistics, Endurance + statistics.Endurance, Vitality + statistics.Vitality, Armor + statistics.Armor, FireResistance + statistics.FireResistance, IceResistance + statistics.IceResistance, MagicResistance + statistics.MagicResistance, DarknessResistance + statistics.DarknessResistance }; }
		inline Stats operator-(const Stats & statistics) const { return *this + statistics * -1; }

		Stats & operator+=(const Stats & statistics);

		friend ofstream & operator<<(ofstream & fout, const Stats & statistics);
		friend ifstream & operator>>(ifstream & fin, Stats & statistics);
	};
}