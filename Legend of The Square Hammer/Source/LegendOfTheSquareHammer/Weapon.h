/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Item.h"
namespace LegendOfTheSquareHammer::Renders::Items
{
	class Weapon : public Item
	{
		WeaponType type = Axe;
		Damage damage;
		short criticalChance = 0;
		short penetration = 0;
		MainStats minimumStatistics;
		Stats statistics;
	public:
		Weapon() {}
		Weapon(WeaponType newType, const string & newName, const string & newDescription, const Damage & weaponDamage, short critical, short armorPenetration, size_t levelRequired, const MainStats & statisticsRequired, const Stats & newStatistics, const string & path, int x, int y) : Item(newName, newDescription, levelRequired, path, x, y), type(newType), damage(weaponDamage), criticalChance(critical), penetration(armorPenetration), minimumStatistics(statisticsRequired), statistics(newStatistics) {}

		inline Damage Damage() const { return damage; }
		inline short CriticalChance() const { return criticalChance; }
		inline short ArmorPenetration() const { return penetration; }
		inline MainStats MinimumStatistics() const { return minimumStatistics; }
		inline Stats Statistics() const { return statistics; }
		inline WeaponType Type() const { return type; }

		virtual LegendOfTheSquareHammer::ItemType ItemType() const { return eWeapon; }
		virtual ofstream & Write(ofstream & fout) const { return fout << *this; }
		virtual ifstream & Read(ifstream & fin) { return fin >> *this; }

		friend ofstream & operator<<(ofstream & fout, const Weapon & weapon);
		friend ifstream & operator>>(ifstream & fin, Weapon & weapon);
	};
}