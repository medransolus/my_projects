/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Save.h"
using LegendOfTheSquareHammer::Renders::Objects::IObject;
namespace LegendOfTheSquareHammer
{
	extern const size_t FIELD_WIDTH;
	extern const size_t FIELD_HEIGHT;

	class Game
	{
		bool isNewLevel = false;
		int movement = 5;
		short quit = 0;
		Save * gameSave = nullptr;

		static short CheckCollision(const IObject * object, const NpcMap * monsters);
		static CollisionEntrance CheckCollision(const IObject * object, const BuildingsMap * buildings);
		void Attack(int x, int y, const string & place);
		void RenderOrder(const string & place);
		void MoveObjects(const string & place, CollisionEntrance & collision, int * x, int * y, int fieldWidth = FIELD_WIDTH, int fieldHeight = FIELD_HEIGHT);
		void CheckDeath(NpcMap * monsters);
		void GetItems(PickUpMap * items);
		void EnterBuilding(const string & name);
		void PauseMenu(bool gameOver = false);
		void Castle();
	public:
		Game(Player * newPlayer);
		Game(const string & path, string & startingPlace);

		void Play(const string & place = "main");

		~Game();
	};
}