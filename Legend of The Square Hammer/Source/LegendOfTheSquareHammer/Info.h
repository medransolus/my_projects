/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Player.h"
#include "Data.h"
#include "EventHandler.h"
#include "StatsPotion.h"
#include "AttackScroll.h"
#include "EffectScroll.h"
#include "SpellScroll.h"
using std::to_string;
using LegendOfTheSquareHammer::ProgramData::Data;
using namespace LegendOfTheSquareHammer::Renders::Objects;
using namespace LegendOfTheSquareHammer::Renders::Items;
namespace LegendOfTheSquareHammer
{
	extern SDL_Renderer * Renderer;
	extern SDL_Color HooverColor;
	extern SDL_Color NormalColor;

	class Info
	{
		static void ArmorInfo(Armor * item, const Stats & statistics);
		static void WeaponInfo(Renders::Items::Weapon * item, const Stats & statistics);
		static void PotionInfo(Renders::Items::SingleUse::Potion * item);
		static void SetStatsLabels(const Stats & currentStatistics, const Damage & currentDamage);

		Info() {}
	public:
		static void ItemInfo(Item * item, const Renders::Objects::Player * player);
		static void PlayerStatistics(Player * player);
		static void PlayerEquipment(Player * player);
	};
}