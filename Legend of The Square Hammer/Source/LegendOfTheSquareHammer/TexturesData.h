/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include <vector>
#include "Texture.h"
using LegendOfTheSquareHammer::Renders::Texture;
namespace LegendOfTheSquareHammer::ProgramData
{
	struct TexturesData
	{
		std::vector<Texture *> CodeMenuBackground;
		std::vector<Texture *> MenuBackground;
		Texture Title;
		Texture Background;
		Texture OutFloor;
	};
}