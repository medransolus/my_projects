/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "IRenderable.h"
namespace LegendOfTheSquareHammer::Renders::Objects
{
	__interface IObject : public IRenderable
	{
		size_t AbsoluteX() const;
		size_t AbsoluteY() const;
		void SetAbsolutePosition(size_t x, size_t y);
		void ChangeAbsolutePosition(int x, int y);
		SDL_Rect Collision() const;
		virtual void Render();
	};
}