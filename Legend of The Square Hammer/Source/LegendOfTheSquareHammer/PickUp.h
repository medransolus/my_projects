/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Player.h"
namespace LegendOfTheSquareHammer::Renders::Objects
{
	class PickUp : public IObject
	{
		Items::Item * item = nullptr;
		size_t X = 0;
		size_t Y = 0;
		SDL_Rect collision;
	public:
		PickUp() {}
		PickUp(size_t absoluteX, size_t absoluteY, Items::Item * loot) : X(absoluteX), Y(absoluteY), item(loot) { collision = item->Position(); }

		inline size_t AbsoluteX() const { return X; }
		inline size_t AbsoluteY() const { return Y; }
		inline SDL_Rect Position() const { return item->Position(); }
		inline SDL_Rect Collision() const { return collision; }
		inline Renders::Texture & Texture() { return item->Texture(); }
		inline Items::Item * GetItem() { return item; }

		string Name() const;
		void SetPosition(int x, int y);
		void ChangePosition(int x, int y);
		void SetAbsolutePosition(size_t x, size_t y);
		void ChangeAbsolutePosition(int x, int y);
		virtual void Render();

		void operator()(Player & player);
		friend ofstream & operator<<(ofstream & fout, const PickUp & pickUp);
		friend ifstream & operator>>(ifstream & fin, PickUp & pickUp);

		~PickUp();
	};
}