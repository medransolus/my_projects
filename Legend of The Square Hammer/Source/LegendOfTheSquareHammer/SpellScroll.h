/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "IScroll.h"
namespace LegendOfTheSquareHammer::Renders::Items::Magic
{
	class SpellScroll : public Item, public IScroll
	{
		short manaCost = 0;
		short duration = 0;
		Damage damageOverTime;
		Stats statisticsChangeOverTime;
		MainStats minimumStatistics;
	public:
		SpellScroll() {}
		SpellScroll(const string & newName, const string & newDescription, short Mana, const Damage & damage, const Stats & statistics, size_t levelRequired, const MainStats & statisticsRequired, const string & path, int x, int y) : Item(newName, newDescription, levelRequired, path, x, y), manaCost(Mana), damageOverTime(damage), statisticsChangeOverTime(statistics), minimumStatistics(statisticsRequired) {}

		inline short ManaCost() const { return manaCost; }
		inline short Duration() const { return duration; }
		inline Damage Damage() const { return damageOverTime; }
		inline Stats StatisticsChange() const { return statisticsChangeOverTime; }
		inline MainStats MinimumStatistics() const { return minimumStatistics; }

		virtual LegendOfTheSquareHammer::ItemType ItemType() const { return eSpellS; }
		virtual ofstream & Write(ofstream & fout) const { return fout << *this; }
		virtual ifstream & Read(ifstream & fin) { return fin >> *this; }

		friend ofstream & operator<<(ofstream & fout, const SpellScroll & scroll);
		friend ifstream & operator>>(ifstream & fin, SpellScroll & scroll);
	};
}