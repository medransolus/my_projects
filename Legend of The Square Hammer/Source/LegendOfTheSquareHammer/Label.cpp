/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "stdafx.h"
#include <map>
#include "Label.h"
using std::map;
namespace LegendOfTheSquareHammer 
{
	extern map<string, void(*)()> MenuOptions;
}
namespace LegendOfTheSquareHammer::Renders
{
	Label::Label(const SDL_Rect & newPosition, const string & newPath, const string & callFunction)
	{
		path = newPath;
		if (!texture.LoadFile(path))
			exit(0b1000000000);
		position = newPosition;
		if (position.w == 0 && position.h == 0)
		{
			position.w = texture.Width();
			position.h = texture.Height();
		}
		function = callFunction;
	}

	void Label::operator()()
	{
		map<string, void(*)()>::iterator iterator = MenuOptions.find(function);
		if (iterator != MenuOptions.end())
			iterator->second();
		else
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "Cannot find and call function: " << function << endl;
			fout.close();
		}
	}

	ofstream & operator<<(ofstream & fout, const Label & label)
	{
		fout << label.position << endl;
		if (label.texture.IsText())
			fout << 1 << ' ' << label.texture.TextColor().r << ' ' << label.texture.TextColor().g << ' ' << label.texture.TextColor().b << ' ' << label.texture.TextColor().a << endl << label.texture.Text();
		else
			fout << 0;
		fout << endl << label.path << endl << label.function << endl;
		return fout;
	}

	ifstream & operator>>(ifstream & fin, Label & label)
	{
		fin >> label.position;
		fin.ignore();
		string text = "";
		SDL_Color textColor;
		int isText = 0;
		fin >> isText;
		if (isText)
		{
			fin >> textColor.r >> textColor.g >> textColor.b >> textColor.a;
			fin.ignore();
			getline(fin, text);
		}
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, label.path);
		getline(fin, label.function);
		if (!label.texture.LoadFile(label.path))
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "LoadObject error: cannot load file " << label.path << endl;
			fout.close();
			exit(0b10000000000);
		}
		if (text.size() && !label.texture.LoadText(text, textColor))
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "LoadObject error: cannot load text " << text << endl;
			fout.close();
			exit(0b100000000000);
		}
		return fin;
	}
}