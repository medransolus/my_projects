/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "ISingleUse.h"
namespace LegendOfTheSquareHammer::Renders::Items::SingleUse
{
	class Potion : public Item, public ISingleUse
	{
		int mana = 0;
		int health = 0;
	public:
		Potion() {}
		Potion(const string & newName, const string & newDescription, int newMana, int newHealth, size_t levelRequired, const string & path, int x, int y) : Item(newName, newDescription, levelRequired, path, x, y), mana(newMana), health(newHealth) {}

		inline int Mana() const { return mana; }
		inline int Health() const { return health; }

		virtual LegendOfTheSquareHammer::ItemType ItemType() const { return ePotion; }
		virtual ofstream & Write(ofstream & fout) const { return fout << *this; }
		virtual ifstream & Read(ifstream & fin) { return fin >> *this; }

		friend ofstream & operator<<(ofstream & fout, const Potion & potion);
		friend ifstream & operator>>(ifstream & fin, Potion & potion);
	};
}