/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "IObject.h"
#include "Structs.h"
namespace LegendOfTheSquareHammer::Renders::Objects
{
	class Building : public IObject
	{
		SDL_Rect position;
		SDL_Rect collision;
		SDL_Rect entrance;
		size_t colliderDown = 0;
		size_t colliderRight = 0;
		size_t entranceDown = 0;
		size_t entranceRight = 0;
		Renders::Texture texture;
		size_t X = 0;
		size_t Y = 0;
		string buildingName = "none";
	public:
		Building() {}
		Building(double scale, size_t absoluteX, size_t absoluteY, int x, int y, const SDL_Rect & collider, const SDL_Rect & entrancePosition, const string & path, const string & newBuildingName = "none");
		
		inline SDL_Rect Position() const { return position; }
		inline SDL_Rect Collision() const { return collision; }
		inline Renders::Texture & Texture() { return texture; }
		inline size_t AbsoluteX() const { return X; }
		inline size_t AbsoluteY() const { return Y; }
		inline string Name() const { return buildingName; }

		void SetPosition(int x, int y);
		void ChangePosition(int x, int y);
		void SetAbsolutePosition(size_t x, size_t y);
		void ChangeAbsolutePosition(int x, int y);
		virtual void Render();
		CollisionEntrance CollisionDirection(const IObject * object);

		friend ofstream & operator<<(ofstream & fout, const Building & building);
		friend ifstream & operator>>(ifstream & fin, Building & building);
	};
}