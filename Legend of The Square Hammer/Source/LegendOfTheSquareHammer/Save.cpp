/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "Save.h"
#include <filesystem>
#include <Windows.h>
#include "StatsPotion.h"
using namespace LegendOfTheSquareHammer::Renders::Items;
namespace fileSystem = std::experimental::filesystem;
namespace LegendOfTheSquareHammer
{
	Save::Save(LegendOfTheSquareHammer::Renders::Objects::Player * newPlayer)
	{
		player = newPlayer;
		newPlayer = nullptr;
		buildings.insert(pair<const string, BuildingsMap *>("main", new BuildingsMap));
		buildings.at("main")->insert(pair<const string, Building *>("castle", new Building(3, 0, 0, -1095, -1534, { 0,104,426,350 }, { 195,434,35,20 }, "Data/Textures/Castle.png", "castle")));
		buildings.insert(pair<const string, BuildingsMap *>("castle", new BuildingsMap));
		buildings.at("castle")->insert(pair<const string, Building *>("wallUp", new Building(1, 0, 40, 0, 40, { 0,0,1920,115 }, { 0,0,0,0 }, "Data/Textures/Blank.png")));
		buildings.at("castle")->insert(pair<const string, Building *>("wallLeft", new Building(1, 161, 0, 161, 0, { 0,0,100,1080 }, { 0,0,0,0 }, "Data/Textures/Blank.png")));
		buildings.at("castle")->insert(pair<const string, Building *>("wallRight", new Building(1, 1659, 0, 1659, 0, { 0,0,100,1080 }, { 0,0,0,0 }, "Data/Textures/Blank.png")));
		buildings.at("castle")->insert(pair<const string, Building *>("wallDownLeft", new Building(1, 0, 639, 0, 839, { 0,0,862,201 }, { 0,0,0,0 }, "Data/Textures/Blank.png")));
		buildings.at("castle")->insert(pair<const string, Building *>("wallDownRight", new Building(1, 1059, 839, 1059, 839, { 0,0,862,201 }, { 0,0,0,0 }, "Data/Textures/Blank.png")));
		buildings.at("castle")->insert(pair<const string, Building *>("wallDown", new Building(1, 0, 1039, 0, 1039, { 0,0,1920,41 }, { 760,0,400,20 }, "Data/Textures/Blank.png")));
		monsters.insert(pair<const string, NpcMap *>("main", new NpcMap));
		monsters.at("main")->insert(pair<const string, Npc *>("roger", new Npc(2.5, 2000, 3000, 905, 1466, 201, 30, 500, { 0,56,44,12 }, { 15,0,0,0,0 }, { 5,0,0,0,0 }, "Data/Textures/Roger.png", new PickUp(0, 0, new Armor(Hands, "Best of gloves", "Best what you can get for that price", { 4,5,6,3,5,2,1,6,7,9 }, 1, { 0,0,0 }, "Data/Textures/BestGloves.png", 0, 0)))));
		monsters.insert(pair<const string, NpcMap *>("castle", new NpcMap));
		items.insert(pair<const string, PickUpMap *>("main", new PickUpMap));
		items.at("main")->insert(pair<const string, PickUp *>("Health potion", new PickUp(2500, 4500, new SingleUse::Potion("Minor health potion", "Immediatelly regenerates life but cost your mana.", -5, 10, 1, "Data/Textures/HPotion.png", 1405, 2966))));
		items.at("main")->insert(pair<const string, PickUp *>("Mana potion", new PickUp(2500, 500, new SingleUse::Potion("Minor mana potion", "Immediatelly regenerates mana but cost your life.", 10, -5, 1, "Data/Textures/MPotion.png", 1405, -1034))));
		items.insert(pair<const string, PickUpMap *>("castle", new PickUpMap));
		items.at("castle")->insert(pair<const string, PickUp *>("Sword of Doom", new PickUp(900, 500, new Weapon(Sword, "Sword of Doom", "Sword wield by the almighty God Emperor of Manikind. All Imperial citizens dreams of seeing this sword and you just slashing it from left to right. The Emperor protects!", { 15,9,0,0,0 }, 20, 10, 2, { 0,16,0 }, { 2,6,1,5,7,1,1,1,1,1 }, "Data/Textures/DoomSword.png", 900, 500))));
	}

	Save::Save(const string & path, string & place)
	{
		ifstream fin(path);
		if (!fin.good())
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "Cannot open save file: " << path << endl;
			fout.close();
			exit(0b10000000);
		}
		getline(fin, place);
		player = new LegendOfTheSquareHammer::Renders::Objects::Player;
		fin >> *player;
		string placeName = "", objectName = "";
		size_t count = 0;
		fin >> count;
		for (size_t i = 0, J; i < count; ++i)
		{
			if (fin.peek() == 10)
				fin.ignore();
			getline(fin, placeName);
			items.insert(pair<const string, PickUpMap *>(placeName, new PickUpMap));
			fin >> J;
			for (size_t j = 0; j < J; ++j)
			{
				if (fin.peek() == 10)
					fin.ignore();
				getline(fin, objectName);
				items.at(placeName)->insert(pair<const string, PickUp *>(objectName, new PickUp));
				fin >> *items.at(placeName)->at(objectName);
				objectName = "";
			}
			placeName = "";
		}
		fin >> count;
		for (size_t i = 0, J; i < count; ++i)
		{
			if (fin.peek() == 10)
				fin.ignore();
			getline(fin, placeName);
			buildings.insert(pair<const string, BuildingsMap *>(placeName, new BuildingsMap));
			fin >> J;
			for (size_t j = 0; j < J; ++j)
			{
				if (fin.peek() == 10)
					fin.ignore();
				getline(fin, objectName);
				buildings.at(placeName)->insert(pair<const string, Building *>(objectName, new Building));
				fin >> *buildings.at(placeName)->at(objectName);
				objectName = "";
			}
			placeName = "";
		}
		fin >> count;
		for (size_t i = 0, J; i < count; ++i)
		{
			if (fin.peek() == 10)
				fin.ignore();
			getline(fin, placeName);
			monsters.insert(pair<const string, NpcMap *>(placeName, new NpcMap));
			fin >> J;
			for (size_t j = 0; j < J; ++j)
			{
				if (fin.peek() == 10)
					fin.ignore();
				getline(fin, objectName);
				monsters.at(placeName)->insert(pair<const string, Npc *>(objectName, new Npc));
				fin >> *monsters.at(placeName)->at(objectName);
				objectName = "";
			}
			placeName = "";
		}
		fin.close();
	}

	string Save::Name() const
	{
		if (player != nullptr)
			return player->Name();
		return "Pusty";
	}

	bool Save::SaveGame(const string & place) const
	{
		if (!CreateDirectory((fileSystem::current_path().string() + "\\Data\\Save").c_str(), NULL) && ERROR_ALREADY_EXISTS != GetLastError())
		{
			ofstream efout("error.txt", ios_base::app);
			efout << "Cannot create directory Data/Save. Create it manually.";
			efout.close();
			return false;
		}
		ofstream fout("Data/Save/" + player->Name() + ".sav", ios_base::trunc);
		if (!fout.good())
		{
			ofstream efout("error.txt", ios_base::app);
			efout << "Cannot save: " << player->Name() << endl;
			efout.close();
			return false;
		}
		fout << place << endl;
		fout << *player;
		fout << items.size() << endl;
		for_each(items.begin(), items.end(), [&fout](const pair<const string, PickUpMap *> & x)
		{
			fout << x.first << endl << x.second->size() << endl;
			for_each(x.second->begin(), x.second->end(), [&fout](const pair<const string, PickUp *> & y) { fout << y.first << endl; fout << *y.second; });
		});
		fout << buildings.size() << endl;
		for_each(buildings.begin(), buildings.end(), [&fout](const pair<const string, BuildingsMap *> & x)
		{
			fout << x.first << endl << x.second->size() << endl;
			for_each(x.second->begin(), x.second->end(), [&fout](const pair<const string, Building *> & y) { fout << y.first << endl; fout << *y.second; });
		});
		fout << monsters.size() << endl;
		for_each(monsters.begin(), monsters.end(), [&fout](const pair<const string, NpcMap *> & x)
		{
			fout << x.first << endl << x.second->size() << endl;
			for_each(x.second->begin(), x.second->end(), [&fout](const pair<const string, Npc *> & y) { fout << y.first << endl; fout << *y.second; });
		});
		fout.close();
		return true;
	}

	BuildingsMap * Save::Buildings(const string & key)
	{
		map<string, BuildingsMap *>::iterator iterator = buildings.find(key);
		if (iterator != buildings.end())
			return buildings.at(key);
		return nullptr;
	}

	NpcMap * Save::Monsters(const string & key)
	{
		map<string, NpcMap *>::iterator iterator = monsters.find(key);
		if (iterator != monsters.end())
			return monsters.at(key);
		return nullptr;
	}

	PickUpMap * Save::Items(const string & key)
	{
		map<string, PickUpMap *>::iterator iterator = items.find(key);
		if (iterator != items.end())
			return items.at(key);
		return nullptr;
	}

	Save::~Save()
	{
		if (player != nullptr)
			delete player;
		for_each(items.begin(), items.end(), [](pair<const string, PickUpMap *> & x) { for_each(x.second->begin(), x.second->end(), [](pair<const string, PickUp *> & y) { delete y.second; }); });
		for_each(buildings.begin(), buildings.end(), [](pair<const string, BuildingsMap *> & x) { for_each(x.second->begin(), x.second->end(), [](pair<const string, Building *> & y) { delete y.second; }); });
		for_each(monsters.begin(), monsters.end(), [](pair<const string, NpcMap *> & x) { for_each(x.second->begin(), x.second->end(), [](pair<const string, Npc *> & y) { delete y.second; }); });
	}
}