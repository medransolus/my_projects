/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "Data.h"
#include <algorithm>
namespace LegendOfTheSquareHammer::ProgramData
{
	void Data::DeleteData()
	{
		for_each(labelsData.Mini.begin(), labelsData.Mini.end(), [](Label * x) { delete x; });
		for_each(labelsData.Menu.begin(), labelsData.Menu.end(), [](Label * x) { delete x; });
		for_each(labelsData.Items.begin(), labelsData.Items.end(), [](Label * x) { delete x; });
		for_each(labelsData.Stats.begin(), labelsData.Stats.end(), [](Label * x) { delete x; });
		for_each(labelsData.Resistance.begin(), labelsData.Resistance.end(), [](Label * x) { delete x; });
		for_each(labelsData.Damage.begin(), labelsData.Damage.end(), [](Label * x) { delete x; });
		for_each(labelsData.Arrows.begin(), labelsData.Arrows.end(), [](std::pair<Label *, Label *> & x) { delete x.first; delete x.second; });
		for_each(labelsData.Info.begin(), labelsData.Info.end(), [](Label * x) { delete x; });
		for_each(labelsData.Armor.begin(), labelsData.Armor.end(), [](Label * x) { delete x; });
		for_each(labelsData.Weapon.begin(), labelsData.Weapon.end(), [](Label * x) { delete x; });
		for_each(labelsData.Potion.begin(), labelsData.Potion.end(), [](Label * x) { delete x; });
		for_each(texturesData.CodeMenuBackground.begin(), texturesData.CodeMenuBackground.end(), [](Texture * x) { delete x; });
		for_each(texturesData.MenuBackground.begin(), texturesData.MenuBackground.end(), [](Texture * x) { delete x; });
		for_each(saveLabels.begin(), saveLabels.end(), [](ProgramData::SavePair & x) { delete x.second.first; delete x.second.second; });
	}
}