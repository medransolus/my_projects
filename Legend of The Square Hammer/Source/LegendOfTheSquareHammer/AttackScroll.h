/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "IScroll.h"
namespace LegendOfTheSquareHammer::Renders::Items::Magic
{
	class AttackScroll : public Item, public IScroll
	{
		short manaCost = 0;
		Damage damage;
		MainStats minimumStatistics;
	public:
		AttackScroll() {}
		AttackScroll(const string & newName, const string & newDescription, short Mana, const Damage & newDamage, size_t levelRequired, const MainStats & statisticsRequired, const string & path, int x, int y) : Item(newName, newDescription, levelRequired, path, x, y), manaCost(Mana), damage(newDamage), minimumStatistics(statisticsRequired) {}

		inline short ManaCost() const { return manaCost; }
		inline Damage Damage() const { return damage; }
		inline MainStats MinimumStatistics() const { return minimumStatistics; }

		virtual LegendOfTheSquareHammer::ItemType ItemType() const { return eAttackS; }
		virtual ofstream & Write(ofstream & fout) const { return fout << *this; }
		virtual ifstream & Read(ifstream & fin) { return fin >> *this; }

		friend ofstream & operator<<(ofstream & fout, const AttackScroll & scroll);
		friend ifstream & operator>>(ifstream & fin, AttackScroll & scroll);
	};
}