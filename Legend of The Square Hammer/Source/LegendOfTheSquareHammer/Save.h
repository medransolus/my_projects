/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Building.h"
#include "Npc.h"
using namespace LegendOfTheSquareHammer::Renders::Objects;
namespace LegendOfTheSquareHammer
{
	typedef map<string, Building *> BuildingsMap;
	typedef map<string, Npc *> NpcMap;
	typedef map<string, PickUp *> PickUpMap;

	class Save
	{
		map<string, BuildingsMap *> buildings;
		map<string, NpcMap *> monsters;
		map<string, PickUpMap *> items;
		Player * player = nullptr;
	public:
		Save(Player * newPlayer);
		Save(const string & path, string & place);

		inline Player * Player() { return player; }

		string Name() const;
		bool SaveGame(const string & place) const;
		BuildingsMap * Buildings(const string & key);
		NpcMap * Monsters(const string & key);
		PickUpMap * Items(const string & key);

		~Save();
	};
}