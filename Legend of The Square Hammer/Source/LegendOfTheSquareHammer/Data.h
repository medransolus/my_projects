/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "LabelsData.h"
#include "TexturesData.h"
namespace LegendOfTheSquareHammer::ProgramData
{
	typedef std::pair<string, std::pair<Label *, Label *>> SavePair;

	class Data
	{
		static inline LabelsData labelsData;
		static inline TexturesData texturesData;
		static inline vector<SavePair> saveLabels;

		Data() {}
	public:
		static inline LabelsData & GetLabels() { return labelsData; }
		static inline TexturesData & GetTextures() { return texturesData; }
		static inline vector<SavePair> & GetSaveLabels() { return saveLabels; }

		static void DeleteData();
	};
	
}