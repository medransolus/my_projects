/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "IRenderable.h"
#include "Structs.h"
namespace LegendOfTheSquareHammer::Renders::Items
{
	class Item : public IRenderable
	{
	protected:
		string name = "item";
		string description = "none";
		size_t minimumLevel = 0;
		SDL_Rect position = { 0,0,100,100 };
		Renders::Texture texture;
	public:
		Item() {}
		Item(const string & newName, const string & newDescription, size_t levelRequired, const string & path, int x, int y);

		inline string Name() const { return name; }
		inline string Description() const { return description; }
		inline size_t MinimumLevel() const { return minimumLevel; }
		inline SDL_Rect Position() const { return position; }
		inline Renders::Texture & Texture() { return texture; }

		inline void SetPosition(int x, int y) { position.x = x; position.y = y; }
		inline void ChangePosition(int x, int y) { position.x += x; position.y += y; }

		virtual ItemType ItemType() const = 0;
		virtual ofstream & Write(ofstream & fout) const = 0;
		virtual ifstream & Read(ifstream & fin) = 0;
	};
}