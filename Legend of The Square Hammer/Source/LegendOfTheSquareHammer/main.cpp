/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include <ctime>
#include <sstream>
#include <filesystem>
#include <Windows.h>
#include <SDL_mixer.h>
#include "Data.h"
#include "EventHandler.h"
#include "Game.h"
using std::to_string;
using LegendOfTheSquareHammer::ProgramData::Data;
using LegendOfTheSquareHammer::Events::EventHandler;
using LegendOfTheSquareHammer::Renders::Texture;
namespace fileSystem = std::experimental::filesystem;
namespace LegendOfTheSquareHammer
{
	const size_t FIELD_WIDTH = 3000;
	const size_t FIELD_HEIGHT = 5000;
	SDL_Color HooverColor = { 242,243,108,255 };
	SDL_Color NormalColor = { 255,255,255,255 };
	bool KonamiCode = false;
	size_t ScreenWidth = 1920;
	size_t ScreenHeight = 1080;
	size_t CenterX = 0;
	size_t CenterY = 0;
	double ScreenScale = 1;
	SDL_Window * Window = nullptr;
	SDL_Surface * ScreenSurface = nullptr;
	SDL_Renderer * Renderer = nullptr;
	TTF_Font * Font = nullptr;
	Mix_Music * CodeMusic = nullptr;
	vector<SDL_Keycode> CodeKeys;
	vector<Mix_Music *> Music;
	Game * CurrentGame = nullptr;

	void Initialize();
	void Close();
	void LoadMedia();
	void Menu();
	void NewGame();
	void LoadGame();
	Player * CreatePlayer();
	void GetSaves();

	map<string, void(*)()> MenuOptions{ { "close",Close },{ "menu",Menu },{ "newGame",NewGame },{ "loadGame",LoadGame } };
}
	int main(int argc, char ** argv)
	{
		srand(size_t(time(NULL)));
		LegendOfTheSquareHammer::Initialize();
		LegendOfTheSquareHammer::LoadMedia();
		LegendOfTheSquareHammer::Menu();
		LegendOfTheSquareHammer::Close();
		return 0;
	}
namespace LegendOfTheSquareHammer
{
	void Initialize()
	{
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO))
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "SDL_Init error: " << SDL_GetError() << endl;
			fout.close();
			exit(0b1);
		}
		Window = SDL_CreateWindow("Legend of The Square Hammer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, ScreenWidth, ScreenHeight, SDL_WINDOW_FULLSCREEN);
		if (Window == nullptr)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "SDL_CreateWindow error: " << SDL_GetError() << endl;
			fout.close();
			exit(0b10);
		}
		if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "SDL_Image load error: " << IMG_GetError() << endl;
			fout.close();
			exit(0b100);
		}
		ScreenSurface = SDL_GetWindowSurface(Window);
		if (ScreenSurface == nullptr)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "SDL_GetWindowSurface error: " << SDL_GetError() << endl;
			fout.close();
			exit(0b1000);
		}
		Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE);
		if (Renderer == nullptr)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "SDL_CreateRenderer error: " << SDL_GetError() << endl;
			fout.close();
			exit(0b10000);
		}
		SDL_RenderSetLogicalSize(Renderer, ScreenWidth, ScreenHeight);
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "Mix_OpenAudio error: " << Mix_GetError() << endl;
			fout.close();
			exit(0b100000);
		}
		if (TTF_Init() == -1)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "TTF_Init error: " << SDL_GetError() << endl;
			fout.close();
			exit(0b1000000);
		}
		CodeKeys.push_back(SDLK_UP);
		CodeKeys.push_back(SDLK_UP);
		CodeKeys.push_back(SDLK_DOWN);
		CodeKeys.push_back(SDLK_DOWN);
		CodeKeys.push_back(SDLK_LEFT);
		CodeKeys.push_back(SDLK_RIGHT);
		CodeKeys.push_back(SDLK_LEFT);
		CodeKeys.push_back(SDLK_RIGHT);
		CodeKeys.push_back(SDLK_b);
		CodeKeys.push_back(SDLK_a);
	}

	void Close()
	{
		Data::DeleteData();
		for_each(Music.begin(), Music.end(), [](Mix_Music * x) { Mix_FreeMusic(x); });		
		Mix_FreeMusic(CodeMusic);
		SDL_DestroyRenderer(Renderer);
		TTF_CloseFont(Font);
		SDL_FreeSurface(ScreenSurface);
		SDL_DestroyWindow(Window);
		TTF_Quit();
		Mix_Quit();
		IMG_Quit();
		SDL_Quit();
		exit(0);
	}

	void LoadMedia()
	{
		Data::GetTextures().Background.LoadFile("Data/Textures/LoadBackground.png");
		Data::GetTextures().Background.SetBlendMode(SDL_BLENDMODE_BLEND);
		Data::GetTextures().Background.Render();
		SDL_RenderPresent(Renderer);
#pragma region Music
		Music.push_back(Mix_LoadMUS("Data/Music/Square Hammer.mp3"));
		Mix_PlayMusic(Music.at(0), -1);
		Music.push_back(Mix_LoadMUS("Data/Music/Absolution.mp3"));
		Music.push_back(Mix_LoadMUS("Data/Music/Cirice.mp3"));
		Music.push_back(Mix_LoadMUS("Data/Music/From the Pinnacle to the Pit.mp3"));
		Music.push_back(Mix_LoadMUS("Data/Music/Ghuleh Zombie Queen.mp3"));
		Music.push_back(Mix_LoadMUS("Data/Music/He Is.mp3"));
		Music.push_back(Mix_LoadMUS("Data/Music/Infestissumam.mp3"));
		Music.push_back(Mix_LoadMUS("Data/Music/Majesty.mp3"));
		Music.push_back(Mix_LoadMUS("Data/Music/Monstrance Clock.mp3"));
		Music.push_back(Mix_LoadMUS("Data/Music/Ritual.mp3"));
		Music.push_back(Mix_LoadMUS("Data/Music/Year Zero.mp3"));
		CodeMusic = Mix_LoadMUS("Data/Music/Sax.mp3");
#pragma endregion
		Font = TTF_OpenFont("Data/SEASRN.ttf", 200);
#pragma region Textures
		Data::GetTextures().Title.LoadFile("Data/Textures/Title.png");
		Data::GetTextures().OutFloor.LoadFile("Data/Textures/OutFloor.png");
		ifstream fin("Data/Objects/Menu.ttr");
		size_t I = 0;
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetTextures().MenuBackground.push_back(new Texture);
			fin >> *Data::GetTextures().MenuBackground.back();
		}
		fin.close();
		I = 0;
		fin.open("Data/Objects/Sax.ttr");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetTextures().CodeMenuBackground.push_back(new Texture);
			fin >> *Data::GetTextures().CodeMenuBackground.back();
		}
		fin.close();
#pragma endregion
#pragma region Labels
		fin.open("Data/Objects/Menu.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Menu.push_back(new Label);
			fin >> *Data::GetLabels().Menu.back();
		}
		fin.close();
		fin.open("Data/Objects/Item.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Items.push_back(new Label);
			fin >> *Data::GetLabels().Items.back();
		}
		fin.close();
		fin.open("Data/Objects/Stats.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Stats.push_back(new Label);
			fin >> *Data::GetLabels().Stats.back();
		}
		fin.close();
		fin.open("Data/Objects/Resistance.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Resistance.push_back(new Label);
			fin >> *Data::GetLabels().Resistance.back();
		}
		fin.close();
		fin.open("Data/Objects/Damage.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Damage.push_back(new Label);
			fin >> *Data::GetLabels().Damage.back();
		}
		fin.close();
		fin.open("Data/Objects/Mini.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Mini.push_back(new Label);
			fin >> *Data::GetLabels().Mini.back();
		}
		fin.close();
		fin.open("Data/Objects/Info.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Info.push_back(new Label);
			fin >> *Data::GetLabels().Info.back();
		}
		fin.close();
		fin.open("Data/Objects/Armor.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Armor.push_back(new Label);
			fin >> *Data::GetLabels().Armor.back();
		}
		fin.close();
		fin.open("Data/Objects/Weapon.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Weapon.push_back(new Label);
			fin >> *Data::GetLabels().Weapon.back();
		}
		fin.close();
		fin.open("Data/Objects/Potion.lab");
		fin >> I;
		for (size_t i = 0; i < I; ++i)
		{
			Data::GetLabels().Potion.push_back(new Label);
			fin >> *Data::GetLabels().Potion.back();
		}
		fin.close();
		Data::GetSaveLabels().push_back(ProgramData::SavePair());
		Data::GetSaveLabels().push_back(ProgramData::SavePair());
		Data::GetSaveLabels().push_back(ProgramData::SavePair());
		Data::GetSaveLabels().push_back(ProgramData::SavePair());
		Data::GetSaveLabels().push_back(ProgramData::SavePair());
		fin.open("Data/Objects/NewGame.lab");
		for_each(Data::GetSaveLabels().begin(), Data::GetSaveLabels().end(), [&fin](ProgramData::SavePair & x)
		{
			x.second.first = new Label;
			x.second.second = new Label;
			fin >> *x.second.first >> *x.second.second;
		});
		fin.close();
		Data::GetLabels().Arrows.push_back(pair<Label *, Label * >());
		Data::GetLabels().Arrows.push_back(pair<Label *, Label * >());
		Data::GetLabels().Arrows.push_back(pair<Label *, Label * >());
		Data::GetLabels().Arrows.push_back(pair<Label *, Label * >());
		Data::GetLabels().Arrows.push_back(pair<Label *, Label * >());
		fin.open("Data/Objects/UpgradeArrows.lab");
		for_each(Data::GetLabels().Arrows.begin(), Data::GetLabels().Arrows.end(), [&fin](pair<Label *, Label * > & x)
		{
			x.first = new Label;
			x.second = new Label;
			fin >> *x.first >> *x.second;
		});
		fin.close();
#pragma endregion
	}

	void GetSaves()
	{
		short i = 0;
		for (auto & iterator : fileSystem::directory_iterator("Data/Save"))
		{
			std::stringstream stream;
			stream << iterator;
			string name;
			getline(stream, name);
			stream.flush();
			if (name.size() > 5 && name.back() == 'v' && name.at(name.size() - 2) == 'a' && name.at(name.size() - 3) == 's' && name.at(name.size() - 4) == '.')
			{
				size_t position = name.find('\\');
				while (position != string::npos)
				{
					name.replace(position, 1, "/");
					position = name.find('\\');
				}
				name.erase(0, 10);
				name.pop_back();
				name.pop_back();
				name.pop_back();
				name.pop_back();
				Data::GetSaveLabels().at(i++).first = name;
			}
			if (Data::GetSaveLabels().size() == 6)
				break;
		}
		if (Data::GetSaveLabels().at(0).first.size() == 0)
			Data::GetSaveLabels().at(0).first = "Pusty...  ";
		Data::GetSaveLabels().at(0).second.first->Texture().LoadText(Data::GetSaveLabels().at(0).first, NormalColor);
		if (Data::GetSaveLabels().at(1).first.size() == 0)
			Data::GetSaveLabels().at(1).first = "Pusty...  ";
		Data::GetSaveLabels().at(1).second.first->Texture().LoadText(Data::GetSaveLabels().at(1).first, NormalColor);
		if (Data::GetSaveLabels().at(2).first.size() == 0)
			Data::GetSaveLabels().at(2).first = "Pusty...  ";
		Data::GetSaveLabels().at(2).second.first->Texture().LoadText(Data::GetSaveLabels().at(2).first, NormalColor);
		if (Data::GetSaveLabels().at(3).first.size() == 0)
			Data::GetSaveLabels().at(3).first = "Pusty...  ";
		Data::GetSaveLabels().at(3).second.first->Texture().LoadText(Data::GetSaveLabels().at(3).first, NormalColor);
		if (Data::GetSaveLabels().at(4).first.size() == 0)
			Data::GetSaveLabels().at(4).first = "Pusty...  ";
		Data::GetSaveLabels().at(4).second.first->Texture().LoadText(Data::GetSaveLabels().at(4).first, NormalColor);
	}

#pragma region Display
	void Menu()
	{
		SDL_Event e;
		bool quit = false, reverse = false;
		int frame = 0;
		size_t codeCounter = 0;
		while (!quit)
		{
			SDL_RenderClear(Renderer);
			if (!KonamiCode)
				Data::GetTextures().MenuBackground.at(frame)->Render();
			else
				Data::GetTextures().CodeMenuBackground.at(frame)->Render();
			for_each(Data::GetLabels().Menu.begin(), Data::GetLabels().Menu.end(), [](Label * x) { x->Render(); });
			Data::GetTextures().Title.Render(0, 0, ScreenWidth, int(ScreenHeight / 4.5));
			SDL_RenderPresent(Renderer);
			if (!KonamiCode)
			{
				if (!reverse && frame < 82)
					++frame;
				else if (reverse && frame > 0)
					--frame;
				else if (frame == 82)
					reverse = true;
				else if (frame == 0)
					reverse = false;
			}
			else
			{
				if (codeCounter == 0)
				{
					if (!reverse && frame < 9)
						++frame;
					else if (reverse && frame > 0)
						--frame;
					else if (frame == 9)
						reverse = true;
					else if (frame == 0)
						reverse = false;
					codeCounter = 2;
				}
				else
					--codeCounter;

			}
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_QUIT:
				{
					quit = true;
					break;
				}
				case SDL_KEYDOWN:
				{
					if (!KonamiCode && e.key.keysym.sym == CodeKeys.at(codeCounter))
					{
						++codeCounter;
						if (codeCounter == CodeKeys.size())
							KonamiCode = true;
					}
					else if (!KonamiCode)
						codeCounter = 0;
					break;
				}
				case SDL_MOUSEMOTION:
				{
					int X, Y;
					SDL_GetMouseState(&X, &Y);
					for_each(Data::GetLabels().Menu.begin(), Data::GetLabels().Menu.end(), [&X, &Y](Label * x)
					{
						if (x->IsMouseOn(X, Y))
							x->Texture().SetColor(HooverColor);
						else
							x->Texture().SetColor(NormalColor);
					});
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						int X, Y;
						SDL_GetMouseState(&X, &Y);
						for_each(Data::GetLabels().Menu.begin(), Data::GetLabels().Menu.end(), [&X, &Y](Label * x)
						{
							if (x->IsMouseOn(X, Y))
								(*x)();
						});
					}
					break;
				}
				}
			}
			if (KonamiCode && codeCounter == CodeKeys.size())
			{
				NormalColor = { 255,105,180,255 };
				Mix_PlayMusic(CodeMusic, -1);
				codeCounter = 2;
				for_each(Data::GetLabels().Menu.begin(), Data::GetLabels().Menu.end(), [](Label * x) 
				{ 
					x->Texture().SetBlendMode(SDL_BLENDMODE_BLEND); 
					x->Texture().SetAlpha(120); 
					x->Texture().SetColor(255, 105, 180); 
				});
				for_each(Data::GetLabels().Mini.begin(), Data::GetLabels().Mini.end(), [](Label * x) { x->Texture().SetColor(255, 105, 180); });
				frame = 0;
				Data::GetTextures().Title.SetColor(255, 105, 180);
				Data::GetTextures().Background.SetColor(255, 105, 180);
			}
		}
	}

	void NewGame()
	{
		Data::GetTextures().Background.SetBlendMode(SDL_BLENDMODE_BLEND);
		Data::GetTextures().Background.SetAlpha(170);
		GetSaves();
		SDL_Event e;
		bool back = false;
		while (!back)
		{
			SDL_RenderClear(Renderer);
			Data::GetTextures().Background.Render();
			for_each(Data::GetSaveLabels().begin(), Data::GetSaveLabels().end(), [](ProgramData::SavePair & x)
			{
				x.second.first->Render();
				if (x.first != "Pusty...  " && x.first != "Cannot delete!")
					x.second.second->Render();
			});
			Data::GetLabels().Mini.at(0)->Render();
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_KEYDOWN:
				case SDL_KEYUP:
				{
					EventHandler::GetKeyboard().HandleKey(e);
					break;
				}
				case SDL_MOUSEMOTION:
				{
					int X, Y;
					SDL_GetMouseState(&X, &Y);
					if (Data::GetLabels().Mini.at(0)->IsMouseOn(X, Y))
						Data::GetLabels().Mini.at(0)->Texture().SetColor(HooverColor);
					else
						Data::GetLabels().Mini.at(0)->Texture().SetColor(NormalColor);
					for_each(Data::GetSaveLabels().begin(), Data::GetSaveLabels().end(), [&X, &Y](ProgramData::SavePair & x)
					{
						if (x.first != "Cannot delete!")
						{
							if (x.second.first->IsMouseOn(X, Y))
								x.second.first->Texture().SetColor(HooverColor);
							else
								x.second.first->Texture().SetColor(NormalColor);
							if (x.first != "Pusty...  ")
							{
								if (x.second.second->IsMouseOn(X, Y))
									x.second.second->Texture().SetColor(HooverColor);
								else
									x.second.second->Texture().SetColor(NormalColor);
							}
						}
					});
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						int X, Y;
						bool done = false;
						SDL_GetMouseState(&X, &Y);
						if (Data::GetLabels().Mini.at(0)->IsMouseOn(X, Y))
							(*Data::GetLabels().Mini.at(0))();
						for_each(Data::GetSaveLabels().begin(), Data::GetSaveLabels().end(), [&X, &Y, &done](ProgramData::SavePair & x)
						{
							if (x.first != "Cannot delete!")
							{
								if (x.first == "Pusty...  " && x.second.first->IsMouseOn(X, Y))
								{
									Player * player = CreatePlayer();
									if (player != nullptr)
									{
										done = true;
										if (CurrentGame != nullptr)
											delete CurrentGame;
										CurrentGame = new Game(player);
										return CurrentGame->Play();
									}
								}
								else if (x.first != "Pusty...  " && x.second.second->IsMouseOn(X, Y))
								{

									if (DeleteFile(string(fileSystem::current_path().string() + "\\Data\\Save\\" + x.first + ".sav").c_str()))
									{
										x.first = "Pusty...  ";
										x.second.first->Texture().LoadText(x.first, NormalColor);
									}
									else
									{
										fileSystem::path pt = fileSystem::current_path();
										std::ostringstream str;
										str << GetLastError();
										ofstream fout("error.txt", ios_base::app);
										fout << str.str() << endl << string(pt.string() + "\\Data\\Save\\" + x.first + ".sav") << endl;
										str.clear();
										fout.close();
										x.first = "Cannot delete!";
										x.second.first->Texture().LoadText(x.first, NormalColor);
										x.second.first->Texture().SetColor({ 255,0,0,255 });
									}
								}
							}
						});
						if (done)
							return;
					}
					break;
				}
				}
			}
			if (EventHandler::GetKeyboard().GetKeyState(SDLK_ESCAPE))
			{
				EventHandler::GetKeyboard().SetKeyState(SDLK_ESCAPE, false);
				back = true;
			}
		}
		Data::GetTextures().Background.SetAlpha(255);
	}

	void LoadGame()
	{
		Data::GetTextures().Background.SetBlendMode(SDL_BLENDMODE_BLEND);
		Data::GetTextures().Background.SetAlpha(170);
		GetSaves();
		SDL_Event e;
		bool back = false;
		while (!back)
		{
			SDL_RenderClear(Renderer);
			Data::GetTextures().Background.Render();
			for_each(Data::GetSaveLabels().begin(), Data::GetSaveLabels().end(), [](ProgramData::SavePair & x)
			{
				if (x.first != "Pusty...  ")
				{
					x.second.first->Render();
					if (x.first != "Cannot delete!")
						x.second.second->Render();
				}
			});
			Data::GetLabels().Mini.at(0)->Render();
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_KEYDOWN:
				case SDL_KEYUP:
				{
					EventHandler::GetKeyboard().HandleKey(e);
					break;
				}
				case SDL_MOUSEMOTION:
				{
					int X, Y;
					SDL_GetMouseState(&X, &Y);
					if (Data::GetLabels().Mini.at(0)->IsMouseOn(X, Y))
						Data::GetLabels().Mini.at(0)->Texture().SetColor(HooverColor);
					else
						Data::GetLabels().Mini.at(0)->Texture().SetColor(NormalColor);
					for_each(Data::GetSaveLabels().begin(), Data::GetSaveLabels().end(), [&X, &Y](ProgramData::SavePair & x)
					{
						if (x.first != "Cannot delete!" && x.first != "Pusty...  ")
						{
							if (x.second.first->IsMouseOn(X, Y))
								x.second.first->Texture().SetColor(HooverColor);
							else
								x.second.first->Texture().SetColor(NormalColor);
							if (x.second.second->IsMouseOn(X, Y))
								x.second.second->Texture().SetColor(HooverColor);
							else
								x.second.second->Texture().SetColor(NormalColor);
						}
					});
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						int X, Y;
						bool done = false;
						SDL_GetMouseState(&X, &Y);
						if (Data::GetLabels().Mini.at(0)->IsMouseOn(X, Y))
							(*Data::GetLabels().Mini.at(0))();
						for_each(Data::GetSaveLabels().begin(), Data::GetSaveLabels().end(), [&X, &Y, &done](ProgramData::SavePair & x)
						{
							if (x.first != "Pusty...  " && x.first != "Cannot delete!")
							{
								if (x.second.first->IsMouseOn(X, Y))
								{
									done = true;
									string place;
									if (CurrentGame != nullptr)
										delete CurrentGame;
									CurrentGame = new Game("Data/Save/" + x.first + ".sav", place);
									Mix_PlayMusic(Music.at(10), -1);
									return CurrentGame->Play(place);
								}
								else if (x.second.second->IsMouseOn(X, Y))
								{
									if (DeleteFile(string(fileSystem::current_path().string() + "\\Data\\Save\\" + x.first + ".sav").c_str()))
									{
										x.first = "Pusty...  ";
										x.second.first->Texture().LoadText(x.first, NormalColor);
									}
									else
									{
										fileSystem::path pt = fileSystem::current_path();
										std::ostringstream str;
										str << GetLastError();
										ofstream fout("error.txt", ios_base::app);
										fout << str.str() << ' ' << string(pt.string() + "\\Data\\Save\\" + x.first + ".sav") << endl;
										str.clear();
										fout.close();
										x.first = "Cannot delete!";
										x.second.first->Texture().LoadText(x.first, NormalColor);
										x.second.first->Texture().SetColor({ 255,0,0,255 });
									}
								}
							}
						});
						if (done)
							return;
					}
					break;
				}
				}
			}
			if (EventHandler::GetKeyboard().GetKeyState(SDLK_ESCAPE))
			{
				EventHandler::GetKeyboard().SetKeyState(SDLK_ESCAPE, false);
				back = true;
			}
		}
		Data::GetTextures().Background.SetAlpha(255);
	}

	Player * CreatePlayer()
	{
		if (!KonamiCode)
			Mix_PlayMusic(Music.at(10), -1);
		const size_t nameMaxLength = 16;
		size_t pointsLeft = 10;
		string name = "";
		Stats statistics;
		statistics.LevelUp({ 8, 10, 9, 11, 12, 0, 0, 0, 0, 0 });
		SDL_Event e;
		bool done = false, changed = false, textFocus = false;
		Label labelName;
		Label labelPoints;
		Label labelPlay;
		Label labelBack;
		Label labelTable;
		vector<vector<Label *>> labelsStatistics;
		ifstream fin("Data/Objects/Create.lab");
		fin >> labelName >> labelPoints >> labelPlay >> labelBack >> labelTable;
		if (KonamiCode)
		{
			labelName.Texture().SetColor(NormalColor);
			labelPoints.Texture().SetColor(NormalColor);
			labelPlay.Texture().SetColor(NormalColor);
			labelTable.Texture().SetColor(NormalColor);
		}
		for (short I = 0; I < 5; ++I)
		{
			labelsStatistics.push_back(vector<Label *>());
			for (short i = 0; i < 3; ++i)
			{
				labelsStatistics.at(I).push_back(new Label);
				fin >> *labelsStatistics.at(I).at(i);
				if (KonamiCode)
					labelsStatistics.at(I).at(i)->Texture().SetColor(NormalColor);
			}
		}
		fin.close();
		while (!done)
		{
			SDL_RenderClear(Renderer);
			Data::GetTextures().Background.Render();
			labelName.Render();
			labelTable.Render();
			labelPoints.Render();
			if (name.size() && !pointsLeft)
				labelPlay.Render();
			labelBack.Render();
			for_each(labelsStatistics.begin(), labelsStatistics.end(), [&pointsLeft](vector<Label *> & x)
			{
				x.at(0)->Render();
				if (pointsLeft)
					x.at(2)->Render();
			});
			if (statistics.MainStatistics.Inteligence > 8)
				labelsStatistics.at(0).at(1)->Render();
			if (statistics.MainStatistics.Strenght > 10)
				labelsStatistics.at(1).at(1)->Render();
			if (statistics.MainStatistics.Dextermity > 9)
				labelsStatistics.at(2).at(1)->Render();
			if (statistics.Endurance > 11)
				labelsStatistics.at(3).at(1)->Render();
			if (statistics.Vitality > 12)
				labelsStatistics.at(4).at(1)->Render();
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_KEYDOWN:
				{
					if (textFocus)
					{
						if (name.size() < nameMaxLength)
						{
							if ((e.key.keysym.sym >= SDLK_0 && e.key.keysym.sym <= SDLK_9) || e.key.keysym.sym == SDLK_SPACE || e.key.keysym.sym == SDLK_QUOTE || e.key.keysym.sym == SDLK_MINUS || e.key.keysym.sym == SDLK_UNDERSCORE)
							{
								name += (char)e.key.keysym.sym;
								changed = true;
							}
							else if (e.key.keysym.sym >= SDLK_a && e.key.keysym.sym <= SDLK_z)
							{
								name += (char)e.key.keysym.sym;
								changed = true;
							}
						}
						if (e.key.keysym.sym == SDLK_BACKSPACE && name.size())
						{
							name.pop_back();
							changed = true;
						}
					}
				}
				case SDL_KEYUP:
				{
					EventHandler::GetKeyboard().HandleKey(e);
					break;
				}
				case SDL_MOUSEMOTION:
				{
					int X, Y;
					SDL_GetMouseState(&X, &Y);
					if (labelPlay.IsMouseOn(X, Y))
						labelPlay.Texture().SetColor(HooverColor);
					else
						labelPlay.Texture().SetColor(NormalColor);
					if (labelBack.IsMouseOn(X, Y))
						labelBack.Texture().SetColor(HooverColor);
					else
						labelBack.Texture().SetColor(NormalColor);
					for_each(labelsStatistics.begin(), labelsStatistics.end(), [&X, &Y](vector<Label *> & x)
					{
						for_each(x.begin() + 1, x.end(), [&X, &Y](Label * y)
						{
							if (y->IsMouseOn(X, Y))
								y->Texture().SetColor(HooverColor);
							else
								y->Texture().SetColor(NormalColor);
						});
					});
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						int X, Y;
						SDL_GetMouseState(&X, &Y);
						if (labelName.IsMouseOn(X, Y))
						{
							textFocus = true;
							labelName.Texture().SetColor(HooverColor);
						}
						else
						{
							textFocus = false;
							labelName.Texture().SetColor(NormalColor);
						}
						if (name.size() && !pointsLeft && labelPlay.IsMouseOn(X, Y))
							done = true;
						else if (labelBack.IsMouseOn(X, Y))
							return nullptr;
						else if (pointsLeft && labelsStatistics.at(0).at(2)->IsMouseOn(X, Y))
						{
							++statistics.MainStatistics.Inteligence;
							--pointsLeft;
							labelsStatistics.at(0).at(0)->Texture().LoadText("Inteligence:  " + to_string(statistics.MainStatistics.Inteligence), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
						else if (pointsLeft && labelsStatistics.at(1).at(2)->IsMouseOn(X, Y))
						{
							++statistics.MainStatistics.Strenght;
							--pointsLeft;
							labelsStatistics.at(1).at(0)->Texture().LoadText("Strenght:  " + to_string(statistics.MainStatistics.Strenght), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
						else if (pointsLeft && labelsStatistics.at(2).at(2)->IsMouseOn(X, Y))
						{
							++statistics.MainStatistics.Dextermity;
							--pointsLeft;
							labelsStatistics.at(2).at(0)->Texture().LoadText("Dextermity:  " + to_string(statistics.MainStatistics.Dextermity), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
						else if (pointsLeft && labelsStatistics.at(3).at(2)->IsMouseOn(X, Y))
						{
							++statistics.Endurance;
							--pointsLeft;
							labelsStatistics.at(3).at(0)->Texture().LoadText("Endurance:  " + to_string(statistics.Endurance), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
						else if (pointsLeft && labelsStatistics.at(4).at(2)->IsMouseOn(X, Y))
						{
							++statistics.Vitality;
							--pointsLeft;
							labelsStatistics.at(4).at(0)->Texture().LoadText("Vitality:  " + to_string(statistics.Vitality), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
						else if (statistics.MainStatistics.Inteligence > 8 && labelsStatistics.at(0).at(1)->IsMouseOn(X, Y))
						{
							--statistics.MainStatistics.Inteligence;
							++pointsLeft;
							labelsStatistics.at(0).at(0)->Texture().LoadText("Inteligence:  " + to_string(statistics.MainStatistics.Inteligence), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
						else if (statistics.MainStatistics.Strenght > 10 && labelsStatistics.at(1).at(1)->IsMouseOn(X, Y))
						{
							--statistics.MainStatistics.Strenght;
							++pointsLeft;
							labelsStatistics.at(1).at(0)->Texture().LoadText("Strenght:  " + to_string(statistics.MainStatistics.Strenght), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
						else if (statistics.MainStatistics.Dextermity > 9 && labelsStatistics.at(2).at(1)->IsMouseOn(X, Y))
						{
							--statistics.MainStatistics.Dextermity;
							++pointsLeft;
							labelsStatistics.at(2).at(0)->Texture().LoadText("Dextermity:  " + to_string(statistics.MainStatistics.Dextermity), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
						else if (statistics.Endurance > 11 && labelsStatistics.at(3).at(1)->IsMouseOn(X, Y))
						{
							--statistics.Endurance;
							++pointsLeft;
							labelsStatistics.at(3).at(0)->Texture().LoadText("Endurance:  " + to_string(statistics.Endurance), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
						else if (statistics.Vitality > 12 && labelsStatistics.at(4).at(1)->IsMouseOn(X, Y))
						{
							--statistics.Vitality;
							++pointsLeft;
							labelsStatistics.at(4).at(0)->Texture().LoadText("Vitality:  " + to_string(statistics.Vitality), NormalColor);
							labelPoints.Texture().LoadText("Points left:  " + to_string(pointsLeft), NormalColor);
						}
					}
					break;
				}
				case SDL_MOUSEBUTTONUP:
				{
					break;
				}
				}
			}
			if (EventHandler::GetKeyboard().GetKeyState(SDLK_ESCAPE))
			{
				EventHandler::GetKeyboard().SetKeyState(SDLK_ESCAPE, false);
				textFocus = false;
			}
			if (textFocus && changed)
			{
				string displayName = name;
				for (short i = 0, I = short(nameMaxLength - name.size() - 1); i <= I; ++i)
					displayName += '_';
				labelName.Texture().LoadText("Name:    " + displayName, HooverColor);
				changed = false;
			}
		}
		for_each(labelsStatistics.begin(), labelsStatistics.end(), [](vector<Label *> & x) { for_each(x.begin(), x.end(), [](Label * y) { delete y; }); });
		return new Player(name, statistics);
	}
#pragma endregion
}