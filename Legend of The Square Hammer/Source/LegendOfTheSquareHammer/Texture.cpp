/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "Texture.h"
namespace LegendOfTheSquareHammer
{
	extern SDL_Surface * ScreenSurface;
	extern SDL_Renderer * Renderer;
	extern TTF_Font * Font;
	extern double ScreenScale;
}
namespace LegendOfTheSquareHammer::Renders
{
	Texture::Texture(const std::string & newPath)
	{
		path = newPath;
		SDL_Surface * surface = LoadSurface(path);
		SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 1, 1, 1));
		texture = SDL_CreateTextureFromSurface(Renderer, surface);
		if (texture == nullptr)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << path << " SDL_CreateTextureFromSurface error: " << SDL_GetError() << endl;
			fout.close();
			exit(0b1000000000000);
		}
		width = int(surface->w * ScreenScale);
		height = int(surface->h * ScreenScale);
		SDL_FreeSurface(surface);
	}

	SDL_Surface* Texture::LoadSurface(const string & path)
	{
		SDL_Surface * temporarySurface = IMG_Load(path.c_str());
		if (temporarySurface == nullptr)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << path << ": Cannot load image: " << IMG_GetError() << endl;
			fout.close();
			exit(0b10000000000000);
		}
		SDL_Surface * finalSurface = SDL_ConvertSurface(temporarySurface, ScreenSurface->format, NULL);
		if (finalSurface == nullptr)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "Cannot convert image: " << SDL_GetError() << endl;
			fout.close();
			return temporarySurface;
		}
		SDL_FreeSurface(temporarySurface);
		return finalSurface;
	}

	bool Texture::LoadFile(const std::string & newPath)
	{
		if (newPath != "noPath")
		{
			Free(true);
			path = newPath;
			SDL_Surface * surface = LoadSurface(path);
			SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 1, 1, 1));
			texture = SDL_CreateTextureFromSurface(Renderer, surface);
			if (texture == nullptr)
			{
				ofstream fout("error.txt", ios_base::app);
				fout << path << " SDL_CreateTextureFromSurface error: " << SDL_GetError() << endl;
				fout.close();
				return false;
			}
			width = int(surface->w * ScreenScale);
			height = int(surface->h * ScreenScale);
			SDL_FreeSurface(surface);
		}
		return true;
	}

	bool Texture::LoadText(const string & newText, const SDL_Color & color)
	{
		Free(false);
		SDL_Surface * surface = TTF_RenderText_Solid(Font, newText.c_str(), color);
		if (surface == nullptr)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << newText << " TTF_RenderText_Solid error: " << SDL_GetError() << endl;
			fout.close();
			return false;
		}
		textTexture = SDL_CreateTextureFromSurface(Renderer, surface);
		if (textTexture == nullptr)
		{
			ofstream fout("error.txt", ios_base::app);
			fout << newText << " SDL_CreateTextureFromSurface Text error: " << SDL_GetError() << endl;
			fout.close();
			return false;
		}
		text = newText;
		textColor = color;
		if (texture != nullptr)
		{
			textWidth = int(width - abs(width - surface->w));
			textHeight = int(height * 0.6);
		}
		else
		{
			textWidth = width = int(surface->w * ScreenScale);
			textHeight = height = int(surface->h * ScreenScale);
		}
		SDL_FreeSurface(surface);
		return true;
	}

	void Texture::Free(bool option)
	{
		if (option && texture != nullptr)
		{
			SDL_DestroyTexture(texture);
			texture = nullptr;
			width = 0;
			height = 0;
		}
		if (textTexture != nullptr)
		{
			SDL_DestroyTexture(textTexture);
			textTexture = nullptr;
			textWidth = 0;
			textHeight = 0;
		}
	}

	void Texture::Render(SDL_RendererFlip flip, double angle, SDL_Point * point, bool textFlip, SDL_Rect * clip)
	{
		if (texture != nullptr)
			SDL_RenderCopyEx(Renderer, texture, clip, NULL, angle, point, flip);
		if (textTexture != nullptr)
		{
			if (textFlip)
				SDL_RenderCopyEx(Renderer, textTexture, clip, NULL, angle, point, flip);
			else
				SDL_RenderCopy(Renderer, textTexture, clip, NULL);
		}
	}

	void Texture::Render(int x, int y, SDL_RendererFlip flip, double angle, SDL_Point * point, bool textFlip, SDL_Rect * clip)
	{
		if (texture != nullptr)
		{
			SDL_Rect renderQuad = { x, y, width, height };
			if (clip != nullptr)
			{
				renderQuad.w = clip->w;
				renderQuad.h = clip->h;
			}
			SDL_RenderCopyEx(Renderer, texture, clip, &renderQuad, angle, point, flip);
		}
		if (textTexture != nullptr)
		{
			SDL_Rect renderQuad = { int(x + 0.2 * width), int(y + 0.2 * height), textWidth, textHeight };
			if (clip != nullptr)
			{
				renderQuad.w = int(clip->w * 0.6);
				renderQuad.h = int(clip->h * 0.6);
			}
			if (textFlip)
				SDL_RenderCopyEx(Renderer, textTexture, clip, &renderQuad, angle, point, flip);
			else
				SDL_RenderCopy(Renderer, textTexture, clip, &renderQuad);
		}
	}

	void Texture::Render(int x, int y, int w, int h, SDL_RendererFlip flip, double angle, bool textFlip, SDL_Point * point)
	{
		if (texture != nullptr)
		{
			SDL_Rect renderQuad = { x, y, w, h };
			SDL_RenderCopyEx(Renderer, texture, NULL, &renderQuad, angle, point, flip);
		}
		if (textTexture != nullptr)
		{
			SDL_Rect renderQuad = { int(x + 0.2 * w), int(y + 0.2 * h), int(0.6 * w), int(0.6 * h) };
			if (textFlip)
				SDL_RenderCopyEx(Renderer, textTexture, NULL, &renderQuad, angle, point, flip);
			else
				SDL_RenderCopy(Renderer, textTexture, NULL, &renderQuad);
		}
	}

	void Texture::SetColor(Uint8 r, Uint8 g, Uint8 b)
	{
		if (texture != nullptr)
			SDL_SetTextureColorMod(texture, r, g, b);
		if (textTexture != nullptr)
			SDL_SetTextureColorMod(textTexture, r, g, b);
	}

	void Texture::SetColor(const SDL_Color & color)
	{
		if (texture != nullptr)
			SDL_SetTextureColorMod(texture, color.r, color.g, color.b);
		if (textTexture != nullptr)
			SDL_SetTextureColorMod(textTexture, color.r, color.g, color.b);
	}

	void Texture::SetBlendMode(SDL_BlendMode blendMode)
	{
		if (texture != nullptr)
			SDL_SetTextureBlendMode(texture, blendMode);
		if (textTexture != nullptr)
			SDL_SetTextureBlendMode(textTexture, blendMode);
	}

	void Texture::SetAlpha(Uint8 alpha)
	{
		if (texture != nullptr)
			SDL_SetTextureAlphaMod(texture, alpha);
		if (textTexture != nullptr)
			SDL_SetTextureAlphaMod(textTexture, alpha);
	}
}