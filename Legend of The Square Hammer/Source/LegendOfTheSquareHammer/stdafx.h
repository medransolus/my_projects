/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "targetver.h"
#include "IObject.h"
namespace LegendOfTheSquareHammer
{
	using LegendOfTheSquareHammer::Renders::Objects::IObject;

	size_t Distance(const LegendOfTheSquareHammer::Renders::Objects::IObject * object1, const IObject * object2);
	ofstream & operator<<(ofstream & fout, const SDL_Rect & rectangle);
	ifstream & operator>>(ifstream & fin, SDL_Rect & rectangle);
}
using LegendOfTheSquareHammer::operator<<;
using LegendOfTheSquareHammer::operator>>;