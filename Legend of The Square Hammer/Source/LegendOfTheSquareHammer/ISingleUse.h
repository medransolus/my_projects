/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Item.h"
namespace LegendOfTheSquareHammer::Renders::Items::SingleUse
{
	__interface ISingleUse
	{
		int Mana() const;
		int Health() const;
	};
}