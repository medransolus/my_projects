/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "stdafx.h"
#include "Building.h"
#include "Player.h"
namespace LegendOfTheSquareHammer::Renders::Objects
{
	Building::Building(double scale, size_t absoluteX, size_t absoluteY, int x, int y, const SDL_Rect & collider, const SDL_Rect & entrancePosition, const string & path, const string & newBuildingName) : X(absoluteX), Y(absoluteY)
	{
		if (!texture.LoadFile(path))
		{
			ofstream fout("error.txt", ios_base::app);
			fout << ": Building\n";
			fout.close();
			exit(0b100000000000000);
		}
		buildingName = newBuildingName;
		position.x = x;
		position.y = y;
		position.w = int(texture.Width() * scale);
		position.h = int(texture.Height() * scale);
		collision.y = position.y + int(collider.y * scale);
		collision.x = position.x + int(collider.x * scale);
		collision.w = int(collider.w * scale);
		collision.h = int(collider.h * scale);
		entrance.x = position.x + int(entrancePosition.x * scale);
		entrance.y = position.y + int(entrancePosition.y * scale);
		entrance.w = int(entrancePosition.w * scale);
		entrance.h = int(entrancePosition.h * scale);
		colliderDown = position.h - collision.h - (position.y - collision.y);
		entranceDown = position.h - entrance.h - (position.y - entrance.y);
		colliderRight = position.w - collision.w - (position.x - collision.x);
		entranceRight = position.w - entrance.w - (position.x - entrance.x);
	}

	void Building::SetPosition(int x, int y)
	{
		position.x = x;
		collision.x = position.x + position.w - collision.w - colliderRight;
		entrance.x = position.x + position.w - entrance.w - entranceRight;
		position.y = y;
		collision.y = position.y + position.h - collision.h - colliderDown;
		entrance.y = position.y + position.h - entrance.h - entranceDown;
	}

	void Building::ChangePosition(int x, int y)
	{
		position.x += x;
		collision.x += x;
		entrance.x += x;
		position.y += y;
		collision.y += y;
		entrance.y += y;
	}

	void Building::SetAbsolutePosition(size_t x, size_t y)
	{
		X = x;
		Y = y;
	}

	void Building::ChangeAbsolutePosition(int x, int y)
	{
		X += x;
		Y += y;
	}

	void Building::Render()
	{
		texture.Render(position.x, position.y, position.w, position.h);
	}

	CollisionEntrance Building::CollisionDirection(const IObject * object)
	{
		int objectWidth = object->Collision().x + object->Collision().w;
		int objectHeight = object->Collision().y + object->Collision().h;
		int thisWidth = collision.x + collision.w;
		int thisHeight = collision.y + collision.h;
		int entranceWidth = entrance.x + entrance.w;
		int entranceHeight = entrance.y + entrance.h;
		if (objectWidth<collision.x || object->Collision().x>thisWidth || objectHeight<collision.y || object->Collision().y>thisHeight)
			return { NONE,nullptr };
		if (dynamic_cast<const Player *>(object) != nullptr)
		{
			if ((object->Collision().x >= entrance.x && objectWidth <= entranceWidth && ((object->Collision().y <= entranceHeight && object->Collision().y > entrance.y) || (object->Collision().y < entrance.y &&objectHeight >= entrance.y))) || (object->Collision().y >= entrance.y&&objectHeight <= entranceHeight && ((object->Collision().x < entrance.x&&objectWidth >= entrance.x) || (object->Collision().x <= entranceWidth && objectWidth > entranceWidth))))
				return { ENTER,this };
		}
		if (objectWidth >= collision.x && objectWidth < thisWidth && object->Collision().x < collision.x)
		{
			if (objectHeight >= collision.y && objectHeight < thisHeight && object->Collision().y < collision.y)
				return { S | E,nullptr };
			if (object->Collision().y <= thisHeight && object->Collision().y > collision.y&&objectHeight > thisHeight)
				return { N | E,nullptr };
			return { E,nullptr };
		}
		if (object->Collision().x <= thisWidth && object->Collision().x > collision.x&&objectWidth > thisWidth)
		{
			if (objectHeight >= collision.y && objectHeight < thisHeight && object->Collision().y < collision.y)
				return { S | W,nullptr };
			if (object->Collision().y <= thisHeight && object->Collision().y > collision.y&&objectHeight > thisHeight)
				return { N | W,nullptr };
			return { W,nullptr };
		}
		if (object->Collision().x >= collision.x && objectWidth <= thisWidth)
		{
			if (objectHeight >= collision.y&&objectHeight < thisHeight&&object->Collision().y < collision.y)
				return { S,nullptr };
			if (object->Collision().y <= thisHeight && object->Collision().y > collision.y&&objectHeight > thisHeight)
				return { N,nullptr };
		}
		return { NONE,nullptr };
	}

	ofstream & operator<<(ofstream & fout, const Building & building)
	{
		fout << building.X << ' ' << building.Y << ' ' << building.colliderDown << ' ' << building.colliderRight << ' ' << building.entranceDown << ' ' << building.entranceRight << endl << building.buildingName << endl;
		fout << building.position << endl;
		fout << building.collision << endl;
		fout << building.entrance << endl;
		fout << building.texture << endl;
		return fout;
	}

	ifstream & operator>>(ifstream & fin, Building & building)
	{
		fin >> building.X >> building.Y >> building.colliderDown >> building.colliderRight >> building.entranceDown >> building.entranceRight;
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, building.buildingName);
		fin >> building.position >> building.collision >> building.entrance;
		fin >> building.texture;
		return fin;
	}
}