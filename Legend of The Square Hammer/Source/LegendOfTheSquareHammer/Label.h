/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "IRenderable.h"
namespace LegendOfTheSquareHammer::Renders
{
	///<summary>
	///Button on screen
	///</summary>
	class Label : public IRenderable
	{
		SDL_Rect position;
		Renders::Texture texture;
		string path = "none";
		string function = "none";
	public:
		Label() {}
		Label(const SDL_Rect & newPosition, const string & newPath, const string & callFunction = "none");

		inline SDL_Rect Position() const { return position; }
		inline Renders::Texture & Texture() { return texture; }

		void SetPosition(int x, int y) { position.x = x; position.y = y; }
		void ChangePosition(int x, int y) { position.x += x; position.y += y; }
		void Render() { texture.Render(position.x, position.y, position.w, position.h); }
		bool IsMouseOn(int x, int y) { return (x >= position.x && x <= position.x + position.w) && (y >= position.y && y <= position.y + position.h); }

		void operator()();
		friend ofstream & operator<<(ofstream & fout, const Label & label);
		friend ifstream & operator>>(ifstream & fin, Label & label);

		~Label() { texture.Free(true); }
	};
}