/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "stdafx.h"
#include "Player.h"
#include "StatsPotion.h"
#include "AttackScroll.h"
#include "EffectScroll.h"
#include "SpellScroll.h"
using namespace LegendOfTheSquareHammer::Renders::Items::SingleUse;
using namespace LegendOfTheSquareHammer::Renders::Items::Magic;
namespace LegendOfTheSquareHammer
{
	extern size_t ScreenWidth;
	extern size_t ScreenHeight;
	extern size_t CenterX;
	extern size_t CenterY;
	extern double ScreenScale;
}
namespace LegendOfTheSquareHammer::Renders::Objects
{
	Player::Player(const string & newName, const Stats & statistics) : name(newName)
	{
		for (short i = 0; i < 7; ++i)
			currentArmor.at(i) = nullptr;
		playerStatistics = statistics;
		health = maxHealth = playerStatistics.Vitality * 12;
		mana = maxMana = playerStatistics.MainStatistics.Inteligence * 7;
		level = 1;
		nextLevelExperience = 200;
		damage.Normal = int(0.5 * playerStatistics.MainStatistics.Strenght + 0.5 * playerStatistics.MainStatistics.Dextermity);
		for (short i = 1; i < 5; ++i)
			textures.push_back(new Renders::Texture("Data/Textures/Player" + std::to_string(i) + ".png"));
		position.x = 905;
		position.y = 466;
		position.w = int(textures.at(0)->Width() * 0.4 * ScreenScale);
		position.h = int(textures.at(0)->Height() * 0.4 * ScreenScale);
		collision.w = int(0.46 * position.w);
		collision.h = int(0.18 * position.h);
		collision.x = position.x + (position.w - collision.w) / 2;
		collision.y = position.y + position.h - collision.h;
		CenterX = (ScreenWidth - position.w) / 2;
		CenterY = (ScreenHeight - position.h) / 2;
	}

	bool Player::AddExperience(size_t earnedExperience)
	{
		experience += earnedExperience;
		if (experience >= nextLevelExperience)
		{
			experience -= nextLevelExperience;
			++level;
			++levelPoints;
			nextLevelExperience = size_t(level * 190 + pow(level, 2) * 10);
			return true;
		}
		return false;
	}

	void Player::LevelUp(const Stats & additionalStatistics)
	{
		Stats equipmentStatistics;
		if (currentWeapon != nullptr)
			equipmentStatistics = currentWeapon->Statistics();
		for (short i = 0; i < 7; ++i)
			if (currentArmor.at(i) != nullptr)
				equipmentStatistics += currentArmor.at(i)->Statistics();
		playerStatistics -= equipmentStatistics;
		playerStatistics.LevelUp(additionalStatistics);
		playerStatistics += equipmentStatistics;
		health = maxHealth = playerStatistics.Vitality * 12;
		mana = maxMana = playerStatistics.MainStatistics.Inteligence * 7;
		damage.Normal = int(0.5 * playerStatistics.MainStatistics.Strenght + 0.5 * playerStatistics.MainStatistics.Dextermity);
		levelPoints = 0;
	}

	bool Player::Wear(const string & name, short ringNumber)
	{
		ItemMap::iterator iterator = items.find(name);
		if (iterator == items.end())
			return false;
		Item * item = items.at(name);
		if (item->MinimumLevel() > level)
			return false;
		if (dynamic_cast<Weapon *>(item) != nullptr)
		{
			if (playerStatistics.MainStatistics >= ((Weapon *)item)->MinimumStatistics())
			{
				if (currentWeapon != nullptr)
				{
					Weapon * weapon = currentWeapon;
					currentWeapon = (Weapon *)item;
					item = weapon;
					playerStatistics -= ((Weapon *)item)->Statistics();
					playerStatistics += currentWeapon->Statistics();
				}
				else
				{
					currentWeapon = (Weapon *)item;
					playerStatistics += currentWeapon->Statistics();
					item = nullptr;
				}
			}
			else
				return false;
		}
		else if (dynamic_cast<Armor *>(item) != nullptr)
		{
			if (playerStatistics.MainStatistics >= ((Armor *)item)->MinimumStatistics())
			{
				short part;
				switch (((Armor *)item)->Part())
				{
				case Necklace:
				{
					part = 0;
					break;
				}
				case Ring:
				{
					if (ringNumber != 0 || ringNumber != 1)
						return false;
					part = 1 + ringNumber;
					break;
				}
				case Head:
				{
					part = 3;
					break;
				}
				case Chest:
				{
					part = 4;
					break;
				}
				case Leg:
				{
					part = 5;
					break;
				}
				case Hands:
				{
					part = 6;
					break;
				}
				default:
					return false;
				}
				if (currentArmor.at(part) != nullptr)
				{
					Armor * armor = currentArmor.at(part);
					currentArmor.at(part) = (Armor *)item;
					item = armor;
					playerStatistics -= ((Armor *)item)->Statistics();
					playerStatistics += currentArmor.at(part)->Statistics();
				}
				else
				{
					currentArmor.at(part) = (Armor *)item;
					playerStatistics += currentArmor.at(part)->Statistics();
					item = nullptr;
				}
			}
		}
		else
			return false;
		items.erase(iterator);
		if (item != nullptr)
			items.insert(pair<const string, Item *>(item->Name(), item));
		return true;
	}

	bool Player::TakeOff(BodyPart bodyPart, short ringNumber)
	{
		switch (bodyPart)
		{
		case WeaponE:
		{
			items.insert(pair<const string, Item *>(currentWeapon->Name(), currentWeapon));
			playerStatistics -= currentWeapon->Statistics();
			currentWeapon = nullptr;
			break;
		}
		default:
		{
			short part;
			switch (bodyPart)
			{
			case Necklace:
			{
				part = 0;
				break;
			}
			case Ring:
			{
				if (ringNumber != 0 || ringNumber != 1)
					return false;
				part = 1 + ringNumber;
				break;
			}
			case Head:
			{
				part = 3;
				break;
			}
			case Chest:
			{
				part = 4;
				break;
			}
			case Leg:
			{
				part = 5;
				break;
			}
			case Hands:
			{
				part = 6;
				break;
			}
			default:
				return false;
			}
			items.insert(pair<const string, Item *>(currentArmor.at(part)->Name(), currentArmor.at(part)));
			playerStatistics -= currentArmor.at(part)->Statistics();
			currentArmor.at(part) = nullptr;
			break;
		}
		}
		return true;
	}

	Item * Player::ThrowOut(const string & name)
	{
		ItemMap::iterator iterator = items.find(name);
		if (iterator != items.end())
		{
			Item * item = items.at(name);
			items.erase(iterator);
			return item;
		}
		return nullptr;
	}

	void Player::GetItem(Item * item)
	{
		if (item != nullptr)
		{
			items.insert(pair<const string, Item *>(item->Name(), item));
			item = nullptr;
		}
	}

	void Player::SetPosition(int x, int y)
	{
		position.x = x;
		collision.x = position.x + (position.w - collision.w) / 2;
		position.y = y;
		collision.y = position.y + position.h - collision.h;
	}

	void Player::ChangePosition(int x, int y)
	{
		position.x += x;
		collision.x += x;
		position.y += y;
		collision.y += y;
	}

	void Player::SetAbsolutePosition(size_t x, size_t y)
	{
		X = x;
		Y = y;
	}

	void Player::ChangeAbsolutePosition(int x, int y)
	{
		X += x;
		Y += y;
	}

	void Player::Render()
	{
		textures.at(textureNumber)->Render(position.x, position.y, position.w, position.h, flip);
		if (animationDelay == 0)
		{
			if (!reverseAnimation && textureNumber < textures.size() - 1)
				++textureNumber;
			else if (reverseAnimation && textureNumber > 0)
				--textureNumber;
			else if (textureNumber == textures.size() - 1)
				reverseAnimation = true;
			else if (textureNumber == 0)
				reverseAnimation = false;
			animationDelay = 4;
		}
		else
			--animationDelay;
	}

	bool Player::UseMana(size_t requestedMana)
	{
		if (requestedMana > mana)
			return false;
		mana -= requestedMana;
		return true;
	}

	void Player::RegenerateMana(size_t regeneratedMana)
	{
		if (regeneratedMana + mana > maxMana)
			mana = maxMana;
		else
			mana += regeneratedMana;
	}

	bool Player::SpendMoney(size_t requestedMoney)
	{
		if (requestedMoney > money)
			return false;
		money -= requestedMoney;
		return true;
	}

	bool Player::Wound(const LegendOfTheSquareHammer::Damage & damageTaken)
	{
		int unblockedDamage = damageTaken.Normal - playerStatistics.Armor;
		if (unblockedDamage >= int(health))
		{
			health = 0;
			return false;
		}
		if (unblockedDamage > 0)
			health -= unblockedDamage;
		unblockedDamage = damageTaken.Darkness * (100 - playerStatistics.DarknessResistance) / 100;
		if (unblockedDamage >= int(health))
		{
			health = 0;
			return false;
		}
		health -= unblockedDamage;
		unblockedDamage = damageTaken.Fire * (100 - playerStatistics.FireResistance) / 100;
		if (unblockedDamage >= int(health))
		{
			health = 0;
			return false;
		}
		health -= unblockedDamage;
		unblockedDamage = damageTaken.Ice * (100 - playerStatistics.IceResistance) / 100;
		if (unblockedDamage >= int(health))
		{
			health = 0;
			return false;
		}
		health -= unblockedDamage;
		unblockedDamage = damageTaken.Magic * (100 - playerStatistics.MagicResistance) / 100;
		if (unblockedDamage >= int(health))
		{
			health = 0;
			return false;
		}
		health -= unblockedDamage;
		return true;
	}

	void Player::Heal(size_t healthHealed)
	{
		if (healthHealed + health > maxHealth)
			health = maxHealth;
		else
			health += healthHealed;
	}

	void Player::ListOfItems(vector<pair<const string, Item*>>& list)
	{
		list.clear();
		for_each(items.begin(), items.end(), [&list](const pair<const string, Item *> & x) { list.push_back(x); });
	}

	void Player::ListOfEquipment(pair<Weapon*, std::array<Armor*, 7>>& list)
	{
		list.first = currentWeapon;
		list.second = currentArmor;
	}

	short Player::Use(const string & name)
	{
		ItemMap::iterator iterator = items.find(name);
		if (iterator == items.end())
			return 0;
		Item * item = items.at(name);
		if (item->MinimumLevel() > level)
			return 0;
		if (dynamic_cast<Potion *>(item) != nullptr)
		{
			this->Heal(((Potion *)item)->Health());
			this->RegenerateMana(((Potion *)item)->Mana());
			if (dynamic_cast<StatsPotion *>(item) != nullptr)
				this->LevelUp(((StatsPotion *)item)->StatisticsChange());
		}
		else if (dynamic_cast<SpellScroll *>(item) != nullptr)
		{
			return 1;
		}
		else if (dynamic_cast<EffectScroll *>(item) != nullptr)
		{
			return 1;
		}
		else if (dynamic_cast<AttackScroll *>(item) != nullptr)
		{
			return 1;
		}
		else
			return 0;
		items.erase(iterator);
		return 1;
	}

	ofstream & operator<<(ofstream & fout, const Player & player)
	{
		fout << player.name << endl << player.health << ' ' << player.maxHealth << ' ' << player.mana << ' ' << player.maxMana
			<< ' ' << player.money << ' ' << player.experience << ' ' << player.nextLevelExperience << ' ' << player.level << ' ' << player.X << ' ' << player.Y << ' ' << player.levelPoints << endl;
		fout << player.playerStatistics << endl;
		fout << player.damage << endl;
		if (player.currentWeapon != nullptr)
		{
			fout << "1\n";
			fout << *player.currentWeapon << endl;
		}
		else
			fout << "0\n";
		for_each(player.currentArmor.begin(), player.currentArmor.end(), [&fout](Armor * x)
		{
			if (x != nullptr)
			{
				fout << "1\n";
				fout << *x << endl;
			}
			else
				fout << "0\n";
		});
		fout << player.position << endl;
		fout << player.collision << endl;
		fout << player.textures.size() << endl;
		for_each(player.textures.begin(), player.textures.end(), [&fout](const Texture * x) { fout << *x << endl; });
		fout << player.items.size() << endl;
		if (player.items.size() > 0)
			for_each(player.items.begin(), player.items.end(), [&fout](const pair<const string, Item *> & x)
		{
			fout << x.first << endl;
			if (dynamic_cast<AttackScroll *>(x.second) != nullptr)
				fout << (int)eAttackS << endl;
			else if (dynamic_cast<EffectScroll *>(x.second) != nullptr)
				fout << (int)eEffectS << endl;
			else if (dynamic_cast<SpellScroll *>(x.second) != nullptr)
				fout << (int)eSpellS << endl;
			else if (dynamic_cast<Potion *>(x.second) != nullptr)
				fout << (int)ePotion << endl;
			else if (dynamic_cast<StatsPotion *>(x.second) != nullptr)
				fout << (int)eStatsPot << endl;
			else if (dynamic_cast<Armor *>(x.second) != nullptr)
				fout << (int)eArmor << endl;
			else if (dynamic_cast<Weapon *>(x.second) != nullptr)
				fout << (int)eWeapon << endl;
			else
				fout << (int)eItem << endl;
			x.second->Write(fout) << endl;
		});
		return fout;
	}

	ifstream & operator>>(ifstream & fin, Player & player)
	{
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, player.name);
		fin >> player.health >> player.maxHealth >> player.mana >> player.maxMana >> player.money >> player.experience >> player.nextLevelExperience >> player.level >> player.X >> player.Y >> player.levelPoints;
		size_t temporary = 0;
		fin >> player.playerStatistics >> player.damage >> temporary;
		if (temporary == 1)
		{
			player.currentWeapon = new Weapon;
			fin >> *player.currentWeapon;
		}
		for (short i = 0; i < 7; ++i)
		{
			fin >> temporary;
			if (temporary == 1)
			{
				player.currentArmor.at(i) = new Armor;
				fin >> *player.currentArmor.at(i);
			}
			else
				player.currentArmor.at(i) = nullptr;
		}
		fin >> player.position >> player.collision;
		CenterX = (ScreenWidth - player.position.w) / 2;
		CenterY = (ScreenHeight - player.position.h) / 2;
		fin >> temporary;
		for (size_t i = 0; i < temporary; ++i)
		{
			player.textures.push_back(new Texture);
			fin >> *player.textures.back();
		}
		fin >> temporary;
		string itemName = "";
		int itemType;
		Item * item = nullptr;
		for (size_t i = 0; i < temporary; ++i)
		{
			if (fin.peek() == 10)
				fin.ignore();
			getline(fin, itemName);
			fin >> itemType;
			switch (itemType)
			{
			case eAttackS:
			{
				item = new AttackScroll;
				break;
			}
			case eEffectS:
			{
				item = new EffectScroll;
				break;
			}
			case eSpellS:
			{
				item = new SpellScroll;
				break;
			}
			case ePotion:
			{
				item = new Potion;
				break;
			}
			case eStatsPot:
			{
				item = new StatsPotion;
				break;
			}
			case eArmor:
			{
				item = new Armor;
				break;
			}
			case eWeapon:
			{
				item = new Weapon;
				break;
			}
			case eItem:
			default:
			{
				ofstream fout("error.txt", ios_base::app);
				fout << "Unkown Item in item list: " << itemName << endl;
				fout.close();
				continue;
			}
			}
			item->Read(fin);
			player.items.insert(pair<const string, Item *>(itemName, item));
			itemName = "";
			item = nullptr;
		}
		return fin;
	}
}