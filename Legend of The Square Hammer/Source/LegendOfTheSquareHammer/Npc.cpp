/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "stdafx.h"
#include "Npc.h"
namespace LegendOfTheSquareHammer::Renders::Objects
{
	Npc::Npc(double scale, size_t absoluteX, size_t absoluteY, int x, int y, size_t playerExperience, size_t newHealth, size_t deathSpawnTime, const SDL_Rect & collider, const Damage & newDamage, const Damage & newResistance, const string & path, PickUp * droppedItem) : X(absoluteX), Y(absoluteY), experience(playerExperience), health(newHealth), maxHealth(newHealth), spawnTime(deathSpawnTime), damage(newDamage), resistance(newResistance), item(droppedItem)
	{
		if (!texture.LoadFile(path))
		{
			ofstream fout("error.txt", ios_base::app);
			fout << ": Building\n";
			fout.close();
			exit(0b1000000000000000);
		}
		position.x = x;
		position.y = y;
		position.w = int(scale*texture.Width());
		position.h = int(scale*texture.Height());
		collision.x = position.x + int(scale*collider.x);
		collision.y = position.y + int(scale*collider.y);
		collision.w = int(scale*collider.w);
		collision.h = int(scale*collider.h);
		colliderDown = position.h - collision.h - (position.y - collision.y);
		colliderRight = position.w - collision.w - (position.x - collision.x);
	}

	void Npc::DeathSpawn()
	{
		if (spawnTime)
			--spawnTime;
		else
		{
			active = true;
			health = maxHealth;
		}
	}

	void Npc::SetPosition(int x, int y)
	{
		position.x = x;
		collision.x = position.x + position.w - collision.w - colliderRight;
		position.y = y;
		collision.y = position.y + position.h - collision.h - colliderDown;
	}

	void Npc::ChangePosition(int x, int y)
	{
		position.x += x;
		collision.x += x;
		position.y += y;
		collision.y += y;
	}

	void Npc::SetAbsolutePosition(size_t x, size_t y)
	{
		X = x;
		Y = y;
	}

	void Npc::ChangeAbsolutePosition(int x, int y)
	{
		X += x;
		Y += y;
	}

	short Npc::CollisionDirection(const IObject * object)
	{
		int objectWidth = object->Collision().x + object->Collision().w;
		int objectHeight = object->Collision().y + object->Collision().h;
		int thisWidth = collision.x + collision.w;
		int thisHeight = collision.y + collision.h;
		if (objectWidth<collision.x || object->Collision().x>thisWidth || objectHeight<collision.y || object->Collision().y>thisHeight)
			return NONE;
		if (objectWidth >= collision.x && objectWidth < thisWidth && object->Collision().x < collision.x)
		{
			if (objectHeight >= collision.y && objectHeight < thisHeight && object->Collision().y < collision.y)
				return S | E;
			if (object->Collision().y <= thisHeight && object->Collision().y > collision.y&&objectHeight > thisHeight)
				return N | E;
			return E;
		}
		if (object->Collision().x <= thisWidth && object->Collision().x > collision.x&&objectWidth > thisWidth)
		{
			if (objectHeight >= collision.y && objectHeight < thisHeight && object->Collision().y < collision.y)
				return S | W;
			if (object->Collision().y <= thisHeight && object->Collision().y > collision.y&&objectHeight > thisHeight)
				return N | W;
			return W;
		}
		if (object->Collision().x >= collision.x && objectWidth <= thisWidth)
		{
			if (objectHeight >= collision.y&&objectHeight < thisHeight&&object->Collision().y < collision.y)
				return S;
			if (object->Collision().y <= thisHeight && object->Collision().y > collision.y&&objectHeight > thisHeight)
				return N;
		}
		return NONE;
	}

	void Npc::Move(Player * player, int fieldWidth, int fieldHeight, short collisionDirection)
	{
		if (active)
		{
			size_t distance = Distance(player, this);
			bool playerFound = false;
			if (woundTime)
				--woundTime;
			else
				texture.SetColor(255, 255, 255);
			if (moveTime == 0)
			{
				moveTime = rand() % 31 * 10 + 50;
				currentDirection = Direction(rand() % 16);
			}
			if (distance <= 600)
			{
				playerFound = true;
				int playerWidth = player->Collision().x + player->Collision().w;
				int playerHeight = player->Collision().y + player->Collision().h;
				int npcWidth = collision.x + collision.w;
				int npcHeight = collision.y + collision.h;
				if (player->Collision().x > npcWidth)
				{
					if (player->Collision().y > npcHeight)
						currentDirection = S | E;
					else if (playerHeight < collision.y)
						currentDirection = N | E;
					else
						currentDirection = E;
				}
				else if (playerWidth < collision.x)
				{
					if (player->Collision().y > npcHeight)
						currentDirection = S | W;
					else if (playerHeight < collision.y)
						currentDirection = N | W;
					else
						currentDirection = W;
				}
				else if (player->Collision().x >= collision.x && playerWidth <= npcWidth)
				{
					if (player->Collision().y > npcHeight)
						currentDirection = S;
					else if (playerHeight < collision.y)
						currentDirection = N;
				}
				if (attackTime)
					--attackTime;
				if (distance <= 60)
				{
					if (!attackTime)
					{
						player->Wound(damage);
						currentDirection = NONE;
						moveTime = 10;
						attackTime = 200;
					}
				}
			}
			else
				attackTime = 1;
			--moveTime;
			if (currentDirection != collisionDirection && currentDirection != NONE && (moveTime % 5 == 0 || (playerFound && moveTime % 2 == 0)))
			{
				if (currentDirection&N)
				{
					if (Y > 4)
					{
						Y -= 5;
						position.y -= 5;
						collision.y -= 5;
					}
					else
					{
						collision.y -= Y;
						position.y -= Y;
						Y = 0;
					}
				}
				else if (currentDirection&S)
				{
					if (int(Y) < fieldHeight - position.h - 1)
					{
						Y += 5;
						position.y += 5;
						collision.y += 5;
					}
					else
					{
						collision.y += Y - fieldHeight + collision.h;
						position.y += Y - fieldHeight + position.h;
						Y = fieldHeight - position.h;
					}
				}
				if (currentDirection&E)
				{
					if (int(X) < fieldWidth - position.w - 1)
					{
						X += 5;
						position.x += 5;
						collision.x += 5;
					}
					else
					{
						collision.x += X - fieldWidth + collision.w;
						position.x += X - fieldWidth + position.w;
						X = fieldWidth - position.w;
					}
				}
				else if (currentDirection&W)
				{
					if (X > 4)
					{
						X -= 5;
						position.x -= 5;
						collision.x -= 5;
					}
					else
					{
						collision.x -= X;
						position.x -= X;
						X = 0;
					}
				}
			}
		}
	}

	void Npc::Render()
	{
		texture.Render(position.x, position.y, position.w, position.h);
	}

	PickUp * Npc::Wound(const Damage & damageTaken)
	{
		size_t previousHealth = health;
		int d = damageTaken.Normal - resistance.Normal;
		if (d >= int(health))
		{
			health = 0;
			if (item != nullptr)
			{
				item->SetAbsolutePosition(X, Y);
				item->SetPosition(position.x, position.y);
			}
			active = false;
			spawnTime = 1000;
			PickUp * loot = item;
			item = nullptr;
			return loot;
		}
		if (d > 0)
			health -= d;
		d = damageTaken.Darkness * (100 - resistance.Darkness) / 100;
		if (d >= int(health))
		{
			health = 0;
			if (item != nullptr)
			{
				item->SetAbsolutePosition(X, Y);
				item->SetPosition(position.x, position.y);
			}
			active = false;
			spawnTime = 1000;
			PickUp * loot = item;
			item = nullptr;
			return loot;
		}
		health -= d;
		d = damageTaken.Fire * (100 - resistance.Fire) / 100;
		if (d >= int(health))
		{
			health = 0;
			if (item != nullptr)
			{
				item->SetAbsolutePosition(X, Y);
				item->SetPosition(position.x, position.y);
			}
			active = false;
			spawnTime = 1000;
			PickUp * loot = item;
			item = nullptr;
			return loot;
		}
		health -= d;
		d = damageTaken.Ice * (100 - resistance.Ice) / 100;
		if (d >= int(health))
		{
			health = 0;
			if (item != nullptr)
			{
				item->SetAbsolutePosition(X, Y);
				item->SetPosition(position.x, position.y);
			}
			active = false;
			spawnTime = 1000;
			PickUp * loot = item;
			item = nullptr;
			return loot;
		}
		health -= d;
		d = damageTaken.Magic * (100 - resistance.Magic) / 100;
		if (d >= int(health))
		{
			health = 0;
			if (item != nullptr)
			{
				item->SetAbsolutePosition(X, Y);
				item->SetPosition(position.x, position.y);
			}
			active = false;
			spawnTime = 1000;
			PickUp * loot = item;
			item = nullptr;
			return loot;
		}
		health -= d;
		if (health < previousHealth)
		{
			woundTime = 50;
			texture.SetColor(255, 0, 0);
		}
		return nullptr;
	}

	Npc::~Npc()
	{
		if (item != nullptr)
			delete item;
	}

	ofstream & operator<<(ofstream & fout, const Npc & npc)
	{
		fout << npc.X << ' ' << npc.Y << ' ' << npc.spawnTime << ' ' << npc.moveTime << ' ' << npc.active << ' ' << npc.experience << ' ' << npc.health << ' ' << npc.maxHealth << ' ' << npc.currentDirection << ' ' << npc.colliderDown << ' ' << npc.colliderRight << endl;
		fout << npc.position << endl;
		fout << npc.collision << endl;
		fout << npc.damage << endl;
		fout << npc.resistance << endl;
		fout << npc.texture << endl;
		if (npc.item != nullptr)
		{
			fout << 1 << endl;
			fout << *npc.item;
		}
		else
			fout << "0\n";
		return fout;
	}

	ifstream & operator>>(ifstream & fin, Npc & npc)
	{
		fin >> npc.X >> npc.Y >> npc.spawnTime >> npc.moveTime >> npc.active >> npc.experience >> npc.health >> npc.maxHealth >> npc.currentDirection >> npc.colliderDown >> npc.colliderRight;
		fin >> npc.position >> npc.collision >> npc.damage >> npc.resistance >> npc.texture;
		int isLoot;
		fin >> isLoot;
		if (isLoot)
		{
			npc.item = new PickUp;
			fin >> *npc.item;
		}
		return fin;
	}
}