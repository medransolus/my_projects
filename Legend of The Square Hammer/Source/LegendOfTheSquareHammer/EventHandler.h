/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Keyboard.h"
namespace LegendOfTheSquareHammer::Events
{
	class EventHandler
	{
		static inline Keyboard keyboard;

		EventHandler() {}
	public:
		static inline Keyboard & GetKeyboard() { return keyboard; }
	};
}