/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "stdafx.h"
#include <SDL_mixer.h>
#include "Game.h"
#include "EventHandler.h"
#include "Info.h"
using LegendOfTheSquareHammer::Events::EventHandler;
namespace LegendOfTheSquareHammer
{
	extern bool KonamiCode;
	extern size_t ScreenWidth;
	extern size_t ScreenHeight;
	extern size_t CenterX;
	extern size_t CenterY;
	extern SDL_Renderer * Renderer;
	extern SDL_Color HooverColor;
	extern SDL_Color NormalColor;
	extern vector<Mix_Music *> Music;;
	extern Mix_Music * CodeMusic;
	extern Renders::Texture Background;
	extern Renders::Texture OutFloor;

	extern void LoadGame();

	Game::Game(Player * newPlayer)
	{
		gameSave = new Save(newPlayer);
		gameSave->SaveGame("main");
		if (KonamiCode)
			movement = 9;
	}

	Game::Game(const string & path, string & startingPlace)
	{
		gameSave = new Save(path, startingPlace);
		if (KonamiCode)
			movement = 9;
	}

	short Game::CheckCollision(const IObject * object, const NpcMap * monsters)
	{
		short collisionDirection = 0;
		if (monsters != nullptr)
		{
			for_each(monsters->begin(), monsters->end(), [&collisionDirection, &object](const pair<const string, Renders::Objects::Npc *> & x)
			{
				if (Distance(object, x.second) <= 20)
				{
					short previousDirection = collisionDirection;
					collisionDirection = x.second->CollisionDirection(object);
					collisionDirection = collisionDirection | previousDirection;
				}
			});
		}
		return collisionDirection;
	}

	CollisionEntrance Game::CheckCollision(const IObject * object, const BuildingsMap * buildings)
	{
		CollisionEntrance collision;
		if (buildings != nullptr)
		{
			for_each(buildings->begin(), buildings->end(), [&collision, &object](const pair<const string, Building *> & x)
			{
				if (Distance(object, (IObject *)x.second) <= 50)
				{
					short previousDirection = collision.Direction;
					collision = x.second->CollisionDirection(object);
					collision.Direction = collision.Direction | previousDirection;
				}
			});
		}
		return collision;
	}

	void Game::Attack(int x, int y, const string & place)
	{
		short attackDirection;
		if (x < gameSave->Player()->Collision().x)
		{
			if (y < gameSave->Player()->Collision().y)
				attackDirection = N | W;
			else if (y > gameSave->Player()->Collision().y + gameSave->Player()->Collision().h)
				attackDirection = S | W;
			else
				attackDirection = W;
		}
		else if (x > gameSave->Player()->Collision().x + gameSave->Player()->Collision().w)
		{
			if (y < gameSave->Player()->Collision().y)
				attackDirection = N | E;
			else if (y > gameSave->Player()->Collision().y + gameSave->Player()->Collision().h)
				attackDirection = S | E;
			else
				attackDirection = E;
		}
		else
		{
			if (y < gameSave->Player()->Collision().y)
				attackDirection = N;
			else
				attackDirection = S;
		}
		if (gameSave->Monsters(place) != nullptr)
		{
			for_each(gameSave->Monsters(place)->begin(), gameSave->Monsters(place)->end(), [this, &attackDirection, &place](pair<const string, Renders::Objects::Npc *> & x)
			{
				if (x.second->IsActive() && 300 >= sqrt(pow(abs(x.second->Collision().x + x.second->Collision().w / 2 - gameSave->Player()->Collision().x - gameSave->Player()->Collision().w / 2), 2) + pow(abs(x.second->Collision().y + x.second->Collision().h - gameSave->Player()->Collision().y - gameSave->Player()->Collision().h / 2), 2)))
				{
					bool isDamage = false;
					switch (attackDirection)
					{
					case N:
					{
						if (((x.second->Collision().x >= gameSave->Player()->Collision().x&&x.second->Collision().x <= gameSave->Player()->Collision().x + gameSave->Player()->Collision().w) || (x.second->Collision().x + x.second->Collision().w >= gameSave->Player()->Collision().x&&x.second->Collision().x + x.second->Collision().w <= gameSave->Player()->Collision().x + gameSave->Player()->Collision().w) || (x.second->Collision().x <= gameSave->Player()->Collision().x&&x.second->Collision().x + x.second->Collision().w >= gameSave->Player()->Collision().x + gameSave->Player()->Collision().w)) && (x.second->Collision().y + x.second->Collision().h <= gameSave->Player()->Collision().y))
							isDamage = true;
						break;
					}
					case S:
					{
						if (((x.second->Collision().x >= gameSave->Player()->Collision().x&&x.second->Collision().x <= gameSave->Player()->Collision().x + gameSave->Player()->Collision().w) || (x.second->Collision().x + x.second->Collision().w >= gameSave->Player()->Collision().x&&x.second->Collision().x + x.second->Collision().w <= gameSave->Player()->Collision().x + gameSave->Player()->Collision().w) || (x.second->Collision().x <= gameSave->Player()->Collision().x&&x.second->Collision().x + x.second->Collision().w >= gameSave->Player()->Collision().x + gameSave->Player()->Collision().w)) && (x.second->Collision().y >= gameSave->Player()->Collision().y + gameSave->Player()->Collision().h))
							isDamage = true;
						break;
					}
					case E:
					{
						if (((x.second->Collision().y >= gameSave->Player()->Collision().y&&x.second->Collision().y <= gameSave->Player()->Collision().y + gameSave->Player()->Collision().h) || (x.second->Collision().y + x.second->Collision().h >= gameSave->Player()->Collision().y&&x.second->Collision().y + x.second->Collision().h <= gameSave->Player()->Collision().y + gameSave->Player()->Collision().h) || (x.second->Collision().y <= gameSave->Player()->Collision().y&&x.second->Collision().y + x.second->Collision().h >= gameSave->Player()->Collision().y + gameSave->Player()->Collision().h)) && (x.second->Collision().x >= gameSave->Player()->Collision().x + gameSave->Player()->Collision().w))
							isDamage = true;
						break;
					}
					case W:
					{
						if (((x.second->Collision().y >= gameSave->Player()->Collision().y&&x.second->Collision().y <= gameSave->Player()->Collision().y + gameSave->Player()->Collision().h) || (x.second->Collision().y + x.second->Collision().h >= gameSave->Player()->Collision().y&&x.second->Collision().y + x.second->Collision().h <= gameSave->Player()->Collision().y + gameSave->Player()->Collision().h) || (x.second->Collision().y <= gameSave->Player()->Collision().y&&x.second->Collision().y + x.second->Collision().h >= gameSave->Player()->Collision().y + gameSave->Player()->Collision().h)) && (x.second->Collision().x + x.second->Collision().w <= gameSave->Player()->Collision().x))
							isDamage = true;
						break;
					}
					case N | E:
					{
						if (x.second->Collision().y <= gameSave->Player()->Collision().y&&x.second->Collision().x + x.second->Collision().w >= gameSave->Player()->Collision().x + gameSave->Player()->Collision().w)
							isDamage = true;
						break;
					}
					case N | W:
					{
						if (x.second->Collision().y <= gameSave->Player()->Collision().y&&x.second->Collision().x <= gameSave->Player()->Collision().x)
							isDamage = true;
						break;
					}
					case S | E:
					{
						if (x.second->Collision().y >= gameSave->Player()->Collision().y + gameSave->Player()->Collision().h&&x.second->Collision().x + x.second->Collision().w >= gameSave->Player()->Collision().x + gameSave->Player()->Collision().w)
							isDamage = true;
						break;
					}
					case S | W:
					{
						if (x.second->Collision().y >= gameSave->Player()->Collision().y + gameSave->Player()->Collision().h&&x.second->Collision().x <= gameSave->Player()->Collision().x)
							isDamage = true;
						break;
					}
					}
					if (isDamage)
					{
						Renders::Objects::PickUp * loot = x.second->Wound(gameSave->Player()->Damage());
						if (loot != nullptr)
						{
							if (gameSave->Player()->AddExperience(x.second->Experience()))
								isNewLevel = true;
							if (gameSave->Items(place) != nullptr)
								gameSave->Items(place)->insert(pair<string, Renders::Objects::PickUp *>(loot->Name(), loot));
						}
					}
				}
			});
		}
	}

	void Game::RenderOrder(const string & place)
	{
		BuildingsMap * buildings = gameSave->Buildings(place);
		NpcMap * monsters = gameSave->Monsters(place);
		PickUpMap * items = gameSave->Items(place);
		vector<IObject *> toRender;
		toRender.push_back(gameSave->Player());
		if (buildings != nullptr)
		{
			for_each(buildings->begin(), buildings->end(), [&toRender](pair<const string, Building *> & x)
			{
				if (((x.second->Position().x < int(ScreenWidth) && x.second->Position().x >= 0) || (x.second->Position().x + x.second->Position().w > 0 && x.second->Position().x + x.second->Position().w <= int(ScreenWidth)) || (x.second->Position().x < 0 && x.second->Position().x + x.second->Position().w > int(ScreenWidth))) && ((x.second->Position().y < int(ScreenHeight) && x.second->Position().y >= 0) || (x.second->Position().y + x.second->Position().h > 0 && x.second->Position().y + x.second->Position().h <= int(ScreenHeight)) || (x.second->Position().y < 0 && x.second->Position().y + x.second->Position().h > int(ScreenHeight))))
					toRender.push_back(x.second);
			});
		}
		if (monsters != nullptr)
		{
			for_each(monsters->begin(), monsters->end(), [&toRender](pair<const string, Npc *> & x)
			{
				if (x.second->IsActive() && (((x.second->Position().x < int(ScreenWidth) && x.second->Position().x >= 0) || (x.second->Position().x + x.second->Position().w > 0 && x.second->Position().x + x.second->Position().w <= int(ScreenWidth))) && ((x.second->Position().y < int(ScreenHeight) && x.second->Position().y >= 0) || (x.second->Position().y + x.second->Position().h > 0 && x.second->Position().y + x.second->Position().h <= int(ScreenHeight)))))
					toRender.push_back(x.second);
			});
		}
		if (items != nullptr)
		{
			for_each(items->begin(), items->end(), [&toRender](pair<const string, PickUp *> & x)
			{
				if (((x.second->Position().x < int(ScreenWidth) && x.second->Position().x >= 0) || (x.second->Position().x + x.second->Position().w > 0 && x.second->Position().x + x.second->Position().w <= int(ScreenWidth))) && ((x.second->Position().y < int(ScreenHeight) && x.second->Position().y >= 0) || (x.second->Position().y + x.second->Position().h > 0 && x.second->Position().y + x.second->Position().h <= int(ScreenHeight))))
					toRender.push_back(x.second);
			});
		}
		sort(toRender.begin(), toRender.end(), [](IObject * x, IObject * y) { return x->Collision().y < y->Collision().y; });
		for_each(toRender.begin(), toRender.end(), [](IObject * x) { x->Render(); });
		Data::GetLabels().Mini.at(1)->Texture().LoadText("Health:  " + to_string(gameSave->Player()->Health()), { 255,0,0,255 });
		Data::GetLabels().Mini.at(1)->Render();
		if (isNewLevel)
			Data::GetLabels().Mini.at(2)->Render();
	}

	void Game::MoveObjects(const string & place, CollisionEntrance & collision, int * x, int * y, int fieldWidth, int fieldHeight)
	{
		Player * player = gameSave->Player();
		BuildingsMap * buildings = gameSave->Buildings(place);
		NpcMap * monsters = gameSave->Monsters(place);
		PickUpMap * items = gameSave->Items(place);
		if (EventHandler::GetKeyboard().GetKeyState(SDLK_w) && player->Position().y > 0 && !(collision.Direction&N))
		{
			if (fieldHeight != ScreenHeight)
			{
				if (player->AbsoluteY() <= CenterY)
				{
					if (player->Position().y - movement < 0)
					{
						player->SetAbsolutePosition(player->AbsoluteX(), 0);
						player->SetPosition(player->Position().x, 0);
						if (buildings != nullptr)
							for_each(buildings->begin(), buildings->end(), [this, &player](pair<const string, Building *> & x) { x.second->ChangePosition(0, movement - player->Position().y); });
						if (monsters != nullptr)
							for_each(monsters->begin(), monsters->end(), [this, &player](pair<const string, Npc *> & x) { if (x.second->IsActive()) x.second->ChangePosition(0, movement - player->Position().y); });
						if (items != nullptr)
							for_each(items->begin(), items->end(), [this, &player](pair<const string, PickUp *> & x) { x.second->ChangePosition(0, movement - player->Position().y); });
					}
					else
					{
						player->ChangeAbsolutePosition(0, -1 * movement);
						player->ChangePosition(0, -1 * movement);
					}
				}
				else if (player->AbsoluteY() >= fieldHeight - CenterY)
				{
					player->ChangeAbsolutePosition(0, -1 * movement);
					player->ChangePosition(0, -1 * movement);

				}
				else
				{
					player->SetPosition(player->Position().x, CenterY);
					player->ChangeAbsolutePosition(0, -1 * movement);
					if (buildings != nullptr)
						for_each(buildings->begin(), buildings->end(), [this](pair<const string, Building *> & x) { x.second->ChangePosition(0, movement); });
					if (monsters != nullptr)
						for_each(monsters->begin(), monsters->end(), [this, &player](pair<const string, Npc *> & x) { if (x.second->IsActive()) x.second->ChangePosition(0, movement); });
					if (items != nullptr)
						for_each(items->begin(), items->end(), [this, &player](pair<const string, PickUp *> & x) { x.second->ChangePosition(0, movement); });
					if (y != nullptr)
					{
						if (*y > 68)
							*y -= 68 - movement;
						else
							*y += movement;
					}
				}
			}
			else
			{
				if (player->Position().y - movement < 0)
				{
					player->SetAbsolutePosition(player->AbsoluteX(), 0);
					player->SetPosition(player->Position().x, 0);
				}
				else
				{
					player->ChangeAbsolutePosition(0, -1 * movement);
					player->ChangePosition(0, -1 * movement);
				}
			}
		}
		else if (EventHandler::GetKeyboard().GetKeyState(SDLK_s) && player->Position().y < int(ScreenHeight) && !(collision.Direction&S))
		{
			if (fieldHeight != ScreenHeight)
			{
				if (player->AbsoluteY() <= CenterY)
				{
					player->ChangeAbsolutePosition(0, movement);
					player->ChangePosition(0, movement);
				}
				else if (player->AbsoluteY() >= fieldHeight - CenterY)
				{
					if (player->Position().y + player->Position().h + movement > int(ScreenHeight))
					{
						player->SetAbsolutePosition(player->AbsoluteX(), fieldHeight - player->Position().h);
						player->SetPosition(player->Position().x, ScreenHeight - player->Position().h);
						if (buildings != nullptr)
							for_each(buildings->begin(), buildings->end(), [this, &player](pair<const string, Building *> & x) { x.second->ChangePosition(0, player->Position().y + player->Position().h - ScreenHeight); });
						if (monsters != nullptr)
							for_each(monsters->begin(), monsters->end(), [this, &player](pair<const string, Npc *> & x) { if (x.second->IsActive()) x.second->ChangePosition(0, player->Position().y + player->Position().h - ScreenHeight); });
						if (items != nullptr)
							for_each(items->begin(), items->end(), [this, &player](pair<const string, PickUp *> & x) { x.second->ChangePosition(0, player->Position().y + player->Position().h - ScreenHeight); });
					}
					else
					{
						player->ChangeAbsolutePosition(0, movement);
						player->ChangePosition(0, movement);
					}
				}
				else
				{
					player->SetPosition(player->Position().x, CenterY);
					player->ChangeAbsolutePosition(0, movement);
					if (buildings != nullptr)
						for_each(buildings->begin(), buildings->end(), [this](pair<const string, Building *> & x) { x.second->ChangePosition(0, -1 * movement); });
					if (monsters != nullptr)
						for_each(monsters->begin(), monsters->end(), [this, &player](pair<const string, Npc *> & x) { if (x.second->IsActive()) x.second->ChangePosition(0, -1 * movement); });
					if (items != nullptr)
						for_each(items->begin(), items->end(), [this, &player](pair<const string, PickUp *> & x) { x.second->ChangePosition(0, -1 * movement); });
					if (y != nullptr)
					{
						if (*y < -68)
							*y += 68 - movement;
						else
							*y -= movement;
					}
				}
			}
			else
			{
				if (player->Position().y + player->Position().h + movement > int(ScreenHeight))
				{
					player->SetAbsolutePosition(player->AbsoluteX(), fieldHeight - player->Position().h);
					player->SetPosition(player->Position().x, ScreenHeight - player->Position().h);
				}
				else
				{
					player->ChangeAbsolutePosition(0, movement);
					player->ChangePosition(0, movement);
				}
			}
		}
		if (EventHandler::GetKeyboard().GetKeyState(SDLK_a) && player->Position().x > 0 && !(collision.Direction&W))
		{
			if (fieldWidth != ScreenWidth)
			{
				if (player->AbsoluteX() <= CenterX)
				{
					if (player->Position().x - movement < 0)
					{
						player->SetAbsolutePosition(0, player->AbsoluteY());
						player->SetPosition(0, player->Position().y);
						if (buildings != nullptr)
							for_each(buildings->begin(), buildings->end(), [this, &player](pair<const string, Building *> & x) { x.second->ChangePosition(movement - player->Position().x, 0); });
						if (monsters != nullptr)
							for_each(monsters->begin(), monsters->end(), [this, &player](pair<const string, Npc *> & x) { if (x.second->IsActive()) x.second->ChangePosition(movement - player->Position().x, 0); });
						if (items != nullptr)
							for_each(items->begin(), items->end(), [this, &player](pair<const string, PickUp *> & x) { x.second->ChangePosition(movement - player->Position().x, 0); });
					}
					else
					{
						player->ChangeAbsolutePosition(-1 * movement, 0);
						player->ChangePosition(-1 * movement, 0);
					}
				}
				else if (player->AbsoluteX() >= fieldWidth - CenterX)
				{
					player->ChangeAbsolutePosition(-1 * movement, 0);
					player->ChangePosition(-1 * movement, 0);
				}
				else
				{
					player->SetPosition(CenterX, player->Position().y);
					player->ChangeAbsolutePosition(-1 * movement, 0);
					if (buildings != nullptr)
						for_each(buildings->begin(), buildings->end(), [this](pair<const string, Building *> & x) { x.second->ChangePosition(movement, 0); });
					if (monsters != nullptr)
						for_each(monsters->begin(), monsters->end(), [this, &player](pair<const string, Npc *> & x) { if (x.second->IsActive()) x.second->ChangePosition(movement, 0); });
					if (items != nullptr)
						for_each(items->begin(), items->end(), [this, &player](pair<const string, PickUp *> & x) { x.second->ChangePosition(movement, 0); });
					if (x != nullptr)
					{
						if (*x > 68)
							*x -= 68 - movement;
						else
							*x += movement;
					}
				}
			}
			else
			{
				if (player->Position().x - movement < 0)
				{
					player->SetAbsolutePosition(0, player->AbsoluteY());
					player->SetPosition(0, player->Position().y);
				}
				else
				{
					player->ChangeAbsolutePosition(-1 * movement, 0);
					player->ChangePosition(-1 * movement, 0);
				}
			}
			player->SetTextureFlip(SDL_FLIP_NONE);
		}
		else if (EventHandler::GetKeyboard().GetKeyState(SDLK_d) && player->Position().x < int(ScreenWidth) && !(collision.Direction&E))
		{
			if (fieldWidth != ScreenWidth)
			{
				if (player->AbsoluteX() <= CenterX)
				{
					player->ChangeAbsolutePosition(movement, 0);
					player->ChangePosition(movement, 0);
				}
				else if (player->AbsoluteX() >= fieldWidth - CenterX)
				{
					if (player->Position().x + player->Position().w + movement > int(ScreenWidth))
					{
						player->SetAbsolutePosition(fieldWidth - player->Position().w, player->AbsoluteY());
						player->SetPosition(ScreenWidth - player->Position().w, player->Position().y);
						if (buildings != nullptr)
							for_each(buildings->begin(), buildings->end(), [this, &player](pair<const string, Building *> & x) { x.second->ChangePosition(player->Position().x + player->Position().w - ScreenWidth, 0); });
						if (monsters != nullptr)
							for_each(monsters->begin(), monsters->end(), [this, &player](pair<const string, Npc *> & x) { if (x.second->IsActive()) x.second->ChangePosition(player->Position().x + player->Position().w - ScreenWidth, 0); });
						if (items != nullptr)
							for_each(items->begin(), items->end(), [this, &player](pair<const string, PickUp *> & x) { x.second->ChangePosition(player->Position().x + player->Position().w - ScreenWidth, 0); });
					}
					else
					{
						player->ChangeAbsolutePosition(movement, 0);
						player->ChangePosition(movement, 0);
					}
				}
				else
				{
					player->SetPosition(CenterX, player->Position().y);
					player->ChangeAbsolutePosition(movement, 0);
					if (buildings != nullptr)
						for_each(buildings->begin(), buildings->end(), [this](pair<const string, Building *> & x) { x.second->ChangePosition(-1 * movement, 0); });
					if (monsters != nullptr)
						for_each(monsters->begin(), monsters->end(), [this, &player](pair<const string, Npc *> & x) { if (x.second->IsActive()) x.second->ChangePosition(-1 * movement, 0); });
					if (items != nullptr)
						for_each(items->begin(), items->end(), [this, &player](pair<const string, PickUp *> & x) { x.second->ChangePosition(-1 * movement, 0); });
					if (x != nullptr)
					{
						if (*x < -68)
							*x += 68 - movement;
						else
							*x -= movement;
					}
				}
			}
			else
			{
				if (player->Position().x + player->Position().w + movement > int(ScreenWidth))
				{
					player->SetAbsolutePosition(fieldWidth - player->Position().w, player->AbsoluteY());
					player->SetPosition(ScreenWidth - player->Position().w, player->Position().y);
				}
				else
				{
					player->ChangeAbsolutePosition(movement, 0);
					player->ChangePosition(movement, 0);
				}
			}
			player->SetTextureFlip(SDL_FLIP_HORIZONTAL);
		}
		if (monsters != nullptr)
			for_each(monsters->begin(), monsters->end(), [&player, &buildings, &fieldWidth, &fieldHeight, &monsters](pair<const string, Npc *> & x) { x.second->Move(player, fieldWidth, fieldHeight, CheckCollision(x.second, buildings).Direction | x.second->CollisionDirection(player) | CheckCollision(x.second, monsters)); });
	}

	void Game::CheckDeath(NpcMap * monsters)
	{
		if (monsters != nullptr)
			for_each(monsters->begin(), monsters->end(), [](pair<const string, Npc *> & x) { if (!x.second->IsActive()) x.second->DeathSpawn(); });
	}

	void Game::GetItems(PickUpMap * items)
	{
		if (items != nullptr)
		{
			vector<string> toDelete;
			for_each(items->begin(), items->end(), [this, &toDelete](const pair<const string, PickUp *> & x)
			{
				if (Distance(gameSave->Player(), x.second) <= 10)
				{
					gameSave->Player()->GetItem(x.second->GetItem());
					toDelete.push_back(x.first);
				}
			});
			if (toDelete.size())
				for_each(toDelete.begin(), toDelete.end(), [&items](const string & x) { items->erase(x); });
		}
	}

	void Game::EnterBuilding(const string & name)
	{
		if (name == "castle")
			return Castle();
		else
		{
			ofstream fout("error.txt", ios_base::app);
			fout << "Cannot find and call function: " << name << endl;
			fout.close();
		}
	}

	void Game::PauseMenu(bool gameOver)
	{
		if (gameOver)
		{
			Mix_PlayMusic(Music.at(1), -1);
			Data::GetTextures().Background.LoadFile("Data/Textures/GameOver.png");
		}
		else if (!KonamiCode)
			Mix_PlayMusic(Music.at(5), -1);
		Data::GetTextures().Background.SetBlendMode(SDL_BLENDMODE_BLEND);
		Data::GetTextures().Background.SetAlpha(170);
		Data::GetLabels().Menu.at(0)->Texture().LoadText("Resume", NormalColor);
		Data::GetLabels().Menu.at(2)->Texture().LoadText("Main Menu", NormalColor);
		SDL_Event e;
		bool finish = false;
		while (!finish)
		{
			SDL_RenderClear(Renderer);
			Data::GetTextures().Background.Render();
			if (!gameOver)
				Data::GetLabels().Menu.at(0)->Render();
			for_each(Data::GetLabels().Menu.begin() + 1, Data::GetLabels().Menu.end(), [](Renders::Label * x) { x->Render(); });
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_KEYDOWN:
				case SDL_KEYUP:
				{
					EventHandler::GetKeyboard().HandleKey(e);
					break;
				}
				case SDL_MOUSEMOTION:
				{
					int X, Y;
					SDL_GetMouseState(&X, &Y);
					for_each(Data::GetLabels().Menu.begin(), Data::GetLabels().Menu.end(), [&X, &Y](Renders::Label * x)
					{
						if (x->IsMouseOn(X, Y))
							x->Texture().SetColor(HooverColor);
						else
							x->Texture().SetColor(NormalColor);
					});
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						int X, Y;
						SDL_GetMouseState(&X, &Y);
						if (Data::GetLabels().Menu.at(0)->IsMouseOn(X, Y))
							finish = true;
						if (Data::GetLabels().Menu.at(1)->IsMouseOn(X, Y))
						{
							quit = 1;
							if (!KonamiCode)
								Mix_PlayMusic(Music.at(0), -1);
							else if (!gameOver)
								Mix_PlayMusic(CodeMusic, -1);
							finish = true;
						}
						if (Data::GetLabels().Menu.at(2)->IsMouseOn(X, Y))
						{
							quit = 2;
							if (!KonamiCode)
								Mix_PlayMusic(Music.at(0), -1);
							else if (!gameOver)
								Mix_PlayMusic(CodeMusic, -1);
							finish = true;
						}
					}
					break;
				}
				}
			}
			if (EventHandler::GetKeyboard().GetKeyState(SDLK_ESCAPE))
			{
				EventHandler::GetKeyboard().SetKeyState(SDLK_ESCAPE, false);
				break;
			}
		}
		Data::GetTextures().Background.SetAlpha(255);
		Data::GetLabels().Menu.at(0)->Texture().LoadText("New Game", NormalColor);
		Data::GetLabels().Menu.at(2)->Texture().LoadText("Exit", NormalColor);
		if (gameOver)
			Data::GetTextures().Background.LoadFile("Data/Textures/LoadData::GetTextures().Background.png");
	}

	void Game::Castle()
	{
		if (!KonamiCode)
			Mix_PlayMusic(Music.at(9), -1);
		if (gameSave->Player()->PointsLefToUse())
			isNewLevel = true;
		SDL_Event e;
		Renders::Texture Brick("Data/Textures/Brick.png");
		CollisionEntrance collision;
		while (true)
		{
			SDL_RenderClear(Renderer);
			SDL_RenderFillRect(Renderer, nullptr);
			for (short i = 0; i < 14; ++i)
				for (short j = 0; j < 7; ++j)
					Brick.Render(260 + i * 100, 140 + j * 100);
			for (short i = 0; i < 2; ++i)
				for (short j = 0; j < 2; ++j)
					Brick.Render(860 + i * 100, 840 + j * 100);
			RenderOrder("castle");
			SDL_RenderPresent(Renderer);
			while (SDL_PollEvent(&e))
			{
				switch (e.type)
				{
				case SDL_KEYDOWN:
				case SDL_KEYUP:
				{
					EventHandler::GetKeyboard().HandleKey(e);
					break;
				}
				case SDL_MOUSEBUTTONDOWN:
				{
					if (e.button.button == SDL_BUTTON_LEFT)
					{
						int x, y;
						SDL_GetMouseState(&x, &y);
						Attack(x, y, "castle");
					}
					break;
				}
				}
			}
			collision = CheckCollision(gameSave->Player(), gameSave->Buildings("castle"));
			collision.Direction = collision.Direction | CheckCollision(gameSave->Player(), gameSave->Monsters("castle"));
			if (EventHandler::GetKeyboard().GetKeyState(SDLK_ESCAPE))
			{
				EventHandler::GetKeyboard().SetKeyState(SDLK_ESCAPE, false);
				PauseMenu();
				if (quit == 1 || quit == 2)
				{
					gameSave->SaveGame("castle");
					break;
				}
				if (!KonamiCode)
					Mix_PlayMusic(Music.at(9), -1);
			}
			if (EventHandler::GetKeyboard().GetKeyState(SDLK_s) && collision.Direction&ENTER)
			{
				EventHandler::GetKeyboard().SetKeyState(SDLK_s, false);
				break;
			}
			if (EventHandler::GetKeyboard().GetKeyState(SDLK_i))
			{
				EventHandler::GetKeyboard().SetKeyState(SDLK_i, false);
				Info::PlayerEquipment(gameSave->Player());
				if (gameSave->Player()->PointsLefToUse())
					isNewLevel = true;
				else
					isNewLevel = false;
			}
			if (EventHandler::GetKeyboard().GetKeyState(SDLK_k))
			{
				EventHandler::GetKeyboard().SetKeyState(SDLK_k, false);
				Info::PlayerStatistics(gameSave->Player());
				if (gameSave->Player()->PointsLefToUse())
					isNewLevel = true;
				else
					isNewLevel = false;
			}
			MoveObjects("castle", collision, nullptr, nullptr, ScreenWidth, ScreenHeight);
			GetItems(gameSave->Items("castle"));
			CheckDeath(gameSave->Monsters("castle"));
			if (!gameSave->Player()->Health())
			{
				PauseMenu(true);
				break;
			}
		}
	}

	void Game::Play(const string & place)
	{
		if (place == "castle")
		{
			Castle();
			if (!quit)
			{
				gameSave->Player()->SetAbsolutePosition(585, 1240);
				gameSave->Player()->SetPosition(585, 466);
				if (!KonamiCode)
					Mix_PlayMusic(Music.at(7), -1);
			}
		}
		else
		{
			if (!KonamiCode)
				Mix_PlayMusic(Music.at(7), -1);
			if (gameSave->Player()->PointsLefToUse())
				isNewLevel = true;
		}
		SDL_Event e;
		CollisionEntrance collision;
		int X = 0, Y = 0;
		if (!quit)
			while (true)
			{
				SDL_RenderClear(Renderer);
				for (short i = -4, I = short(ScreenWidth / 68 + ScreenWidth % 68 + 4); i <= I; ++i)
					for (short j = -4, J = short(ScreenHeight / 68 + ScreenHeight % 68 + 4); j <= J; ++j)
						Data::GetTextures().OutFloor.Render(i * 68 + X, j * 68 + Y);
				RenderOrder("main");
				SDL_RenderPresent(Renderer);
				while (SDL_PollEvent(&e))
				{
					switch (e.type)
					{
					case SDL_KEYUP:
					case SDL_KEYDOWN:
					{
						EventHandler::GetKeyboard().HandleKey(e);
						break;
					}
					case SDL_MOUSEBUTTONDOWN:
					{
						if (e.button.button == SDL_BUTTON_LEFT)
						{
							int x, y;
							SDL_GetMouseState(&x, &y);
							Attack(x, y, "main");
						}
						break;
					}
					}
				}
				collision = CheckCollision(gameSave->Player(), gameSave->Buildings("main"));
				collision.Direction = collision.Direction | CheckCollision(gameSave->Player(), gameSave->Monsters("main"));
				if (EventHandler::GetKeyboard().GetKeyState(SDLK_ESCAPE))
				{
					EventHandler::GetKeyboard().SetKeyState(SDLK_ESCAPE, false);
					PauseMenu();
					if (quit)
					{
						gameSave->SaveGame("main");
						break;
					}
					if (!KonamiCode)
						Mix_PlayMusic(Music.at(7), -1);
				}
				if (EventHandler::GetKeyboard().GetKeyState(SDLK_w) && collision.Direction&ENTER)
				{
					EventHandler::GetKeyboard().SetKeyState(SDLK_w, false);
					SDL_Rect pos = gameSave->Player()->Position();
					pos.w = gameSave->Player()->AbsoluteX();
					pos.h = gameSave->Player()->AbsoluteY();
					gameSave->Player()->SetPosition(CenterX, CenterY + 400);
					gameSave->Player()->SetAbsolutePosition(CenterX, CenterY + 400);
					EnterBuilding(collision.Building->Name());
					if (quit)
						break;
					if (!KonamiCode)
						Mix_PlayMusic(Music.at(7), -1);
					gameSave->Player()->SetPosition(pos.x, pos.y);
					gameSave->Player()->SetAbsolutePosition(pos.w, pos.h);
					collision.Direction = N;
					collision.Building = nullptr;
				}
				if (EventHandler::GetKeyboard().GetKeyState(SDLK_i))
				{
					EventHandler::GetKeyboard().SetKeyState(SDLK_i, false);
					Info::PlayerEquipment(gameSave->Player());
					if (gameSave->Player()->PointsLefToUse())
						isNewLevel = true;
					else
						isNewLevel = false;
				}
				if (EventHandler::GetKeyboard().GetKeyState(SDLK_k))
				{
					EventHandler::GetKeyboard().SetKeyState(SDLK_k, false);
					Info::PlayerStatistics(gameSave->Player());
					if (gameSave->Player()->PointsLefToUse())
						isNewLevel = true;
					else
						isNewLevel = false;
				}
				MoveObjects("main", collision, &X, &Y);
				GetItems(gameSave->Items("main"));
				CheckDeath(gameSave->Monsters("main"));
				if (!gameSave->Player()->Health())
				{
					PauseMenu(true);
					break;
				}
			}
		if (quit == 1)
			return LoadGame();
	}

	Game::~Game()
	{
		delete gameSave;
	}
}