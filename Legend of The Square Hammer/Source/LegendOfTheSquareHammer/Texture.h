/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <fstream>
#include <string>
#undef max
using std::string;
using std::ofstream;
using std::ifstream;
using std::ios_base;
using std::endl;
using std::numeric_limits;
using std::streamsize;
namespace LegendOfTheSquareHammer::Renders
{
	class Texture
	{
		SDL_Texture * texture = nullptr;
		SDL_Texture * textTexture = nullptr;
		string text = "none";
		string path = "noPath";
		SDL_Color textColor = { 0,0,0,0 };
		int width = 0;
		int height = 0;
		int textWidth = 0;
		int textHeight = 0;
	public:
		Texture() {}
		Texture(const string & newPath);

		static SDL_Surface * LoadSurface(const string & path);

		inline int Width() const { return width; }
		inline int Height() const { return height; }
		inline bool IsText() const { return textTexture != nullptr; }
		inline string Text() const { return text; }
		inline SDL_Color TextColor() const { return textColor; }

		bool LoadFile(const string & newPath);
		bool LoadText(const string & newText, const SDL_Color & color);
		void Free(bool option);
		void Render(SDL_RendererFlip flip = SDL_FLIP_NONE, double angle = 0.0, SDL_Point * point = nullptr, bool textFlip = false, SDL_Rect * clip = nullptr);
		void Render(int x, int y, SDL_RendererFlip flip = SDL_FLIP_NONE, double angle = 0.0, SDL_Point * point = nullptr, bool textFlip = false, SDL_Rect * clip = nullptr);
		void Render(int x, int y, int w, int h, SDL_RendererFlip flip = SDL_FLIP_NONE, double angle = 0.0, bool textFlip = false, SDL_Point * point = nullptr);
		void SetColor(Uint8  r, Uint8 g, Uint8 b);
		void SetColor(const SDL_Color & color);
		void SetBlendMode(SDL_BlendMode blendMode);
		void SetAlpha(Uint8 alpha);

		friend ofstream & operator<<(ofstream & fout, const Texture & texture)
		{
			fout << texture.path << endl << texture.width << ' ' << texture.height << endl << texture.text;
			if (texture.text != "none")
				fout << endl << texture.textWidth << ' ' << texture.textHeight << endl << texture.textColor.r << ' ' << texture.textColor.g << ' ' << texture.textColor.b << ' ' << texture.textColor.a;
			return fout;
		}
		friend ifstream & operator>>(ifstream & fin, Texture & texture)
		{
			if (fin.peek() == 10)
				fin.ignore();
			getline(fin, texture.path);
			fin >> texture.width >> texture.height;
			fin.ignore(numeric_limits<streamsize>::max(), '\n');
			getline(fin, texture.text);
			if (texture.path != "noPath")
				if (!texture.LoadFile(texture.path))
				{
					ofstream fout("error.txt", ios_base::app);
					fout << " :Texture load failed\n";
					fout.close();
				}
			if (texture.text != "none")
			{
				fin >> texture.textWidth >> texture.textHeight >> texture.textColor.r >> texture.textColor.g >> texture.textColor.b >> texture.textColor.a;
				if (!texture.LoadText(texture.text, texture.textColor))
				{
					ofstream fout("error.txt", ios_base::app);
					fout << " :Text load failed\n";
					fout.close();
				}
			}
			return fin;
		}

		~Texture() { Free(true); }
	};
}
