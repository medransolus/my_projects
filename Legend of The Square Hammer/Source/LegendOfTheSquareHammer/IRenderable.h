/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Texture.h"
namespace LegendOfTheSquareHammer::Renders
{
	__interface IRenderable
	{
		SDL_Rect Position() const;
		Renders::Texture & Texture();
		void SetPosition(int x, int y);
		void ChangePosition(int x, int y);
	};
}