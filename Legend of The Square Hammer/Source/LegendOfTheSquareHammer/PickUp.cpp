/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "stdafx.h"
#include "PickUp.h"
#include "StatsPotion.h"
#include "AttackScroll.h"
#include "EffectScroll.h"
#include "SpellScroll.h"
using namespace LegendOfTheSquareHammer::Renders::Items::SingleUse;
using namespace LegendOfTheSquareHammer::Renders::Items::Magic;
namespace LegendOfTheSquareHammer::Renders::Objects
{
	extern SDL_Renderer * Renderer;

	string PickUp::Name() const
	{
		if (item != nullptr)
			return item->Name();
		return std::to_string(X) + std::to_string(Y);
	}

	void PickUp::SetPosition(int x, int y)
	{
		item->SetPosition(x, y);
		collision = item->Position();
	}

	void PickUp::ChangePosition(int x, int y)
	{
		item->ChangePosition(x, y);
		collision = item->Position();
	}

	void PickUp::SetAbsolutePosition(size_t x, size_t y)
	{
		X = x;
		Y = y;
	}

	void PickUp::ChangeAbsolutePosition(int x, int y)
	{
		X += x;
		Y += y;
	}

	void PickUp::Render()
	{
		item->Texture().Render(item->Position().x, item->Position().y, item->Position().w, item->Position().h);
	}

	void PickUp::operator()(Player & player)
	{
		player.GetItem(item);
		this->~PickUp();
	}

	PickUp::~PickUp()
	{
		if (item != nullptr)
			delete item;
	}

	ofstream & operator<<(ofstream & fout, const PickUp & pickUp)
	{
		fout << pickUp.X << ' ' << pickUp.Y << ' ' << pickUp.item->ItemType() << endl;
		pickUp.item->Write(fout) << endl;
		fout << pickUp.collision << endl;
		return fout;
	}

	ifstream & operator>>(ifstream & fin, PickUp & pickUp)
	{
		short itemType;
		fin >> pickUp.X >> pickUp.Y >> itemType;
		switch (itemType)
		{
		case eAttackS:
		{
			pickUp.item = new AttackScroll;
			break;
		}
		case eEffectS:
		{
			pickUp.item = new EffectScroll;
			break;
		}
		case eSpellS:
		{
			pickUp.item = new SpellScroll;
			break;
		}
		case ePotion:
		{
			pickUp.item = new Potion;
			break;
		}
		case eStatsPot:
		{
			pickUp.item = new StatsPotion;
			break;
		}
		case eArmor:
		{
			pickUp.item = new Armor;
			break;
		}
		case eWeapon:
		{
			pickUp.item = new Weapon;
			break;
		}
		case eItem:
		default:
			exit(1534);
		}
		pickUp.item->Read(fin);
		fin >> pickUp.collision;
		return fin;
	}
}