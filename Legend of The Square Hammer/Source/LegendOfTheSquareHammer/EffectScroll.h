/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "IScroll.h"
namespace LegendOfTheSquareHammer::Renders::Items::Magic
{
	class EffectScroll : public Item, public IScroll
	{
		short manaCost = 0;
		Stats statisticsChange;
		MainStats minimumStatistics;
	public:
		EffectScroll() {}
		EffectScroll(const string & newName, const string & newDescription, short Mana, const Stats & newStatistics, size_t levelRequired, const MainStats & statisticsRequired, const string & path, int x, int y) : Item(newName, newDescription, levelRequired, path, x, y), manaCost(Mana), statisticsChange(newStatistics), minimumStatistics(statisticsRequired) {}

		inline short ManaCost() const { return manaCost; }
		inline Stats StatisticsChange() const { return statisticsChange; }
		inline MainStats MinimumStatistics() const { return minimumStatistics; }

		virtual LegendOfTheSquareHammer::ItemType ItemType() const { return eEffectS; }
		virtual ofstream & Write(ofstream & fout) const { return fout << *this; }
		virtual ifstream & Read(ifstream & fin) { return fin >> *this; }

		friend ofstream & operator<<(ofstream & fout, const EffectScroll & scroll);
		friend ifstream & operator>>(ifstream & fin, EffectScroll & scroll);
	};
}