/*
Author: Marek Machliński
Date: 20.04.2018
*/
#pragma once
#include "Item.h"
namespace LegendOfTheSquareHammer::Renders::Items
{
	class Armor : public Item
	{
		BodyPart part = Necklace;
		Stats statistics;
		MainStats minimumStatistics;
	public:
		Armor() {}
		Armor(BodyPart type, const string & newName, const string & newDescription, const Stats & newStatistics, size_t levelRequired, const MainStats & statisticsRequired, const string & path, int x, int y) : part(type), Item(newName, newDescription, levelRequired, path, x, y), statistics(newStatistics), minimumStatistics(statisticsRequired) {}

		inline BodyPart Part() const { return part; }
		inline Stats Statistics() const { return statistics; }
		inline MainStats MinimumStatistics() const { return minimumStatistics; }

		virtual LegendOfTheSquareHammer::ItemType ItemType() const { return eArmor; }
		virtual ofstream & Write(ofstream & fout) const { return fout << *this; }
		virtual ifstream & Read(ifstream & fin) { return fin >> *this; }

		friend ofstream & operator<<(ofstream & fout, const Armor & armor);
		friend ifstream & operator>>(ifstream & fin, Armor & armor);
	};
}