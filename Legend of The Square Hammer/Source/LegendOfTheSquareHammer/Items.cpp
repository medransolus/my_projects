/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "stdafx.h"
#include "Weapon.h"
#include "Armor.h"
#include "StatsPotion.h"
#include "SpellScroll.h"
#include "EffectScroll.h"
#include "AttackScroll.h"
namespace LegendOfTheSquareHammer
{
	extern double ScreenScale;
}
namespace LegendOfTheSquareHammer::Renders::Items
{
	Item::Item(const string & newName, const string & newDescription, size_t levelRequired, const string & path, int x, int y) : name(newName), description(newDescription), minimumLevel(levelRequired)
	{
		if (!texture.LoadFile(path))
		{
			ofstream fout("error.txt", ios_base::app);
			fout << ": Item " << name << " texture load error\n";
			fout.close();
			exit(0b100000000);
		}
		position.h = int(50 * ScreenScale);
		position.w = int(50 * ScreenScale);
		position.x = x;
		position.y = y;
	}

	ofstream & operator<<(ofstream & fout, const Weapon & weapon)
	{
		fout << weapon.type << endl << weapon.name << endl << weapon.description << endl;
		fout << weapon.damage << endl << weapon.criticalChance << ' ' << weapon.penetration << ' ' << weapon.minimumLevel << endl;
		fout << weapon.minimumStatistics << endl;
		fout << weapon.statistics << endl;
		fout << weapon.position << endl;
		fout << weapon.texture;
		return fout;
	}
	ifstream & operator>>(ifstream & fin, Weapon & weapon)
	{
		int tmp;
		fin >> tmp;
		weapon.type = (WeaponType)tmp;
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, weapon.name);
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, weapon.description);
		fin >> weapon.damage >> weapon.criticalChance >> weapon.penetration >> weapon.minimumLevel;
		fin >> weapon.minimumStatistics >> weapon.statistics >> weapon.position >> weapon.texture;
		return fin;
	}

	ofstream & operator<<(ofstream & fout, const Armor & armor)
	{
		fout << armor.part << endl << armor.name << endl << armor.description << endl;
		fout << armor.statistics << endl << armor.minimumLevel << endl;
		fout << armor.minimumStatistics << endl;
		fout << armor.position << endl;
		fout << armor.texture;
		return fout;
	}
	ifstream & operator>>(ifstream & fin, Armor & armor)
	{
		int tmp;
		fin >> tmp;
		armor.part = (BodyPart)tmp;
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, armor.name);
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, armor.description);
		fin >> armor.statistics >> armor.minimumLevel;
		fin >> armor.minimumStatistics >> armor.position >> armor.texture;
		return fin;
	}
}
namespace LegendOfTheSquareHammer::Renders::Items::SingleUse
{
	ofstream & operator<<(ofstream & fout, const Potion & potion)
	{
		fout << potion.name << endl << potion.description << endl << potion.mana << ' ' << potion.health << ' ' << potion.minimumLevel << endl;
		fout << potion.position << endl;
		fout << potion.texture;
		return fout;
	}
	ifstream & operator>>(ifstream & fin, Potion & potion)
	{
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, potion.name);
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, potion.description);
		fin >> potion.mana >> potion.health >> potion.minimumLevel;
		fin >> potion.position >> potion.texture;
		return fin;
	}

	ofstream & operator<<(ofstream & fout, const StatsPotion & potion)
	{
		fout << potion.statisticsChange << endl;
		fout << Potion(potion);
		return fout;
	}
	ifstream & operator>>(ifstream & fin, StatsPotion & potion)
	{
		fin >> potion.statisticsChange >> Potion(potion);
		return fin;
	}
}
namespace LegendOfTheSquareHammer::Renders::Items::Magic
{
	ofstream & operator<<(ofstream & fout, const SpellScroll & scroll)
	{
		fout << scroll.name << endl << scroll.description << endl << scroll.manaCost << ' ' << scroll.duration << ' ' << scroll.minimumLevel << endl;
		fout << scroll.damageOverTime << endl;
		fout << scroll.statisticsChangeOverTime << endl;
		fout << scroll.minimumStatistics << endl;
		fout << scroll.position << endl;
		fout << scroll.texture;
		return fout;
	}
	ifstream & operator>>(ifstream & fin, SpellScroll & scroll)
	{
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, scroll.name);
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, scroll.description);
		fin >> scroll.manaCost >> scroll.duration >> scroll.minimumLevel;
		fin >> scroll.damageOverTime >> scroll.statisticsChangeOverTime >> scroll.minimumStatistics >> scroll.position >> scroll.texture;
		return fin;
	}

	ofstream & operator<<(ofstream & fout, const EffectScroll & scroll)
	{
		fout << scroll.name << endl << scroll.description << endl << scroll.manaCost << ' ' << scroll.minimumLevel << endl;
		fout << scroll.minimumStatistics << endl;
		fout << scroll.position << endl;
		fout << scroll.texture;
		return fout;
	}
	ifstream & operator>>(ifstream & fin, EffectScroll & scroll)
	{
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, scroll.name);
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, scroll.description);
		fin >> scroll.manaCost >> scroll.minimumLevel;
		fin >> scroll.minimumStatistics >> scroll.position >> scroll.texture;
		return fin;
	}

	ofstream & operator<<(ofstream & fout, const AttackScroll & scroll)
	{
		fout << scroll.name << endl << scroll.description << endl << scroll.manaCost << ' ' << scroll.minimumLevel << endl;
		fout << scroll.damage << endl;
		fout << scroll.minimumStatistics << endl;
		fout << scroll.position << endl;
		fout << scroll.texture;
		return fout;
	}
	ifstream & operator>>(ifstream & fin, AttackScroll & scroll)
	{
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, scroll.name);
		if (fin.peek() == 10)
			fin.ignore();
		getline(fin, scroll.description);
		fin >> scroll.manaCost >> scroll.minimumLevel;
		fin >> scroll.damage >> scroll.minimumStatistics >> scroll.position >> scroll.texture;
		return fin;
	}
}