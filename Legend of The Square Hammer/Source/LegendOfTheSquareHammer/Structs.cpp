/*
Author: Marek Machliński
Date: 20.04.2018
*/
#include "Structs.h"
namespace LegendOfTheSquareHammer
{
	using std::endl;

	ofstream & operator<<(ofstream & fout, const Damage & damage)
	{
		fout << damage.Normal << ' ' << damage.Fire << ' ' << damage.Ice << ' ' << damage.Magic << ' ' << damage.Darkness;
		return fout;
	}
	ifstream & operator>>(ifstream & fin, Damage & damage)
	{
		fin >> damage.Normal >> damage.Fire >> damage.Ice >> damage.Magic >> damage.Darkness;
		return fin;
	}

	MainStats & MainStats::operator+=(const MainStats & mainStatistics)
	{
		Inteligence += mainStatistics.Inteligence;
		Strenght += mainStatistics.Strenght;
		Dextermity += mainStatistics.Dextermity;
		return *this;
	}

	ofstream & operator<<(ofstream & fout, const MainStats & mainStatistics)
	{
		fout << mainStatistics.Inteligence << ' ' << mainStatistics.Strenght << ' ' << mainStatistics.Dextermity;
		return fout;
	}
	ifstream & operator>>(ifstream & fin, MainStats & mainStatistics)
	{
		fin >> mainStatistics.Inteligence >> mainStatistics.Strenght >> mainStatistics.Dextermity;
		return fin;
	}

	void Stats::LevelUp(const Stats & statistics)
	{
		MainStatistics += statistics.MainStatistics;
		Endurance += statistics.Endurance;
		Vitality += statistics.Vitality;
		FireResistance = short(0.1 * MainStatistics.Inteligence + 0.03 * MainStatistics.Strenght + 0.02 * MainStatistics.Dextermity + 0.06 * Endurance + 0.04 * Vitality);
		IceResistance = short(0.11 * MainStatistics.Inteligence + 0.02 * MainStatistics.Strenght + 0.01 * MainStatistics.Dextermity + 0.06 * Endurance + 0.05 * Vitality);
		MagicResistance = short(0.17 * MainStatistics.Inteligence + 0.05 * Endurance + 0.03 * Vitality);
		DarknessResistance = short(0.12 * MainStatistics.Inteligence + 0.02 * MainStatistics.Strenght + 0.01 * MainStatistics.Dextermity + 0.04 * Endurance + 0.06 * Vitality);
	}

	Stats & Stats::operator+=(const Stats & statistics)
	{
		MainStatistics += statistics.MainStatistics;
		Endurance += statistics.Endurance;
		Vitality += statistics.Vitality;
		Armor += statistics.Armor;
		FireResistance += statistics.FireResistance;
		IceResistance += statistics.IceResistance;
		MagicResistance += statistics.MagicResistance;
		DarknessResistance += statistics.DarknessResistance;
		return *this;
	}

	ofstream & operator<<(ofstream & fout, const Stats & statistics)
	{
		fout << statistics.MainStatistics << endl << statistics.Endurance << ' ' << statistics.Vitality << ' ' << statistics.Armor << ' ' << statistics.FireResistance << ' ' << statistics.IceResistance << ' ' << statistics.MagicResistance << ' ' << statistics.DarknessResistance;
		return fout;
	}
	ifstream & operator>>(ifstream & fin, Stats & statistics)
	{
		fin >> statistics.MainStatistics >> statistics.Endurance >> statistics.Vitality >> statistics.Armor >> statistics.FireResistance >> statistics.IceResistance >> statistics.MagicResistance >> statistics.DarknessResistance;
		return fin;
	}
}