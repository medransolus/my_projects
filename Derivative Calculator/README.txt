Due to some mismatch between different versions of Qt Creator and connecting it to DerivativeLibrary it is recomended to compile if necessary DerivativeCalculator with Qt Creator 4.4.1 Community version with Qt 5.9.2 (MinGW 5.3.0 32 bit).

All projects should be used as 32 bit programs.

After writing down functions like sin, cos, ctg, tg, ln and log please use following notation:
"sin 5" or "sin (5*x+2)"